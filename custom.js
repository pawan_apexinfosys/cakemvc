/*
* Doctors Module
* All Jquery for doctors module is under
*/

$(document).ready(function() {
	
	/*
	*************************************************************
	****************COMMON FUNCTIONS*****************************
	*************************************************************
	*/
	
	loadActivitySection(null, 'today');
	
	// Hassan Removed this step
	$(document).on('click', '.my_activity', function() {
		var profile_id = $(this).attr('data-id');
		loadActivitySection(profile_id);
	});

	$(document).on('change', '.cmn-toggle', function() {
		var day = $(this).prop('checked');
		if(day){
			day = 'yesturday';
		}else{
			day = 'today';
		}
		console.log('load '+day+' activities');
		loadActivitySection(null, day)
	});
	
	$(document).on('click', '.remove_runtime_progress_bar', function() {
		$(this).parent().parent().remove();
	});
	
	/*
	* Date & Datetime picker function
	*/

	$('.showCalander').datetimepicker({
		 format: 'ddd MM/DD/YYYY'
	});

	$('.showTime').datetimepicker({
		 format: 'LT'
	});

	$('.patientCalander').datetimepicker({
		 format: 'MM-DD-YYYY'
	});
	
	$(document).on('click', '.glyphicon-calendar', function() {
		$(this).parent().parent().find('.showCalander').focus();
	});

	$(document).on('click', '.glyphicon-time', function() {
		$(this).parent().parent().find('.showTime').focus();
	});

	/*
	* Select Picker 
	*/
	$('.selectpicker').selectpicker();
	
	/*
	* TOOLTIP
	*/
	$('[data-toggle="tooltip"]').tooltip();

	/*
	* Phone masking
	*/
	$("#profile-phone").mask("999-999-9999");
	
	// Input attachment click open file 

	$(document).on('click', '.attachments', function () {
		$(this).next().find('input[type="file"]').trigger('click');
	});

	/*
	* User select/Check functionality - Contact modyle
	* Check all checkbox functionality
	*/

	$(document).on('click', '.check_user_all', function() {
		var countReturn = countSelectedUsers();
		console.log(countReturn['total']+'_____'+countReturn['selected']);
		checkedCount = 0;
		if($(this).is(':checked')){
			$('.check_user_single').each(function() {
				$(this).prop('checked', true);
				checkedCount = checkedCount + $(this).length;
			});
			$('.selected-user-count').html(checkedCount);
			// Doctor delete popup display true using data-toggle = modal
			if(checkedCount > 0){
				addModal();
			}else{
				removeModal();
			}
		}else{
			$('.check_user_single').each(function() {
				$(this).prop('checked', false);
			});
			$('.selected-user-count').html(0);
			removeModal();
		}
	});
	
	// Single check functionality
	$(document).on('click', '.check_user_single', function() {
		var countReturn = countSelectedUsers();
		
		$('.selected-user-count').html(countReturn['selected']);

		// Doctor delete popup display true using data-toggle = modal
		if(countReturn['selected'] > 0){
			addModal();
		}else{
			removeModal();
		}

		if(countReturn['selected'] == countReturn['total']){
			$('.check_user_all').prop('checked', true);
		}else{
			$('.check_user_all').prop('checked', false);
		}
	});

	
	$(document).on('click', '.delete_user_icon, .delete_task_icon, .assign_task_icon, .completed_task_icon', function() {
		var findSelected = countSelectedUsers();
		if(findSelected.selected <= 0){
			alert('Select atleast one Task');
			return false;
		}
	});

	
	/*
	************************************************************* 
	****************DOCTORS MODULE FUNCTIONS*********************
	*************************************************************
	*/

	/*
	* Empty fields on add doctor button click
	*/
	$(document).on('click', '#add-doctor-button', function() {
		emptyInputFields();
		removeFlashMessage();
		progressBar();
	});

	$(document).on('click', '.edit-doctor-button', function() {
		emptyInputFields();
		removeFlashMessage();
		progressBar();
	});
	
	/*
	* Add Doctor functionality using ajax submission
	*/
	$( document ).on( "submit", '.add_doctor', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'doctors/add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					
					$form.find( ".btn-default" ).trigger('click');
					afterAdd('<p><strong>'+response.msg+'</strong></p>');
					afterAddRemove();

					//$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');

					// Empty all previous fields
					$form.find( ".add_doctor").val('');
					$form.find( "input[type=text], input[type=password], input[type=hidden], textarea" ).val("");
					$form.find( ":checkbox" ).prop('checked', false);
					
					// Empty all Select dropdown
					$form.children().find('.selectpicker option:first-child').attr("selected", "selected");
					$form.children().find('.selectpicker').each(function() {
						$(this).next().children().find('span.filter-option').html($(this).find(':first-child').text());
					});

					//Prepend new added data
					$('#doctor_grid_tbody').prepend(response.data);
					progressBar();
					removeFlashMessage();
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');

					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								console.log(i+'-'+k+'___'+n);
								
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");

								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	/*
	* Edit Doctor functionality using ajax submission
	*/
	$( document ).on( "submit", '.edit_doctor', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'doctors/edit',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 

					$form.find( ".btn-default" ).trigger('click');

					var search_filter_key = $('#search_keyword').val();
					var search_filter_by = $('#search_filter').val();
					searchFunction(search_filter_key, search_filter_by);
					setTimeout(function () {

						$('#tr_'+response.user_id).trigger('click');
						afterAdd('<p><strong>'+response.msg+'</strong></p>');
						afterAddRemove();
						//$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');

						progressBar();
						removeFlashMessage();

					}, 3000);

				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');

					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								console.log(i+'-'+k+'___'+n);
								
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");

								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	/*
	* Edit Doctor functionality using ajax submission
	*/
	$( document ).on( "submit", '.edit_user', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'manage/edit',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					
					$form.find( ".btn-default" ).trigger('click');

					var search_filter_by = $('#manage_search_filter').val();
					var search_filter_key = $('#search_manage_keyword').val();
					searchManageFunction(search_filter_key, search_filter_by);

					setTimeout(function () {

						$('#tr_'+response.user_id).trigger('click');
						afterAdd('<p><strong>'+response.msg+'</strong></p>');
						afterAddRemove();
						//$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');

						progressBar();
						removeFlashMessage();

					}, 3000);

					
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');

					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								console.log(i+'-'+k+'___'+n);
								
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");

								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});
	
	/*
	* Right sidebar doctor show functionality
	*/
	$(document).on('click', '.get_right_sidebar_user', function(){ 
		$('table > tbody > tr').removeClass('tr_hover');
		$(this).addClass('tr_hover');
		var id = $(this).attr('id');
		$.ajax({
			url			: defaultUrl + 'doctors/right_sidebar',
			type		: 'POST',
			dataType	: 'HTML',
			data		: {
				id	: id
			},
			success : function(response){
				$('#right-sidebar').html(response);
			}
		});
	});
	

	/*
	* Search Functionality
	*/

	$(document).on('change', '#search_filter', function() {
		var search_filter_by = $(this).val();
		var search_filter_key = $('#search_keyword').val();
		//console.log(search_filter_key+"___"+search_filter_by);
		searchFunction(search_filter_key, search_filter_by);
	});
	
	$(document).on('keyup', '#search_keyword', function() {
		var search_filter_by = $('#search_filter').val();
		var search_filter_key = $(this).val();
		//console.log(search_filter_key+"___"+search_filter_by);
		searchFunction(search_filter_key, search_filter_by);
	});

});
	/*
	*
	*/

	function searchFunction(search_filter_key, search_filter_by) {
		$.ajax({
			url			: defaultUrl + 'doctors/index',
			type		: 'POST',
			dataType	: 'HTML',
			data		: {
				search_filter_by	: search_filter_by,
				search_filter_key	: search_filter_key
			},
			success : function(response){
				//console.log(response);
				$('#doctor_grid_tbody').html('').html(response);
			}
		});
	}

	function removeModal(){
		console.log('Remove Modal');
		// CONTACT
		$('.delete_user_icon').attr('data-toggle', '');
		$('.owner_user_icon').attr('data-toggle', '');
		$('.call_user_icon').attr('data-toggle', '');
		$('.dead_user_icon').attr('data-toggle', '');
		
		// TASK
		$('.delete_task_icon').attr('data-toggle', '');
		$('.assign_task_icon').attr('data-toggle', '');
		$('.completed_task_icon').attr('data-toggle', '');
	}

	function addModal(){
		console.log('Add Modal');
		// CONTACT
		$('.delete_user_icon').attr('data-toggle', 'modal');
		$('.owner_user_icon').attr('data-toggle', 'modal');
		$('.call_user_icon').attr('data-toggle', 'modal');
		$('.dead_user_icon').attr('data-toggle', 'modal');

		// TASK
		$('.delete_task_icon').attr('data-toggle', 'modal');
		$('.assign_task_icon').attr('data-toggle', 'modal');
		$('.completed_task_icon').attr('data-toggle', 'modal');
	}
	
	function afterAdd(text){
		$('.after-add-text').html(text);
		$('#success_popup').modal('show');
	}

	function afterAddRemove(){
		setTimeout(function() {
			$('#success_popup').modal('hide');
		}, 3000)
	}

	function hide_fileUpload(){
		$('.file_edit').hide();
		return false;
	}
	
	function clear_note(obj){
		obj.parent().parent().find('textarea').val('');
		removeFlashMessage();
		progressBar();
	}

	$(document).on('click', '.delete_file', function(){
		var obj = $(this);
		var id = $(this).attr('data-attr');
		var user_id = $(this).attr('data-user-attr');
		console.log(id);
		$.ajax({
			url			: defaultUrl + 'contacts/deleteFile',
			type		: 'POST',
			dataType	: 'HTML',
			data		: {
				id	: id
			},
			success : function(response){
				console.log(".remove_file_"+user_id);
				$("#deleteFileModal").modal('hide');
				$(".modal").modal('hide');
				$("#tr_"+user_id).trigger('click');
				$(".remove_file_"+id).remove();
				return false;
			}
		});
		return false;
	});
/*
	* Search Functionality for All search filter commen layour
	*/
	$(document).on('keyup', '#comon_search_keyword', function() {
		var search_filter_key = $(this).val();
  		searchCommonFilterFunction(search_filter_key);
		return false;
	});
	function searchCommonFilterFunction(search_filter_key) {
		$.ajax({
			url			: ajaxUrl + 'users/searchCommonFilter',
			type		: 'POST',
			dataType	: 'HTML',
			data		: {
				search_filter_key	: search_filter_key,
			},
			success : function(response){
				//console.log(response);
				//$('#contact_grid_tbody').html('').html(response);
				return false;
			}
		});
	}
	
	/*
	* Search Functionality for contact module
	*/
	$(document).on('change', '#contact_search_filter', function() {
		var search_filter_by = $(this).val();
		var search_filter_key = $('#search_contact_keyword').val();
		console.log(search_filter_key+"___"+search_filter_by);
		searchContactFunction(search_filter_key, search_filter_by);
		return false;
	});
	$(document).on('keyup', '#search_contact_keyword', function() {
		var search_filter_by = $('#contact_search_filter').val();
		var search_filter_key = $(this).val();
		console.log(search_filter_key+"___"+search_filter_by);
		searchContactFunction(search_filter_key, search_filter_by);
		return false;
	});
	
	function searchContactFunction(search_filter_key, search_filter_by) {
		$.ajax({
			url			: ajaxUrl + 'contacts/index',
			type		: 'POST',
			dataType	: 'HTML',
			data		: {
				search_filter_by	: search_filter_by,
				search_filter_key	: search_filter_key
			},
			success : function(response){
				//console.log(response);
				$('#contact_grid_tbody').html('').html(response);
				return false;
			}
		});
	}

	/*
	* Search Functionality for contact module all contacts 
	*/
	$(document).on('change', '#all_contact_search', function() {
		var search_filter_by =  $(this).val();
		console.log(search_filter_by);
		searchAllContactFunction(search_filter_by);
		return false;
	});

	function searchAllContactFunction(search_filter_by) {
		

		if(search_filter_by == "allcontact"){
			window.location = defaultUrl + 'contacts';
		}else{
			window.location = defaultUrl + 'contacts/allsearch/' + search_filter_by;
		}
		return false;
		/*
		$.ajax({
			url			: ajaxUrl + 'contacts/allsearch',
			type		: 'POST',
			dataType	: 'HTML',
			data		: {
				search_filter_by	: search_filter_by,
				type				: 'countSearch'
			},
			success : function(response){
				console.log(response);
				$('#contact_grid_tbody').html('').html(response);
				return false;
			}
		});
		*/
	}

	function searchAllAjax() {
		$.ajax({
			url			: ajaxUrl + 'contacts/odering',
			type		: 'POST',
			dataType	: 'HTML',
			success : function(response){
				$('.all_contact_search_counter').html(response);
				return false;
			}
		});
		return false;
	}

	/*
	* Search Functionality for task module all contacts 
	*/
	$(document).on('change', '#task_search_filter', function() {
			var search_filter_by = $(this).val();
			var search_filter_key = $('#search_task_keyword').val();
			console.log(search_filter_key+"___"+search_filter_by);
			searchTaskFunction(search_filter_key, search_filter_by);
			return false;
		});
	$(document).on('keyup', '#search_task_keyword', function() {
			var search_filter_by = $('#task_search_filter').val();
			var search_filter_key = $(this).val();
			console.log(search_filter_key+"___"+search_filter_by);
			searchTaskFunction(search_filter_key, search_filter_by);
			return false;
	});

	function searchTaskFunction(search_filter_key, search_filter_by) {
		$.ajax({
			url			: defaultUrl + 'tasks/index',
			type		: 'POST',
			dataType	: 'HTML',
			data		: {
				search_filter_by	: search_filter_by,
				search_filter_key	: search_filter_key
			},
			success : function(response){
				//console.log(response);
				$('#task_grid_tbody').html('').html(response);
				return false;
			}
		});
	}

	/*
	* Search Functionality for task module all task left dropdown 
	*/
	$(document).on('change', '#all_task_left_filter', function() {
		var search_filter_by =  $(this).val();
		console.log(search_filter_by);
		searchAllTaskLeftDropFunction(search_filter_by);
		return false;
	});
	function searchAllTaskLeftDropFunction(search_filter_by){
	$.ajax({
		url			: defaultUrl + 'tasks/taskDropDownFilter',
		type		: 'POST',
		dataType	: 'HTML',
		data		: {
			search_filter_by	: search_filter_by
		},
		success : function(response){
			//console.log(response);
			$('#task_grid_tbody').html('').html(response);
			return false;
			}
		});
	}
	
	/*
	* Search Functionality for manage module all contacts 
	*/
	$(document).on('change', '#manage_search_filter', function() {
			var search_filter_by = $(this).val();
			var search_filter_key = $('#search_manage_keyword').val();
			console.log(search_filter_key+"___"+search_filter_by);
			searchManageFunction(search_filter_key, search_filter_by);
			return false;
		});
	$(document).on('keyup', '#search_manage_keyword', function() {
			var search_filter_by = $('#manage_search_filter').val();
			var search_filter_key = $(this).val();
			console.log(search_filter_key+"___"+search_filter_by);
			searchManageFunction(search_filter_key, search_filter_by);
			return false;
	});


	function searchManageFunction(search_filter_key, search_filter_by) {
		$.ajax({
			url			: defaultUrl + 'manage/index',
			type		: 'POST',
			dataType	: 'HTML',
			data		: {
				search_filter_by	: search_filter_by,
				search_filter_key	: search_filter_key
			},
			success : function(response){
				//console.log(response);
				$('#manage_grid_tbody').html('').html(response);
				return false;
			}
		});
	}



	/*
	* Remove Flash Message
	*/
	function removeFlashMessage(){
		// Remove messages if any
		$('.form-error').remove();

		// Remove all error class and label first
		$( ".form-group" ).removeClass( "has-error" );
		$( ".runtime-label" ).remove();

		$('.files-status-table').html('');

		return true;
	}
	

	// Empty form fields
	function emptyInputFields(){
		// Add lead
		$('.add_lead').children().find('input[type="text"]').each(function(){
			$(this).val('');
		});
		$('.add_lead').children().find('input[type="url"]').each(function(){
			$(this).val('');
		});
		$('.add_lead').children().find('input[type="tel"]').each(function(){
			$(this).val('');
		});
		$('.add_lead').children().find('input[type="email"]').each(function(){
			$(this).val('');
		});
		$('.add_lead').children().find('textarea').each(function(){
			$(this).val('');
		});
		
		// Add Doctor
		$('.add_doctor').children().find('input[type="text"]').each(function(){
			$(this).val('');
		});
		$('.add_doctor').children().find('input[type="url"]').each(function(){
			$(this).val('');
		});
		$('.add_doctor').children().find('input[type="tel"]').each(function(){
			$(this).val('');
		});
		$('.add_doctor').children().find('input[type="email"]').each(function(){
			$(this).val('');
		});
		$('.add_doctor').children().find('textarea').each(function(){
			$(this).val('');
		});
		
		// Add Task
		$('.add_activity_task').children().find('input[type="text"]').each(function(){
			$(this).val('');
		});
		$('.add_activity_task').children().find('input[type="url"]').each(function(){
			$(this).val('');
		});
		$('.add_activity_task').children().find('input[type="tel"]').each(function(){
			$(this).val('');
		});
		$('.add_activity_task').children().find('input[type="email"]').each(function(){
			$(this).val('');
		});
		$('.add_activity_task').children().find('textarea').each(function(){
			$(this).val('');
		});

		// Add People
		$('.add_people').children().find('input[type="text"]').each(function(){
			$(this).val('');
		});
		$('.add_people').children().find('input[type="url"]').each(function(){
			$(this).val('');
		});
		$('.add_people').children().find('input[type="tel"]').each(function(){
			$(this).val('');
		});
		$('.add_people').children().find('input[type="email"]').each(function(){
			$(this).val('');
		});
		$('.add_people').children().find('textarea').each(function(){
			$(this).val('');
		});
		

		return true;
	}	

	// Progress bar at 0% and Hide parent div

	function progressBar(){
		$('#progress .progress-bar').css('width', '0%');
		$('.progress .progress-bar').css('width', '0%');
		$('#progress').hide();
		$('.progress').hide();
		$('.files-name').html("");
		$('.files-status-2').html('');
		$('.files-status-table-2').html('');
		$('.att-div-2').find("input[type='hidden']").remove();
		$('.form-success').delay(5000).fadeOut('slow');
		return true;
	}

	function countSelectedUsers(){
		var singleCheckedCount = 0;
		var totalCount = 0;
		$('.check_user_single').each(function() {
			if($(this).is(':checked')){
				singleCheckedCount = singleCheckedCount + 1;
			}
			totalCount = totalCount + $(this).length;
		});
		var countReturn = [];
		countReturn = {'total':totalCount, 'selected':singleCheckedCount};
		console.log(countReturn);
		return countReturn;
	}

	function loadActivitySection(profile_id, day){
		$.ajax({
			url			: defaultUrl+'activities/loadGridAjax',
			type		: 'POST',
			dataType	: 'html',
			data		: {id: profile_id, day : day},
			success : function(response){
				$('#user_activities').html(response);
				$('#activity_section').html(response);
			}
		});
	}

	/*
	* Contact Modules Javascript/Jquery
	*/

	/*
	* Empty fields on add doctor button click
	*/
	$(document).on('click', '#add-lead-button', function() {
		emptyInputFields();
		removeFlashMessage();
		progressBar();
	});

	/*
	* Add Lead functionality using ajax submission
	* Data is saving under Users, Profile
	*/

	$(document).on('click', '.procedure_view', function() {
		$('.procedure_edit').show();
		$(this).hide();
	});

	$(document).on('click', '.state_view', function() {
		$('.state_edit').show();
		$(this).hide();
	});
		
	$(document).on('click', '.cancelpassword', function() {
		$('.pass_view').show();
		$('.pass_edit').hide();
	});

	$(document).on('click', '.pass_view', function() {
		$('.pass_edit').show();
		$(this).hide();
	});
	
	$(document).on('click', '.state_view', function() {
		$('.state_edit').show();
		$(this).hide();
	});

	$(document).on('click', '.file_upload_2, .file_upload_5', function() {
		$('.file_edit').show();
	});
	

	/*
	* Add Lead Jquery functionality
	*/
	$( document ).on( "submit", '.add_lead', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();
		
		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 

					$form.find( ".btn-default" ).trigger('click');
					afterAdd('<p><strong>'+response.msg+'</strong></p>');
					afterAddRemove();
					
					//$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');

					// Empty all previous fields
					$form.find( ".add_doctor").val('');
					$form.find( "input[type=text], input[type=password], input[type=hidden], textarea" ).val("");
					
					// Empty all Select dropdown
					$form.children().find('.selectpicker option:first-child').attr("selected", "selected");
					$form.children().find('.selectpicker').each(function() {
						$(this).next().children().find('span.filter-option').html($(this).find(':first-child').text());
					});
					$form.find( ":checkbox" ).prop('checked', false);
					
					//Prepend new added data
					$('#contact_grid_tbody').prepend(response.data);
					progressBar();
					removeFlashMessage();
					searchAllAjax();
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	/*
	* Right sidebar Manage Lead functionality
	*/
	$(document).on('click', '.get_right_sidebar_user_leads', function(){ 
		$('table > tbody > tr').removeClass('tr_hover');
		$(this).addClass('tr_hover');
		var id = $(this).attr('id');
		$.ajax({
			url			: ajaxUrl + 'contacts/right_sidebar',
			type		: 'POST',
			dataType	: 'HTML',
			data		: {
				id	: id
			},
			success : function(response){
				$('.nav-pills').find('.ft-rt').removeClass('active');
				$('.nav-pills li:last-child').addClass('active');
				$('#profile_overright').html('').html(response);
			}
		});
	});
	
	/*
	* Add Data from right sidebar 
	* Manage Profile Tab 
	* Update Procedure
	*/
	$( document ).on( "submit", '.updateprofile', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();
		console.log($form);
		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					
					var search_filter_by = $('#contact_search_filter').val();
					var search_filter_key = $('#search_contact_keyword').val();
					searchContactFunction(search_filter_key, search_filter_by);

					setTimeout(function () {

						$('#tr_'+response.user_id).trigger('click');
						$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
						$form.find( "."+response.type+"_view_txt" ).html(response.data);

						$('#tr_'+response.user_id).find(".grid_"+response.type+"").html(response.data);
						$('#step3-tick').removeClass('hide').addClass('show');

						$("."+response.type+"_message").html('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
						$("."+response.type+"_view").show();
						$("."+response.type+"_edit").hide();
						
						progressBar();
						removeFlashMessage();

					}, 3000);

				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					if(response.result == "custom"){
						$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">'+ response.msg +'</div>');
						return false;
					}
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	/*
	* Add Data from right sidebar 
	* step2 
	* Patient Booking
	*/
	$( document ).on( "submit", '.add_lead_step2', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();
		
		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();
		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();

				if(response.result == "success"){
					
					resetContact();
					$form.find( ".save" ).parent().find('.form-success').remove();
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					
					progressBar();
					removeFlashMessage();
					
					$('#step2-tick').removeClass('hide').addClass('show');
					$('#tr_'+response.user_id).trigger('click');

					setTimeout(function () {
						$( ".stage_tab3 a" ).trigger( "click" );	
					}, 2000);
					
					if(response.status == "Client is not interested"){
						$('#tr_'+response.user_id).find(".grid_state").html(response.b_data);
						$('.state_view_txt').html(response.b_data+' <sup><span class="glyphicon glyphicon-edit" aria-hidden="true" style="opacity:0.5"></span></sup>');
					}
					
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	/*
	* Add Data from right sidebar 
	* step3 
	* CREATE LOGIN
	*/
	$( document ).on( "submit", '.add_lead_step3', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();
		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();
				
				
				if(response.result == "success"){
					
					resetContact();
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					$('#step3-tick').removeClass('hide').addClass('show');
					
					progressBar();
					removeFlashMessage();

					$('#user-username').prop('disabled', true);
					$('#user-password').prop('disabled', true);
					$('#user-confirm-password').prop('disabled', true);
					$('.login-txt').html('Patient login has been created already')
					$('#tr_'+response.user_id).trigger('click');

					setTimeout(function () {
						$( ".stage_tab4 a" ).trigger( "click" );
					}, 2000);
					
				
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					if(response.custom){
						$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">'+response.custom+'</div>');
						return false;
					}
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	

	/*
	* Add Data from right sidebar 
	* step4 
	* Patient Form Completed.   // This will be set green tick when patient complete his all forms after login.
	*/
	$( document ).on( "submit", '.add_lead_step4', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();
		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					
					resetContact();
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					$('#step4-tick').removeClass('hide').addClass('show');
					progressBar();
					removeFlashMessage();
					$('#tr_'+response.user_id).trigger('click');
					
					setTimeout(function () {
						$( ".stage_tab5 a" ).trigger( "click" );
					}, 2000);

					
				
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					if(response.custom){
						$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">'+response.custom+'</div>');
						return false;
					}
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	
	/*
	* Add Data from right sidebar 
	* step5 
	* REVIEW PROFILE - R4aC will review patient profile
	*/
	$( document ).on( "submit", '.add_lead_step5', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();
		
		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					resetContact();
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					$('#step5-tick').removeClass('hide').addClass('show');
					progressBar();
					removeFlashMessage();
					$('#tr_'+response.user_id).trigger('click');
					
					setTimeout(function () {
						$( ".stage_tab6 a" ).trigger( "click" );	
					}, 2000);
					

				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});


	/*
	* Add Data from right sidebar 
	* step6 
	* Dr. Notified - R4aC will notifiy doctor about patient completed form.
	*/
	$( document ).on( "submit", '.add_lead_step6', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();
		
		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					
					resetContact();
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					$('#step6-tick').removeClass('hide').addClass('show');
					progressBar();
					removeFlashMessage();
					$('#tr_'+response.user_id).trigger('click');

					setTimeout(function () {
						$( ".stage_tab7 a" ).trigger( "click" );
					}, 2000);
					
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});


	/* Add Data from right sidebar 
	* step7 
	* Dr. Completed - Doctor give comment on patient completed form.
	*/
	$( document ).on( "submit", '.add_lead_step7', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();
		
		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 

					resetContact();
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					$('#step7-tick').removeClass('hide').addClass('show');
					$('#tr_'+response.user_id).trigger('click');
					setTimeout(function () {
						$( ".stage_tab8 a" ).trigger( "click" );
					}, 2000);
					progressBar();
					removeFlashMessage();
					
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});


	/*
	* Add Data from right sidebar 
	* step8 
	* Patient Approved and SCHEDULE PATIENT
	*/
	$( document ).on( "submit", '.add_lead_step8', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();
		
		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					resetContact();
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					$('#step4-tick').removeClass('hide').addClass('show');
					$('#tr_'+response.user_id).trigger('click');
					progressBar();
					removeFlashMessage();
					
					setTimeout(function () {
						$( ".stage_tab9 a" ).trigger( "click" );
					}, 2000);
					
					
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	/*
	* Add Data from right sidebar 
	* step9 
	* Deposit request
	*/
	$( document ).on( "submit", '.add_lead_step9', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					
					resetContact();
					$('#tr_'+response.user_id).find(".tr_surgery_date").html(response.b_data);
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					$('#step9-tick').removeClass('hide').addClass('show');
					progressBar();
					removeFlashMessage();
					$('#tr_'+response.user_id).trigger('click');
					
					setTimeout(function () {
						$( ".stage_tab10 a" ).trigger( "click" );
					}, 2000);

				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	
	/*
	* Add Data from right sidebar 
	* step10 
	* Deposit Paid
	*/
	$( document ).on( "submit", '.add_lead_step10', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					resetContact();
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					$('#step10-tick').removeClass('hide').addClass('show');
					$('#tr_'+response.user_id).find(".tr_surgery_date").html(response.b_data);
					progressBar();
					removeFlashMessage();
					$('#tr_'+response.user_id).trigger('click');

					setTimeout(function () {
						
						$( ".stage_tab11 a" ).trigger( "click" );
					}, 2000);
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	
	/*
	* Add Data from right sidebar 
	* step11 
	* Patietn travel info added
	*/
	$( document ).on( "submit", '.add_lead_step11', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					resetContact();
					$('#tr_'+response.user_id).find(".tr_surgery_date").html(response.b_data);
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					$('#step11-tick').removeClass('hide').addClass('show');
					progressBar();
					removeFlashMessage();
					$('#tr_'+response.user_id).trigger('click');

					setTimeout(function () {
						$( ".stage_tab12 a" ).trigger( "click" );
					}, 2000);
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});
	
	/*
	* Add Data from right sidebar 
	* step12 
	* Patietn travel info added
	*/
	$( document ).on( "submit", '.add_lead_step12', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					resetContact();
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					$('#step12-tick').removeClass('hide').addClass('show');
					progressBar();
					removeFlashMessage();
					$('#tr_'+response.user_id).trigger('click');

					setTimeout(function () {
						$( ".stage_tab13 a" ).trigger( "click" );
					}, 2000);
				}else if (response.result == "not_confirmed"){
					resetContact();
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					progressBar();
					removeFlashMessage();
					$('#tr_'+response.user_id).trigger('click');
					setTimeout(function () {
						$( ".stage_tab13 a" ).trigger( "click" );
					}, 2000);
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	
	/*
	* Add Data from right sidebar 
	* step13
	* Patietn travel info added
	*/
	$( document ).on( "submit", '.add_lead_step13', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					resetContact();
					
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					$('#step13-tick').removeClass('hide').addClass('show');
					progressBar();
					removeFlashMessage();
					$('#tr_'+response.user_id).trigger('click');
					$('#tr_'+response.user_id).find(".tr_surgery_date").html(response.b_data);
					setTimeout(function () {
						$( ".stage_tab14 a" ).trigger( "click" );
					}, 2000);
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	
	/*
	* Add Data from right sidebar 
	* step14
	* Patient Post op info
	*/
	$( document ).on( "submit", '.add_lead_step14', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find( "input[type='submit']" ).prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					resetContact();
					$('#tr_'+response.user_id).trigger('click');
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					$('#step14-tick').removeClass('hide').addClass('show');
					setTimeout(function () {
						$( ".stage_tab14 a" ).trigger( "click" );
					}, 2000);
					progressBar();
					removeFlashMessage();
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	/*
	* Add Data from right sidebar 
	* step15
	* Patient Post op info
	*/
	$( document ).on( "submit", '.add_lead_step15', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/right_sidebar_add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					resetContact();
					$('#tr_'+response.user_id).trigger('click');
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					$('#step15-tick').removeClass('hide').addClass('show');
					setTimeout(function () {
						$( ".stage_tab15 a" ).trigger( "click" );
					}, 2000);
					progressBar();
					removeFlashMessage();
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	/*
	* Delete Users functionality
	*/
	$(document).on('click', '#delete_user', function(){

		var usersId = [];
		$('.check_user_single').each(function() {
			if($(this).is(':checked')){ 
				usersId.push($(this).prop('value'));
			}
		});
		
		if(usersId == ""){
			alert('Select alteast one user first');
			return false;
		}
		
		$.ajax({
			url			: defaultUrl + 'manage/delete',
			type		: 'POST',
			dataType	: 'json',
			data		: {
				ids	: usersId
			},
			success : function(response){
					var msg = response.msg;
					var deletedIds = response.data;

					$.each(deletedIds, function(i, j){
						$('#tr_'+j).remove();
					});

					$('#delete_user_cancel').trigger('click');
					
					//remove delete model box
					removeModal();

					// Select count
					var countReturn = countSelectedUsers();
					$('.selected-user-count').html(countReturn['selected']);
					searchAllAjax();
					console.log(msg);
			}
		});
	});


	/*
	* Add contacts under Owner functionality
	*/

	$(document).on('click', '.owner_user_icon', function() {
		var userCount = countSelectedUsers();
		if(userCount.selected <=0){
			alert('Select atleast one user first');
			return false;
		}
	});
	
	
	$(document).on('change', '.get_owner', function() {
		$('#add_owner_user').attr('data-attr', $(this).val());
	});

	$(document).on('click', '#add_owner_user', function(){
		var obj = $(this);
		var usersId = [];
		$('.check_user_single').each(function() {
			if($(this).is(':checked')){ 
				usersId.push($(this).prop('value'));
			}
		});
		
		if(usersId == ""){
			alert('Select alteast one user first');
			return false;
		}

		$.ajax({
			url			: defaultUrl + 'contacts/assign_owner',
			type		: 'POST',
			dataType	: 'json',
			data		: {
				ids	: usersId,
				sales_rep_id : obj.attr('data-attr')
			},
			success : function(response){
					var msg = response.msg;
					var deletedIds = response.data;

					$.each(deletedIds, function(i, j){
						$('#tr_'+j).children().find('.check_user_single').prop('checked', false);
					});
					
					$('.check_user_all').prop('checked', false);
					$('#owner_user_cancel').trigger('click');
					removeModal();
					
					// Select count
					var countReturn = countSelectedUsers();
					$('.selected-user-count').html(countReturn['selected']);

					searchAllAjax();
					console.log(msg);
			}
		});
	});


	/*
	* Add contacts under Call Queue functionality
	*/

	$(document).on('click', '.call_user_icon', function() {
		var userCount = countSelectedUsers();
		if(userCount.selected <=0){
			alert('Select atleast one user first');
			return false;
		}
	});
	
	$(document).on('click', '#add_call_user', function(){
		var usersId = [];
		$('.check_user_single').each(function() {
			if($(this).is(':checked')){ 
				usersId.push($(this).prop('value'));
			}
		});
		
		if(usersId == ""){
			alert('Select alteast one user first');
			return false;
		}

		$.ajax({
			url			: defaultUrl + 'contacts/assign_call_queue',
			type		: 'POST',
			dataType	: 'json',
			data		: {
				ids	: usersId
			},
			success : function(response){
					var msg = response.msg;
					var deletedIds = response.data;

					$.each(deletedIds, function(i, j){
						$('#tr_'+j).children().find('.check_user_single').prop('checked', false);
					});
					
					$('.check_user_all').prop('checked', false);
					$('#call_user_cancel').trigger('click');
					removeModal();
								
					// Select count
					var countReturn = countSelectedUsers();
					$('.selected-user-count').html(countReturn['selected']);
					console.log(msg);
					searchAllAjax();
			}
		});
	});

	/*
	* Update user status functionality
	*/

	$(document).on('click', '.dead_user_icon', function() {
		var userCount = countSelectedUsers();
		if(userCount.selected <=0){
			alert('Select atleast one user first');
			removeModal();
			return false;
		}
	});
	
	$(document).on('change', '.get_owner_status', function() {
		$('#add_dead_user').attr('data-attr', $(this).val());
	});


	$(document).on('click', '#add_dead_user', function(){
		var obj = $(this);
		var usersId = [];
		$('.check_user_single').each(function() {
			if($(this).is(':checked')){ 
				usersId.push($(this).prop('value'));
			}
		});
		
		if(usersId == ""){
			alert('Select alteast one user first');
			return false;
		}
		
		$.ajax({
			url			: defaultUrl + 'contacts/update_user_status',
			type		: 'POST',
			dataType	: 'json',
			data		: {
				ids	: usersId,
				status : obj.attr('data-attr')
			},
			success : function(response){
					var msg = response.msg;
					var deletedIds = response.data;
					
					$.each(deletedIds, function(i, j){
						console.log('#tr_'+j);
						$('#tr_'+j).children().find('.check_user_single').prop('checked', false);
						$('#tr_'+j).trigger('click');
					});
					
					$('.check_user_all').prop('checked', false);
					$('#dead_user_cancel').trigger('click');
					removeModal();

					// Select count
					var countReturn = countSelectedUsers();
					$('.selected-user-count').html(countReturn['selected']);
					searchAllAjax();
			}
		});
	});


	/*
	* Add Doctor functionality using ajax submission
	*/
	$( document ).on( "submit", '.add_note', function( event ) {
		
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();
		$(this).find("input[type='submit']").prop('disabled',true);
		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'activities/add/',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled',false);

				if(response.result == "success"){
					// If success 
					// Display success message 
					resetContact();
					$form.find( ".save" ).parent().find('.form-success').remove();
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">Note added successfully.</div>');
					
					//$('#step2-tick').removeClass('hide').addClass('show');
					
					$('#tr_'+response.user_id).trigger('click');
					setTimeout(function () {
						$( ".stage_tab1 a" ).trigger( "click" );
						$( ".stage_tab1 a" ).trigger( "click" );
						if(response.type == "call"){
							$('#step1 a[href="#log-call"]').tab('show');						
						}else{
							$('#step1 a[href="#new-note"]').tab('show');
						}
					}, 2000);


					// Empty all previous fields
					$form.find( "textarea" ).val("");
					$('.notes_timeline').prepend(response.data);
					// Remove msg
					progressBar();
					removeFlashMessage();
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								console.log(i+'-'+k+'___'+n);
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");

								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});
	
	/*
	* Patient Section Task
	*/
	$( document ).on( "submit", '.add_patient_task', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'contacts/add_task',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					resetContact();
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');

					// Empty all previous fields
					$form.find( ".add_doctor").val('');
					$form.find( "input[type=text], input[type=password], input[type=hidden], textarea" ).val("");
					$form.find( ":checkbox" ).prop('checked', false);
					
					// Empty all Select dropdown
					$form.children().find('.selectpicker option:first-child').attr("selected", "selected");
					$form.children().find('.selectpicker').each(function() {
						$(this).next().children().find('span.filter-option').html($(this).find(':first-child').text());
					});
					
					$('.notes_timeline').prepend(response.data);
					//$('#myModal').modal('toggle');
					
					//$('#step2-tick').removeClass('hide').addClass('show');
					
					$('#tr_'+response.user_id).trigger('click');
					setTimeout(function () {
						$( ".stage_tab1 a" ).trigger( "click" );
						$( ".stage_tab1 a" ).trigger( "click" );
						$('#step1 a[href="#new-task"]').tab('show');
					}, 2000);

					loadTaskGrid();
					//progressBar();
					//removeFlashMessage();
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Please fill all required fields.</div>');
					
					//console.log(response);
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								console.log(i+'-'+k+'___'+n);
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								$('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
								$('#'+i+'-'+k).parent().parent().parent('.form-group').addClass('has-error');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	/*
	* Add Doctor functionality using ajax submission
	*/
	$( document ).on( "submit", '.add_note_result', function( event ) {
		
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();
		$(this).find("input[type='submit']").prop('disabled',true);
		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'activities/add_interest/',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled',false);

				if(response.result == "success"){
					// If success 
					// Display success message 
					resetContact();
					$form.find( ".save" ).parent().find('.form-success').remove();
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">Note added successfully.</div>');
					
					$('#step2-tick').removeClass('hide').addClass('show');
					
					$('#tr_'+response.user_id).trigger('click');
					setTimeout(function () {
						$( ".stage_tab2 a" ).trigger( "click" );
					}, 2000);


					// Empty all previous fields
					$form.find( "textarea" ).val("");
					$('.notes_timeline').prepend(response.data);
					// Remove msg
					progressBar();
					removeFlashMessage();
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								console.log(i+'-'+k+'___'+n);
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");

								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});


	$( document ).on( "submit", '.add_activity_task', function( event ) {
		
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'tasks/add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');

					// Empty all previous fields
					$form.find( ".add_doctor").val('');
					$form.find( "input[type=text], input[type=password], input[type=hidden], textarea" ).val("");
					$form.find( ":checkbox" ).prop('checked', false);
					
					// Empty all Select dropdown
					$form.children().find('.selectpicker option:first-child').attr("selected", "selected");
					$form.children().find('.selectpicker').each(function() {
						$(this).next().children().find('span.filter-option').html($(this).find(':first-child').text());
					});
					
					$('.notes_timeline').prepend(response.data);
					//$('#myModal').modal('toggle');

					loadTaskGrid();
					//progressBar();
					//removeFlashMessage();
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Please fill the required fields.</div>');
					
					//console.log(response);
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								console.log(i+'-'+k+'___'+n);
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								$('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	function loadTaskGrid(){
		$.ajax({
			url			: defaultUrl + 'tasks/loadGridAjax',
			//type		: 'POST',
			dataType	: 'html',
			//data		: $( '.frm_task' ).serialize(),
			success : function(response){
				$('#task_grid_tbody').html(response);
			}
		});
}


	/*
	*************************************************************
	*******************ADMIN MODULE (MANAGE PEOPLE)**************
	*************************************************************
	*/

	
	/*
	* Add Doctor functionality using ajax submission
	*/
	$( document ).on( "blur", ".add_doctor", function(){
		var formName = $(this).closest( "form" ).attr('class');
		//$('.'+formName).submit();
	});
	$( document ).on( "submit", '.add_people', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'manage/add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					
					$form.find( ".btn-default" ).trigger('click');
					afterAdd('<p><strong>'+response.msg+'</strong></p>');
					afterAddRemove();
					
					//$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');

					// Empty all previous fields
					$form.find( ".add_doctor").val('');
					$form.find( "input[type=text], input[type=password], input[type=hidden], textarea" ).val("");
					$form.find( ":checkbox" ).prop('checked', false);
					
					// Empty all Select dropdown
					$form.children().find('.selectpicker option:first-child').attr("selected", "selected");
					$form.children().find('.selectpicker').each(function() {
						$(this).next().children().find('span.filter-option').html($(this).find(':first-child').text());
					});

					//Prepend new added data
					//$('#doctor_grid_tbody').prepend(response.data);
					$('#manage_grid_tbody').prepend(response.data);
					progressBar();
					removeFlashMessage();
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');

					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								console.log(i+'-'+k+'___'+n);
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");

								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});
	
	
	/*
	* Right sidebar functionality
	*/
	$(document).on('click', '.get_right_sidebar_people', function(){ 
		$('table > tbody > tr').removeClass('tr_hover');
		$(this).addClass('tr_hover');
		var id = $(this).attr('id');
		$.ajax({
			url			: defaultUrl + 'manage/right_sidebar',
			type		: 'POST',
			dataType	: 'HTML',
			data		: {
				id	: id
			},
			success : function(response){
				$('#right-sidebar').html(response);
			}
		});
	});





/*
*	Patient Panel
*/


jQuery(document).ready(function($){
	$('#profile-ft, #profile-in, #profile-weight').on( 'keypress', function( e ){
		// Ensure that it is a number and stop the keypress
		var key = e.which || e.charCode || e.keyCode;
		if (e.shiftKey || (key < 48 || key > 57)) {
			if(key == 8  || key == 46) {
			} else {
				e.preventDefault();
			}
		}
	});

	$('#profile-ft, #profile-in, #profile-weight').change(function() { 
		var pattern = /^\d+$/;

		var weight = $('#profile-weight').val();
		var heightft = $('#profile-ft').val();
		var heightin = $('#profile-in').val();
		//console.log(weight+"__"+heightft+"__"+heightin);
		/*
		if(!pattern.test(heightft))
		{
		 alert("Please enter integers only");
		 $('#profile-ft').focus();
		  return false;
		}
		if(!pattern.test(heightin))
		{
		  alert("Please enter integers only");
		  $('#profile-in').focus();
		  return false;
		}
		*/
		
		if(weight != ''){
			if(!pattern.test(weight))
			{
			  alert("Please enter integers only");
			  $('#profile-weight').focus();
			  return false;
			}
		}
		
		var height = heightft*12 + +heightin;
		 

		 if(weight != '' && heightft != '' && heightin != '') {
			var bmi = Math.round((weight /(height*height)) * 703.06957964);
			$('#profile-bmi').val(bmi);
		 }else{
			$('#profile-bmi').val('0');
			return false;
		 }	
		 
		 
	});	 
});

function medicalTab(showTab){
	switch(showTab){
		case 1:
				$('.first-step-form').css({'display':'block'});
				$('.second-step-form').css({'display':'none'});
				$('.third-step-form').css({'display':'none'});
				$('.fourth-step-form').css({'display':'none'});
				$('.five-step-form').css({'display':'none'});
				$("html, body").animate({ scrollTop: 0 }, "slow");
			break;
		case 2:
				$('.first-step-form').css({'display':'none'});
				$('.second-step-form').css({'display':'block'});
				$('.third-step-form').css({'display':'none'});
				$('.fourth-step-form').css({'display':'none'});
				$('.five-step-form').css({'display':'none'});
				$("html, body").animate({ scrollTop: 0 }, "slow");
			break;
		case 3:
				$('.first-step-form').css({'display':'none'});
				$('.second-step-form').css({'display':'none'});
				$('.third-step-form').css({'display':'block'});
				$('.fourth-step-form').css({'display':'none'});
				$('.five-step-form').css({'display':'none'});
				$("html, body").animate({ scrollTop: 0 }, "slow");
			break;
		case 4:
				$('.first-step-form').css({'display':'none'});
				$('.second-step-form').css({'display':'none'});
				$('.third-step-form').css({'display':'none'});
				$('.fourth-step-form').css({'display':'block'});
				$('.five-step-form').css({'display':'none'});
				$("html, body").animate({ scrollTop: 0 }, "slow");
			break;
		case 5:
				$('.first-step-form').css({'display':'none'});
				$('.second-step-form').css({'display':'none'});
				$('.third-step-form').css({'display':'none'});
				$('.fourth-step-form').css({'display':'none'});
				$('.five-step-form').css({'display':'block'});
				//$("html, body").animate({ scrollTop: 0 }, "slow");
			break;
	}
}

$('.custom-next').click(function() {
	var stepNo = $(this).attr('data-attr');
	var step = parseInt(stepNo) + 1;
	medicalTab(step);
	return false;
});

/*Save patient info ajax login as patient module under medical history tab */
$(document).on('submit', '.patient_info', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();
	

	if($('#profile-weight').val() == '' && ($('#profile-ft').val() != '' && $('#profile-in').val() != '')){
		alert("Weight is required field");
		$('#profile-weight').focus();
		event.preventDefault();
		return false;	
	}


	var $form = $(this);
	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	event.preventDefault();
		
	// Ajax request started
	$.ajax({
		url			: ajaxUrl + 'contacts/add', // OOO
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
				
				//updateInfo
				$('#patient_info_firstname').html($('#profile-firstname').val());
				$('#patient_info_lastname').html($('#profile-lastname').val());
				$('#patient_info_email').html($('#user-email').val());
				$('#patient_info_phone').html($('#profile-phone').val());
				$('#patient_info_procedure').html($('#profile-procedure-id :selected').text());
				$('#patient_info_doctor').html($('#profile-doctor-id :selected').text());

				medicalTab(2);
				progressBar();
				removeFlashMessage();
			}else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});
/*Save Pay Deposit ajax login as patient module under medical Pay Deposit */

$(document).on('submit', '.patient_pay_deposit', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	event.preventDefault();

	// Ajax request started
	$.ajax({
		url			: defaultUrl  + 'payments/process', // OOO
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');

				$('#patient_info_schedule').html($('#payment-surgery-date').val());
				$('#patient_info_cost').html($('#payment-amount').val());
				$('#patient_info_payment_status').html('Yes');			

				medicalTab(3);
				progressBar();
				removeFlashMessage();
			}else if(response.result == "failed"){					
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">'+response.msg+'</div>');
					$('#patient_info_payment_status').html('No');		
			}else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});
/*Save MEDICAL HISTORY ajax login as patient module under medical history tab */
$(document).on('submit', '.patient_major_illnesses', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	event.preventDefault();

	// Ajax request started
	$.ajax({
		url			: ajaxUrl + 'contacts/major_illnesses', // OOO
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
				medicalTab(4);
				progressBar();
				removeFlashMessage();
			}else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});

$(document).on('submit', '.patient_major_illnesses_dr', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	event.preventDefault();

	// Ajax request started
	$.ajax({
		url			: ajaxUrl + 'contacts/major_illnesses_dr', // OOO
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
				medicalTab(4);
				progressBar();
				removeFlashMessage();
			} else if (response.result == "custom"){
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">'+ response.msg +'</div>');
				return false;
			} else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});
/*Save DIET HISTORY ajax login as patient module under medical history tab */
$(document).on('submit', '.patient_diet_history', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	event.preventDefault();

	// Ajax request started
	$.ajax({
		url			: ajaxUrl + 'contacts/patient_diet_history', // OOO
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
				medicalTab(5);
				progressBar();
				removeFlashMessage();
			}else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});
/*Save DIET HISTORY commennt save ajax login as doctor module under medical history tab */

$(document).on('submit', '.patient_diet_history_dr', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	event.preventDefault();

	// Ajax request started
	$.ajax({
		url			: ajaxUrl + 'contacts/patient_diet_history_dr', // OOO
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
				medicalTab(5);
				progressBar();
				removeFlashMessage();
			} else if (response.result == "custom"){
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">'+ response.msg +'</div>');
				return false;
			} else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});
/*save PATIENT INFORMATION comment ajax log in as doctor under medical history tab */

$(document).on('submit', '.patient_info_dr', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	event.preventDefault();

	// Ajax request started
	$.ajax({
		url			: ajaxUrl + 'contacts/add',
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
				medicalTab(2);
				progressBar();
				removeFlashMessage();
			}else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});
/*Save Pre-Op Diet ajax login as patient module under Pre-Op info tab */


$(document).on('submit', '.pre_op_info', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();
	$('#label_is_accept_info').removeClass('has-error');
	var $form = $(this);
	
	if(!$('#patientpreopinfo-is-accept-info').is(":checked")){
		$('#label_is_accept_info').addClass('has-error');
		$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Please accept the acknowledgement.</div>');
		event.preventDefault();
		return false;
	}

	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	event.preventDefault();
	// Ajax request started
	$.ajax({
		url			: ajaxUrl + 'contacts/pre_op_info',
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
				//medicalTab(2);
				progressBar();
				removeFlashMessage();
			}else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});

function validateEmpty(form){
	var empty = false;
	$(form).find(".empty").each(function() {
		 if($(this).val()!=''){
			empty = true;
			return false;
		 }
	});
	
	return empty;
}
/*Save Add Travel info ajax login as patient module under  Add Travel info tab*/
$(document).on('submit', '.patient_travel_info', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	event.preventDefault();

	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		
	// Ajax request started
	$.ajax({
		url			: ajaxUrl + 'contacts/patient_travel_info',
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
				
				$('#patient_info_travel_date').html($('#patienttravelinfo-departure-date').val());
				$('#patient_info_ticket_no').html($('#patienttravelinfo-ticket-number').val());
				$('#patient_info_trip_driver').html($('#patienttravelinfo-driver-name').val());
				
				if(response.data){
					$('.patient_status_dr_view').html(response.data);
				}
				progressBar();
				removeFlashMessage();

			}else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							$form.find('#'+i+'-'+k).parent().parent().parent('.form-group').addClass('has-error');
						});
					});
				});
			}
			return false;
		}
	});

});
/*Save Add Travel info ajax login as patient post_op info tab */ 
$(document).on('submit', '.patient_travel_information', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	event.preventDefault();

	if(validateEmpty($form)){
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		
		// Ajax request started
		$.ajax({
			url			: ajaxUrl + 'contacts/patient_post_op_info',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){

				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
					//medicalTab(2);
					progressBar();
					removeFlashMessage();
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");
								console.log(i+'-'+k+'___'+n);
								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
								$form.find('#'+i+'-'+k).parent().parent().parent('.form-group').addClass('has-error');
							});
						});
					});
				}
				return false;
			}
		});
	}
	return false;
});
/*Save REVIEW OF SYSTEMS ajax login as patient module under medical history tab */
$(document).on('submit', '.patient_review_system_dr', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	event.preventDefault();
	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	// Ajax request started
	$.ajax({
		url			: ajaxUrl + 'contacts/patient_review_system_dr',
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
				$('.patient_status_dr_view').html(response.data);
				medicalTab(5);
				progressBar();
				removeFlashMessage();
			} else if (response.result == "custom"){
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">'+ response.msg +'</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
				return false;
			} else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});

/*Save CURRENT MEDICATION ajax login as patient module under medical history tab */
$(document).on('submit', '.patient_medication', function(event){

	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	event.preventDefault();

	// Ajax request started
	$.ajax({
		url			: ajaxUrl + 'contacts/patient_medication',
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
				medicalTab(3);
				progressBar();
				removeFlashMessage();
			}else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});
/*save CURRENT MEDICATION comment ajx log in as doctor under medical history tab*/
$(document).on('submit', '.patient_medication_dr', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	event.preventDefault();

	// Ajax request started
	$.ajax({
		url			: ajaxUrl + 'contacts/add_medication_comment',
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
				medicalTab(3);
				progressBar();
				removeFlashMessage();
			}else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">'+response.msg+'</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});

$(document).on('submit', '.patient_medical_history', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	event.preventDefault();

	// Ajax request started
	$.ajax({
		url			: defaultUrl + 'contacts/patient_allergic',
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
				//medicalTab(3);
				progressBar();
				removeFlashMessage();
			}else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});

$(document).on('submit', '.patient_review_system', function(event){
	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	event.preventDefault();

	// Ajax request started
	$.ajax({
		url			: ajaxUrl + 'contacts/patient_review_system',
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){

			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');
				medicalTab(5);
				progressBar();
				removeFlashMessage();
			}else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							$form.find('#'+i+'-'+k).parent().parent().parent('.form-group').addClass('has-error');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});


$( document ).on( "submit", '.update_doc_profile', function( event ) {

	// Remove messages if any
	$('.form-success').remove();
	$('.form-error').remove();

	var $form = $(this);
	$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
	event.preventDefault();

	// Ajax request started
	$.ajax({
		url			: defaultUrl+ 'doctors/right_sidebar_add',
		type		: 'POST',
		dataType	: 'json',
		data		: $( this ).serialize(),
		success : function(response){
			// Remove all error class and label first
			$form.find( ".form-group" ).removeClass( "has-error" );
			$form.find( ".runtime-label" ).remove();
			$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

			if(response.result == "success"){
				// If success 
				// Display success message 
				$('#tr_'+response.user_id).trigger('click');
				$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');

				$("."+response.type+"_view").show();
				$("."+response.type+"_edit").hide();
				
				progressBar();
				removeFlashMessage();
			}else{
				// If Getting Errors
				// Display error message
				$form.find( ".save" ).parent().find('.form-error').remove();
				if(response.result == "custom"){
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">'+ response.msg +'</div>');
					return false;
				}
				$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');
				$.each(response, function(i, j){
					$.each(j, function(k, l){
						$.each(l, function(m, n){
							// If any name is consisting or "_", Remove and replace with "-";
							var j = k.split("_");
							k = j.join("-");
							
							console.log(i+'-'+k+'___'+n);
							$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
							$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
						});
					});
				});
			}
			return false;
		}
	});
	return false;
});


$(document).ready(function() {
	$('.doc_file_upload_cancel').click(function() {
		hide_fileUpload();
		return false;
	});
});


$(document).on( 'click', 'input[type="reset"]', function(){
	$(this).closest('form').find('input[type=text],select,textarea').each(function(){
		$(this).val('');
	});
	return false;
});



$(document).on('click', '#patientreviewsystems-is-fatigue-0', function() {
	$('#patientreviewsystems-fatigue-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-fatigue-1', function() {
	$('#patientreviewsystems-fatigue-detail').prop('disabled', false);
});

$(document).on('click', '#patientreviewsystems-is-weakness-0', function() {
	$('#patientreviewsystems-weakness-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-weakness-1', function() {
	$('#patientreviewsystems-weakness-detail').prop('disabled', false);
});

$(document).on('click', '#patientreviewsystems-is-night-sweats-0', function() {
	$('#patientreviewsystems-night-sweats-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-night-sweats-1', function() {
	$('#patientreviewsystems-night-sweats-detail').prop('disabled', false);
});

$(document).on('click', '#patientreviewsystems-is-headache-0', function() {
	$('#patientreviewsystems-headache-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-headache-1', function() {
	$('#patientreviewsystems-headache-detail').prop('disabled', false);
});

/* Start */
$(document).on('click', '#patientreviewsystems-is-head-injury-0', function() {
	$('#patientreviewsystems-head-injury-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-head-injury-1', function() {
	$('#patientreviewsystems-head-injury-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-hearing-problem-0', function() {
	$('#patientreviewsystems-hearing-problem-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-hearing-problem-1', function() {
	$('#patientreviewsystems-hearing-problem-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-ear-pain-0', function() {
	$('#patientreviewsystems-ear-pain-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-ear-pain-1', function() {
	$('#patientreviewsystems-ear-pain-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-nasal-conjestion-0', function() {
	$('#patientreviewsystems-nasal-conjestion-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-nasal-conjestion-1', function() {
	$('#patientreviewsystems-nasal-conjestion-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-chronic-sinus-conjestion-0', function() {
	$('#patientreviewsystems-chronic-sinus-conjestion-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-chronic-sinus-conjestion-1', function() {
	$('#patientreviewsystems-chronic-sinus-conjestion-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-bloody-nose-0', function() {
	$('#patientreviewsystems-bloody-nose-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-bloody-nose-1', function() {
	$('#patientreviewsystems-bloody-nose-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-dental-problem-0', function() {
	$('#patientreviewsystems-dental-problem-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-dental-problem-1', function() {
	$('#patientreviewsystems-dental-problem-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-dentures-0', function() {
	$('#patientreviewsystems-dentures-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-dentures-1', function() {
	$('#patientreviewsystems-dentures-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-sores-0', function() {
	$('#patientreviewsystems-sores-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-sores-1', function() {
	$('#patientreviewsystems-sores-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-wheezing-0', function() {
	$('#patientreviewsystems-wheezing-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-wheezing-1', function() {
	$('#patientreviewsystems-wheezing-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-coughing-0', function() {
	$('#patientreviewsystems-coughing-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-coughing-1', function() {
	$('#patientreviewsystems-coughing-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-breast-lump-0', function() {
	$('#patientreviewsystems-breast-lump-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-breast-lump-1', function() {
	$('#patientreviewsystems-breast-lump-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-heart-murmur-0', function() {
	$('#patientreviewsystems-heart-murmur-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-heart-murmur-1', function() {
	$('#patientreviewsystems-heart-murmur-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-high-blood-pressure-0', function() {
	$('#patientreviewsystems-high-blood-pressure-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-high-blood-pressure-1', function() {
	$('#patientreviewsystems-high-blood-pressure-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-chest-pain-0', function() {
	$('#patientreviewsystems-chest-pain-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-chest-pain-1', function() {
	$('#patientreviewsystems-chest-pain-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-sexually-disease-0', function() {
	$('#patientreviewsystems-sexually-disease-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-sexually-disease-1', function() {
	$('#patientreviewsystems-sexually-disease-detail').prop('disabled', false);
});
/* End */
/* Start */
$(document).on('click', '#patientreviewsystems-is-birth-control-0', function() {
	$('#patientreviewsystems-birth-control-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-birth-control-1', function() {
	$('#patientreviewsystems-birth-control-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-infertility-0', function() {
	$('#patientreviewsystems-infertility-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-infertility-1', function() {
	$('#patientreviewsystems-infertility-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-anemia-0', function() {
	$('#patientreviewsystems-anemia-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-anemia-1', function() {
	$('#patientreviewsystems-anemia-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-blood-transfusion-0', function() {
	$('#patientreviewsystems-blood-transfusion-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-blood-transfusion-1', function() {
	$('#patientreviewsystems-blood-transfusion-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-bleeding-tendency-0', function() {
	$('#patientreviewsystems-bleeding-tendency-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-bleeding-tendency-1', function() {
	$('#patientreviewsystems-bleeding-tendency-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-convulsions-0', function() {
	$('#patientreviewsystems-convulsions-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-convulsions-1', function() {
	$('#patientreviewsystems-convulsions-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-paralysis-0', function() {
	$('#patientreviewsystems-paralysis-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-paralysis-1', function() {
	$('#patientreviewsystems-paralysis-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-numbness-0', function() {
	$('#patientreviewsystems-numbness-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-numbness-1', function() {
	$('#patientreviewsystems-numbness-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-memory-loss-0', function() {
	$('#patientreviewsystems-memory-loss-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-memory-loss-1', function() {
	$('#patientreviewsystems-memory-loss-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-depression-0', function() {
	$('#patientreviewsystems-depression-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-depression-1', function() {
	$('#patientreviewsystems-depression-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-anxiety-0', function() {
	$('#patientreviewsystems-anxiety-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-anxiety-1', function() {
	$('#patientreviewsystems-anxiety-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-mood-swings-0', function() {
	$('#patientreviewsystems-mood-swings-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-mood-swings-1', function() {
	$('#patientreviewsystems-mood-swings-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-sleep-problems-0', function() {
	$('#patientreviewsystems-sleep-problems-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-sleep-problems-1', function() {
	$('#patientreviewsystems-sleep-problems-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-alcohol-abuse-0', function() {
	$('#patientreviewsystems-alcohol-abuse-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-alcohol-abuse-1', function() {
	$('#patientreviewsystems-alcohol-abuse-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-chronic-0', function() {
	$('#patientreviewsystems-chronic-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-chronic-1', function() {
	$('#patientreviewsystems-chronic-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-asthma-0', function() {
	$('#patientreviewsystems-asthma-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-asthma-1', function() {
	$('#patientreviewsystems-asthma-detail').prop('disabled', false);
});
/* End */

/* Start */
$(document).on('click', '#patientreviewsystems-is-hay-fever-0', function() {
	$('#patientreviewsystems-hay-fever-detail').prop('disabled', true);
});
$(document).on('click', '#patientreviewsystems-is-hay-fever-1', function() {
	$('#patientreviewsystems-hay-fever-detail').prop('disabled', false);
});
/* End */


$(window).load(function() {
	var d = new Date();

	var month = d.getMonth()+1;
	var day = d.getDate();

	var output = ((''+month).length<2 ? '0' : '') + month + '-' + ((''+day).length<2 ? '0' : '') + day + '-' + d.getFullYear();

	$('.patientCalander').each(function(){
		//console.log($(this).val());
		if("01-01-1970" == $(this).val()){
			//$(this).val(output);
		}
	});
});

function explode(){
	$('.fc-day').each(function() {
		//$(':first-child', this).css("min-height", "101px"); 
	});
}
setTimeout(explode, 2000);



$(document).on('click', '.edit-doctor-button', function() {
	var user_id = $(this).attr('data-attr-id');
	$('.accordion_edit').html('');
	console.log(user_id);
	$.ajax({
		url			: defaultUrl + 'users/edit_doctor',
		type		: 'POST',
		dataType	: 'html',
		data		: {
			id : user_id
		},
		success : function(response){
			$('.accordion_edit').html(response);		
		}
	});
	return false;
});

$(document).on('mouseover', '.get_right_sidebar_user', function() {
		$(this).find('.edit-doctor-button').show();
});
$(document).on('mouseout', '.get_right_sidebar_user', function() {
		$(this).find('.edit-doctor-button').hide();
});


$(document).on('mouseover', '.get_right_sidebar_people', function() {
		$(this).find('.edit-user-button').show();
		$(this).find('.login_as_user_button').show();
});
$(document).on('mouseout', '.get_right_sidebar_people', function() {
		$(this).find('.edit-user-button').hide();
		$(this).find('.login_as_user_button').hide();
});


$(document).on('click', '.edit-user-button', function() {
	var user_id = $(this).attr('data-attr-id');
	$('.accordion_edit').html('');
	console.log(user_id);
	$.ajax({
		url			: defaultUrl + 'users/edit_user',
		type		: 'POST',
		dataType	: 'html',
		data		: {
			id : user_id
		},
		success : function(response){
			$('.accordion_edit').html(response);		
		}
	});
	return false;
});


$(document).ready(function() {	

	$('.add_more_medicus').click(function() {
		cmm++;
		var inputMedi = '<tr>';
			inputMedi += '<td class="col-md-3"><div class="input text"><input type="text" name="PatientCurrentMedications['+cmm+'][name]" class="form-control add_doctor" id="patientcurrentmedications-'+cmm+'-name" value=""></div></td>';
			inputMedi += '<td class="col-md-3"><div class="input text"><input type="text" name="PatientCurrentMedications['+cmm+'][dose]" class="form-control add_doctor" id="patientcurrentmedications-'+cmm+'-dose" value=""></div></td>';
			inputMedi += '<td class="col-md-2"><div class="input text"><input type="text" name="PatientCurrentMedications['+cmm+'][period]" class="form-control add_doctor" id="patientcurrentmedications-'+cmm+'-period" value=""></div></td>';
			inputMedi += '<td class="col-md-2"><div class="input text"><input type="text" name="PatientCurrentMedications['+cmm+'][purpose]" class="form-control add_doctor" id="patientcurrentmedications-'+cmm+'-purpose" value=""></div></td>';
			inputMedi += '<td class="col-md-2" style="position:relative;">';
				inputMedi += '<div class="input-group date">';
					inputMedi += '<div class="input text"><input type="text" name="PatientCurrentMedications['+cmm+'][started]" class="form-control add_doctor patientCalander" placeholder="mm-dd-yyyy" id="patientcurrentmedications-'+cmm+'-started" value=""></div>';
					inputMedi += '<span class="input-group-addon calendar-icon">';
						inputMedi += '<span class="glyphicon glyphicon-calendar"></span>';
					inputMedi += '</span>';
				inputMedi += '</div>';
				inputMedi += '<input type="hidden" name="PatientCurrentMedications['+cmm+'][id]" value="">';
				inputMedi += '<i class="glyphicon glyphicon-minus remove_patient_row" data-attr-type = "cmm" data-attr="'+cmm+'" style="position: absolute; right: -7px;top: 19px; cursor:pointer;">';
				inputMedi += '</td>';
		inputMedi += '</tr>';

		$('.cm_medication').append(inputMedi);

		$('.patientCalander').datetimepicker({
			format: 'MM-DD-YYYY'
		});
	});
	//List of Any Major Illnesses
	$('.add_more_major_illness').click(function() {
		mhmi++;
		//mhmi = medical history major illness
		var inputMajorIllness = '<tr>';
		inputMajorIllness +=	'<td class="col-md-2"><input type="hidden" value="" name="PatientMajorIllnesses['+mhmi+'][id]">';		
		inputMajorIllness += '<div class="input-group date">';
		inputMajorIllness +=	'<div class="input text"><input type="text" value="" id="patientmajorillnesses-'+mhmi+'-date" class="form-control patientCalander " tabindex="1" name="PatientMajorIllnesses['+mhmi+'][date]"></div><span class="input-group-addon calendar-icon"><span class="glyphicon glyphicon-calendar"></span></span>'
		inputMajorIllness += '</div>';
		inputMajorIllness += '</td>';
		inputMajorIllness +=	'<td class="col-md-3">';
		inputMajorIllness += '<div class="input text"><input type="text" value="" id="patientmajorillnesses-'+mhmi+'-illnesses" class="form-control " tabindex="2" name="PatientMajorIllnesses['+mhmi+'][illnesses]"></div></td>';
		inputMajorIllness += '<td class="col-md-3">';
		inputMajorIllness += '<div class="input text"><input type="text" value="" id="patientmajorillnesses-'+mhmi+'-treatment" class="form-control add_doctor" tabindex="3" name="PatientMajorIllnesses['+mhmi+'][treatment]"></div></td>';
		inputMajorIllness += '<td style="position:relative;" class="col-md-3">';
		inputMajorIllness += '<div class="input text"><input type="text" value="" id="patientmajorillnesses-'+mhmi+'-outcome" class="form-control add_doctor" tabindex="4" name="PatientMajorIllnesses['+mhmi+'][outcome]"></div>';
		inputMajorIllness += '</td>';
		inputMajorIllness += '<td><i class="glyphicon glyphicon-minus remove_patient_row" data-attr-type = "mhmi" data-attr-id="" data-attr="0" style="cursor:pointer;"></i></td>';
		inputMajorIllness += '</tr>';

		$('.illness_grid').append(inputMajorIllness);
		
		$('.patientCalander').datetimepicker({
			format: 'MM-DD-YYYY'
		});

		$(":input").each(function (i) { 
			$(this).attr('tabindex', i + 1); 
		});
	});

		//List of Any Surgeries
	$('.add_history_surgeries').click(function() {
		//mhps = medical history patient surgeries
		mhps++;
		var inputHistorySurgery = '<tr>';
		inputHistorySurgery += '<td class="form-group col-md-2 pad-right-0"><input type="hidden" value="" name="PatientSurgery['+mhps+'][id]">		<div class="input-group date">';
		inputHistorySurgery += '<div class="input text"><input type="text" value="" id="patientsurgery-'+mhps+'-date" class="form-control add_doctor patientCalander" tabindex="21" name="PatientSurgery['+mhps+'][date]"></div><span class="input-group-addon calendar-icon"><span class="glyphicon glyphicon-calendar"></span></span>';
		inputHistorySurgery += '</div>';
		inputHistorySurgery += '</td>';
		inputHistorySurgery += '<td class="form-group col-md-3 pad-right-0">';
		inputHistorySurgery += '<div class="input text"><input type="text" value="" id="patientsurgery-'+mhps+'-surgery" class="form-control add_doctor" tabindex="22" name="PatientSurgery['+mhps+'][surgery]"></div></td>';
		inputHistorySurgery += '<td class="form-group col-md-3 pad-right-0">';
		inputHistorySurgery += '<div class="input textarea"><textarea rows="5" id="patientsurgery-'+mhps+'-reason" style="height:34px;width:400px;" class="form-control add_doctor" tabindex="23" name="PatientSurgery['+mhps+'][reason]"></textarea></div>	</td>';
		inputHistorySurgery += '<td><i class="glyphicon glyphicon-minus remove_patient_row" data-attr-type = "mhps" data-attr-id="" data-attr="" style="cursor:pointer;"></i></td>';
		inputHistorySurgery += '</tr>';

		$('.history_surgeries').append(inputHistorySurgery);
		

		$('.patientCalander').datetimepicker({
			format: 'MM-DD-YYYY'
		});

		$(":input").each(function (i) { 
			$(this).attr('tabindex', i + 1); 
		});
	});

		//List of Family History
	$('.add_more_mh_family_history').click(function() {
		//mhfh = medical history family history
		mhfh++;
		var inputFamilyHistory = '<tr>';
		inputFamilyHistory += '<td class="form-group col-md-3 pad-right-0"><input type="hidden" value="" name="PatientFamilyHistory['+mhfh+'][id]"><div class="input text"><input type="text" value="" id="patientfamilyhistory-'+mhfh+'-family-member" class="form-control" tabindex="30" name="PatientFamilyHistory['+mhfh+'][family_member]"></div></td>';
		inputFamilyHistory += '<td class="form-group col-md-2 pad-right-0">';
		inputFamilyHistory += '<div class="input text"><input type="text" value="" id="patientfamilyhistory-'+mhfh+'-age-now" class="form-control" tabindex="31" name="PatientFamilyHistory['+mhfh+'][age_now]"></div></td>';
		inputFamilyHistory += '<td class="form-group col-md-3 pad-right-0">';
		inputFamilyHistory += '<div class="input text"><input type="text" value="" id="patientfamilyhistory-'+mhfh+'-cause-death" class="form-control" tabindex="32" name="PatientFamilyHistory['+mhfh+'][cause_death]"></div></td>';
		inputFamilyHistory += '<td class="form-group col-md-3 pad-right-0">';
		inputFamilyHistory += '<div class="input select"><select id="patientfamilyhistory-'+mhfh+'-physical-built" class="selectpicker form-control bs-select-hidden" tabindex="33" name="PatientFamilyHistory['+mhfh+'][physical_built]"><option value="Normal">Normal</option><option value="Slim">Slim</option><option value="Fat">Fat</option><option value="Athletes">Athletes</option></select>';
		
		/*<div class="btn-group bootstrap-select form-control"><button data-toggle="dropdown" class="btn dropdown-toggle btn-default" type="button" data-id="patientfamilyhistory-'+mhfh+'-physical-built" tabindex="33" title="Normal"><span class="filter-option pull-left">Normal</span>&nbsp;<span class="caret"></span></button><div class="dropdown-menu open"><ul role="menu" class="dropdown-menu inner"><li data-original-index="0"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Normal</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="1" class="selected"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Slim</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Fat</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Athletes</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div></div>*/
		
		inputFamilyHistory += '</div>';		
		inputFamilyHistory += '</td>';
		inputFamilyHistory += '<td><i class="glyphicon glyphicon-minus remove_patient_row" data-attr-type = "mhfh" data-attr-id="" data-attr="0" style="cursor:pointer;"></i></td>';
		inputFamilyHistory += '</tr>';

		$('.mh_family_history').append(inputFamilyHistory);

		$('.selectpicker').selectpicker();		

		$('.patientCalander').datetimepicker({
			format: 'MM-DD-YYYY'
		});

		$(":input").each(function (i) { 
			$(this).attr('tabindex', i + 1); 
		});
	});

		//List of Other Family Member
	$('.add_more_mh_other_family_member').click(function() {
		//mhps = medical history other family member
		mhofm++;
		var inputOtherFamilyMember = '<tr>';
		
		inputOtherFamilyMember += '<td class="form-group col-md-3 pad-right-0"><input type="hidden" value="" name="PatientOtherFamilyMember['+mhofm+'][id]"><div class="input text"><input type="text" value="" id="patientotherfamilymember-'+mhofm+'-family-member" class="form-control" tabindex="50" name="PatientOtherFamilyMember['+mhofm+'][family_member]"></div></td><td class="form-group col-md-3 pad-right-0"><div class="input select">';
		
		inputOtherFamilyMember += '<select id="patientotherfamilymember-'+mhofm+'-illness-type" class="selectpicker form-control bs-select-hidden" tabindex="51" name="PatientOtherFamilyMember['+mhofm+'][illness_type]"><option value="Accident (unintentional injury)">Accident (unintentional injury)</option><option value="Alcoholism and drug addiction">Alcoholism and drug addiction</option><option value="Alzheimer\'s disease">Alzheimer\'s disease</option><option value="Amputation">Amputation</option><option value="Arthritis">Arthritis</option><option value="Asperger syndrome">Asperger syndrome</option><option value="Attention deficit hyperactivity disorder">Attention deficit hyperactivity disorder</option><option value="Autism">Autism</option><option value="Bipolar disorder">Bipolar disorder</option><option value="Burn injury">Burn injury</option><option value="Cancer">Cancer</option><option value="Celiac disease">Celiac disease</option><option value="Cerebral palsy">Cerebral palsy</option><option value="Charcot-Marie-Tooth disease">Charcot-Marie-Tooth disease</option><option value="Chronic fatigue syndrome">Chronic fatigue syndrome</option><option value="Chronic obstructive pulmonary disease">Chronic obstructive pulmonary disease</option><option value="Crohn\'s disease">Crohn\'s disease</option><option value="Cystic fibrosis">Cystic fibrosis</option><option value="Dementia">Dementia</option><option value="Depression">Depression</option><option value="Diabetes">Diabetes</option><option value="Dissociative disorder">Dissociative disorder</option><option value="Down syndrome">Down syndrome</option><option value="Dwarfism">Dwarfism</option><option value="Eating disorders">Eating disorders</option><option value="Epilepsy">Epilepsy</option><option value="Fetal alcohol spectrum disorders">Fetal alcohol spectrum disorders</option><option value="Fibromyalgia">Fibromyalgia</option><option value="Generalized anxiety disorder">Generalized anxiety disorder</option><option value="Hearing loss and deafness">Hearing loss and deafness</option><option value="Heart disease">Heart disease</option><option value="HIV/AIDS">HIV/AIDS</option><option value="Huntington\'s disease">Huntington\'s disease</option><option value="Intellectual disability">Intellectual disability</option><option value="Kidney disease">Kidney disease</option><option value="Learning disabilities">Learning disabilities</option><option value="Lupus">Lupus</option><option value="Multiple sclerosis">Multiple sclerosis</option><option value="Narcolepsy">Narcolepsy</option><option value="Obsessive-compulsive disorder">Obsessive-compulsive disorder</option><option value="Panic disorder">Panic disorder</option><option value="Pervasive developmental disorders">Pervasive developmental disorders</option><option value="Polio and post-polio syndrome">Polio and post-polio syndrome</option><option value="Post-traumatic stress disorder">Post-traumatic stress disorder</option><option value="Psoriasis">Psoriasis</option><option value="Rare diseases">Rare diseases</option><option value="Schizophrenia">Schizophrenia</option><option value="Scleroderma">Scleroderma</option><option value="Social phobia">Social phobia</option><option value="Speech and language disorders">Speech and language disorders</option><option value="Spina bifida">Spina bifida</option><option value="Spinal cord injury">Spinal cord injury</option><option value="Stroke">Stroke</option><option value="Thyroid diseases">Thyroid diseases</option><option value="Traumatic brain injury">Traumatic brain injury</option><option value="Tuberous sclerosis">Tuberous sclerosis</option><option value="Turner syndrome">Turner syndrome</option><option value="Ulcerative colitis">Ulcerative colitis</option><option value="Vision loss and blindness">Vision loss and blindness</option><option value="Williams syndrome">Williams syndrome</option></select>';
		/*
		inputOtherFamilyMember += '<div class="btn-group bootstrap-select form-control"><button data-toggle="dropdown" class="btn dropdown-toggle btn-default" type="button" data-id="patientotherfamilymember-'+mhofm+'-illness-type" tabindex="51" title="Accident (unintentional injury)"><span class="filter-option pull-left">Accident (unintentional injury)</span>&nbsp;<span class="caret"></span></button><div class="dropdown-menu open"><ul role="menu" class="dropdown-menu inner"><li data-original-index="0" class="selected"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Accident (unintentional injury)</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="1"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Alcoholism and drug addiction</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Alzheimer\'s disease</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Amputation</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Arthritis</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="5"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Asperger syndrome</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="6"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Attention deficit hyperactivity disorder</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="7"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Autism</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="8"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Bipolar disorder</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="9"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Burn injury</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="10"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Cancer</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="11"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Celiac disease</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="12"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Cerebral palsy</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="13"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Charcot-Marie-Tooth disease</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="14"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Chronic fatigue syndrome</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="15"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Chronic obstructive pulmonary disease</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="16"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Crohn\'s disease</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="17"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Cystic fibrosis</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="18"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Dementia</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="19"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Depression</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="20"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Diabetes</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="21"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Dissociative disorder</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="22"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Down syndrome</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="23"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Dwarfism</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="24"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Eating disorders</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="25"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Epilepsy</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="26"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Fetal alcohol spectrum disorders</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="27"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Fibromyalgia</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="28"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Generalized anxiety disorder</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="29"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Hearing loss and deafness</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="30"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Heart disease</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="31"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">HIV/AIDS</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="32"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Huntington\'s disease</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="33"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Intellectual disability</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="34"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Kidney disease</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="35"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Learning disabilities</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="36"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Lupus</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="37"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Multiple sclerosis</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="38"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Narcolepsy</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="39"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Obsessive-compulsive disorder</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="40"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Panic disorder</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="41"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Pervasive developmental disorders</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="42"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Polio and post-polio syndrome</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="43"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Post-traumatic stress disorder</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="44"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Psoriasis</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="45"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Rare diseases</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="46"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Schizophrenia</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="47"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Scleroderma</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="48"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Social phobia</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="49"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Speech and language disorders</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="50"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Spina bifida</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="51"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Spinal cord injury</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="52"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Stroke</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="53"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Thyroid diseases</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="54"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Traumatic brain injury</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="55"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Tuberous sclerosis</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="56"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Turner syndrome</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="57"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Ulcerative colitis</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="58"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Vision loss and blindness</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="59"><a data-tokens="null" style="" class="" tabindex="0"><span class="text">Williams syndrome</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div></div>';
		*/
		inputOtherFamilyMember += '</div></td><td class="form-group col-md-3 pad-right-0"><div class="input text"><input type="text" value="" id="patientotherfamilymember-'+mhofm+'-type-specify" class="form-control" tabindex="52" name="PatientOtherFamilyMember['+mhofm+'][type_specify]"></div></td><td><i class="glyphicon glyphicon-minus remove_patient_row" data-attr-type = "mhofm"  data-attr-id="" data-attr="0" style="cursor:pointer;"></i></td></tr>';

		

		$('.mh_other_family_member').append(inputOtherFamilyMember);
		$('.selectpicker').selectpicker();

		$('.patientCalander').datetimepicker({
			format: 'MM-DD-YYYY'
		});

		$(":input").each(function (i) { 
			$(this).attr('tabindex', i + 1); 
		});
	});


$(".patientCalander").click(function(){
	var date = $(this).attr('value');
});



	
		//List of Any Surgeries
	$('.add_more_mh_other_treatment').click(function() {
		//mhot = medical history other treatment
		mhot++;
		var inputHistoryOtherTreatment = '<tr>';
		
		inputHistoryOtherTreatment += '<td class="form-group col-md-3 pad-right-0"><input type="hidden" value="" name="PatientBones['+mhot+'][id]"><div class="input text"><input type="text" value="" id="patientbones-'+mhot+'-doctor" class="form-control add_doctor" tabindex="65" name="PatientBones['+mhot+'][doctor]"></div>	</td><td class="form-group col-md-2 pad-right-0"><div class="input-group date"><div class="input text"><input type="text" value="" id="patientbones-'+mhot+'-date" class="form-control add_doctor patientCalander" tabindex="66" name="PatientBones['+mhot+'][date]"></div><span class="input-group-addon calendar-icon"><span class="glyphicon glyphicon-calendar"></span></span></div></td><td class="form-group col-md-3 pad-right-0"><div class="input text"><input type="text" value="" id="patientbones-'+mhot+'-treatment" class="form-control add_doctor" tabindex="67" name="PatientBones['+mhot+'][treatment]"></div></td><td class="form-group col-md-3 pad-right-0"><div class="input text"><input type="text" value="" id="patientbones-'+mhot+'-medications" class="form-control add_doctor" tabindex="68" name="PatientBones['+mhot+'][medications]"></div></td><td><i class="glyphicon glyphicon-minus remove_patient_row" data-attr-type = "mhot" data-attr-id="" data-attr="0" style="cursor:pointer;"></i></td></tr>';

		$('.mh_other_treatment').append(inputHistoryOtherTreatment);	

		$('.patientCalander').datetimepicker({
			format: 'MM-DD-YYYY'
		});
	});

	$(document).on('click', '.remove_patient_row', function(){
		$(this).parent().parent().remove();
		var id = $(this).attr('data-attr-id');
		var type = $(this).attr('data-attr-type');
		$.ajax({
			url			: ajaxUrl + 'contacts/remove_patient_row',
			type		: 'POST',
			dataType	: 'html',
			data		: {
				id : id,
				type : type
			},
			success : function(response){
						
			}
		});
	});

	$(document).on('click', '.remove_major_illness', function(){
		$(this).parent().remove();
		var id = $(this).attr('data-attr-id');
		if(id!=''){
			$.ajax({
				url			: ajaxUrl + 'contacts/patient_medication_delete',
				type		: 'POST',
				dataType	: 'html',
				data		: {
					id : id
				},
				success : function(response){
							
				}
			});
		}
	});

	$(document).on('click', '.remove_history_surgeries', function(){
		$(this).parent().parent().remove();
		var id = $(this).attr('data-attr-id');
		if(id!=''){
			$.ajax({
				url			: ajaxUrl + 'contacts/patient_medication_delete',
				type		: 'POST',
				dataType	: 'html',
				data		: {
					id : id
				},
				success : function(response){
							
				}
			});
		}
	});

	
	$(document).on('click', '.remove_mh_family_history', function(){
		$(this).parent().parent().remove();
		var id = $(this).attr('data-attr-id');
		if(id!=''){
			$.ajax({
				url			: ajaxUrl + 'contacts/patient_medication_delete',
				type		: 'POST',
				dataType	: 'html',
				data		: {
					id : id
				},
				success : function(response){
							
				}
			});
		}
	});

	$(document).on('click', '.remove_mh_other_family_member', function(){
		$(this).parent().parent().remove();
		var id = $(this).attr('data-attr-id');
		if(id!=''){
			$.ajax({
				url			: ajaxUrl + 'contacts/patient_medication_delete',
				type		: 'POST',
				dataType	: 'html',
				data		: {
					id : id
				},
				success : function(response){
							
				}
			});
		}
	});

});




	// PRODEDURE / SURGERY

	/*
	* Add Doctor functionality using ajax submission
	*/
	$( document ).on( "submit", '.add_procedure', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'surgery/add',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					
					$form.find( ".btn-default" ).trigger('click');
					afterAdd('<p><strong>'+response.msg+'</strong></p>');
					afterAddRemove();

					//$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');

					// Empty all previous fields
					$form.find( "input[type=text], input[type=password], input[type=hidden], textarea" ).val("");
					
					//Prepend new added data
					$('#doctor_grid_tbody').prepend(response.data);
					progressBar();
					removeFlashMessage();
					get_procedure();
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');

					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								console.log(i+'-'+k+'___'+n);
								
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");

								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});

	/*
	* Add Doctor functionality using ajax submission
	*/
	$( document ).on( "submit", '.edit_procedure', function( event ) {
		// Remove messages if any
		$('.form-success').remove();
		$('.form-error').remove();

		var $form = $(this);
		$form.find("input[type='submit']").prop('disabled', true).addClass('disabled-btn').after('<span class="processing"></span>');
		event.preventDefault();

		// Ajax request started
		$.ajax({
			url			: defaultUrl + 'surgery/edit',
			type		: 'POST',
			dataType	: 'json',
			data		: $( this ).serialize(),
			success : function(response){
				// Remove all error class and label first
				$form.find( ".form-group" ).removeClass( "has-error" );
				$form.find( ".runtime-label" ).remove();
				$form.find("input[type='submit']").prop('disabled', false).removeClass('disabled-btn'); $('.processing').remove();

				if(response.result == "success"){
					// If success 
					// Display success message 
					
					$form.find( ".btn-default" ).trigger('click');
					afterAdd('<p><strong>'+response.msg+'</strong></p>');
					afterAddRemove();

					//$form.find( ".save" ).parent().append('<div class="form-success alert-success" role="alert">'+response.msg+'</div>');

					// Empty all previous fields
					$form.find( "input[type=text], input[type=password], input[type=hidden], textarea" ).val("");
					
					//Prepend new added data
					$('#doctor_grid_tbody').prepend(response.data);
					progressBar();
					removeFlashMessage();
					get_procedure();
				}else{
					// If Getting Errors
					// Display error message
					$form.find( ".save" ).parent().find('.form-error').remove();
					$form.find( ".save" ).parent().append('<div class="form-error alert-danger" role="alert">Form has some errors, Please fill the required fields.</div>');

					$.each(response, function(i, j){
						$.each(j, function(k, l){
							$.each(l, function(m, n){
								console.log(i+'-'+k+'___'+n);
								
								// If any name is consisting or "_", Remove and replace with "-";
								var j = k.split("_");
								k = j.join("-");

								$form.find('#'+i+'-'+k).parent().parent('.form-group').addClass('has-error');
								$form.find('#'+i+'-'+k).parent().append('<label class="runtime-label">'+n+'</label>');
							});
						});
					});
				}
				return false;
			}
		});
		return false;
	});


	/*
	* Delete Procedure functionality
	*/
	$(document).on('click', '#delete_procedure', function(){

		var procedureId = [];
		$('.check_user_single').each(function() {
			if($(this).is(':checked')){ 
				procedureId.push($(this).prop('value'));
			}
		});
		
		if(procedureId == ""){
			alert('Select alteast one surgery first');
			return false;
		}
		
		$.ajax({
			url			: defaultUrl + 'surgery/delete',
			type		: 'POST',
			dataType	: 'json',
			data		: {
				ids	: procedureId
			},
			success : function(response){
					var msg = response.msg;
					var deletedIds = response.data;

					$.each(deletedIds, function(i, j){
						$('#tr_'+j).remove();
					});

					$('#delete_user_cancel').trigger('click');
					
					//remove delete model box
					removeModal();
					console.log(msg);
					get_procedure();
			}
		});
	});

	$(document).on('mouseover', '.get_right_sidebar_user', function() {
		$(this).find('.edit-pro-button').show();
	});
	$(document).on('mouseout', '.get_right_sidebar_user', function() {
			$(this).find('.edit-pro-button').hide();
	});

	$(document).on('click', '.edit-pro-button', function() {
		var pro_id = $(this).attr('data-attr-id');
		$('.accordion_edit').html('');
		console.log(pro_id);
		$.ajax({
			url			: defaultUrl + 'surgery/edit_pro',
			type		: 'POST',
			dataType	: 'html',
			data		: {
				id : pro_id
			},
			success : function(response){
				$('.accordion_edit').html(response);	
			}
		});
		return false;
	});

	function get_procedure()
	{
		$.ajax({
			url			: defaultUrl + 'surgery/listing',
			type		: 'POST',
			dataType	: 'html',
			success : function(response){
				$('#doctor_grid_tbody').html(response);		
			}
		});
		return false;
	}



	function resetContact(){
		var search_filter_by = $('#contact_search_filter').val();
		var search_filter_key = $('#search_contact_keyword').val();
		searchContactFunction(search_filter_key, search_filter_by);
	}
