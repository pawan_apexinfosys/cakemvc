<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Controller\Component\CookieComponent;
use Cake\Core\Configure;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */

class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     * @return void
     */
    public function initialize()
    {
        $this->loadComponent('Cookie');
		$this->loadComponent('Flash');
		$this->loadComponent('EmailTrigger');
		$this->loadComponent('Auth', [
			'authorize' => 'Controller',
			'authenticate' => [
				'Form' => [
					'fields' => [
						'username' => 'username', 
						'password' => 'password'
					]
				]
			],
			'loginRedirect' => [
				'controller' => 'contacts',
				'action' => 'index',
				'prefix' => false
			],
			'logoutRedirect' => [
				'prefix' => false,
				'controller' => 'users',
				'action' => 'login', 'home'	
			],
			'authError' => 'You have been logged out due to inactivity. Please log in again',
			'unauthorizedRedirect' => $this->referer()
		]);

		
    }

	public function beforeFilter(Event $event){
		// Allow actions
        $this->Auth->allow(['login', 'forgotPassword', 'logout', 'payment_emails', 'doctor_index', 'cronJobs']);
    }


	public function isAuthorized($user)
    {
		$controller = $this->request->params['controller'];
		$action = $this->request->params['action'];
		$prefix = false;
		
		// Any registered user can access public functions
        if (empty($this->request->params['prefix'])) {
			if($user['role'] === 'doctor'){
				$this->request->params['prefix'] = 'doctor'; 
				return $this->redirect(['controller'=>$controller, 'action'=>$action, 'prefix' => 'doctor']);
			}else if($user['role'] === 'patient' || $user['role'] === 'lead'){
				$this->request->params['prefix'] = 'patient'; 
				return $this->redirect(['controller'=>$controller, 'action'=>$action, 'prefix' => 'patient']);
			}else if($user['role'] === 'driver'){
				$this->request->params['prefix'] = 'driver'; 
				return $this->redirect(['controller'=>$controller, 'action'=>$action, 'prefix' => 'driver']);
			}else{
				return true;
			}
        }else{
			if($user['role'] === 'admin' || $user['role'] === 'superadmin' || $user['role'] === 'salesrep'){
				return $this->redirect(['controller'=>$controller, 'action'=>$action, 'prefix' => false]);
			}
			return true;
		}
       
        // Default deny
        return false;
    }

	public function sendFile($id){
		$this->Uploads = TableRegistry::get('Uploads');
		$file = $this->Uploads->getFile($id);
		$this->response->file(
			$file['path'],
			['download' => true, 'name' => 'foo']
		);
		return $this->response; 
	}
}
