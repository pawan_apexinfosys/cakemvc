function countSelectedUsers(){
	var singleCheckedCount = 0;
	var totalCount = 0;
	$('.check_user_single').each(function() {
		if($(this).is(':checked')){
			singleCheckedCount = singleCheckedCount + 1;
		}
		totalCount = totalCount + $(this).length;
	});
	var countReturn = [];
	countReturn = {'total':totalCount, 'selected':singleCheckedCount};
	console.log(countReturn);
	return countReturn;
}



/*
* User select/Check functionality - Contact module
* Check all checkbox functionality
*/

$(document).on('click', '.check_user_all', function() {
	var countReturn = countSelectedUsers();
	console.log(countReturn['total']+'_____'+countReturn['selected']);
	checkedCount = 0;
	if($(this).is(':checked')){
		$('.check_user_single').each(function() {
			$(this).prop('checked', true);
			checkedCount = checkedCount + $(this).length;
		});
		$('.selected-user-count').html(checkedCount);
		// Doctor delete popup display true using data-toggle = modal
		if(checkedCount > 0){
			addModal();
		}else{
			removeModal();
		}
	}else{
		$('.check_user_single').each(function() {
			$(this).prop('checked', false);
		});
		$('.selected-user-count').html(0);
		removeModal();
	}
});

// Single check functionality
$(document).on('click', '.check_user_single', function() {
	var countReturn = countSelectedUsers();
	
	$('.selected-user-count').html(countReturn['selected']);

	// Doctor delete popup display true using data-toggle = modal
	if(countReturn['selected'] > 0){
		addModal();
	}else{
		removeModal();
	}

	if(countReturn['selected'] == countReturn['total']){
		$('.check_user_all').prop('checked', true);
	}else{
		$('.check_user_all').prop('checked', false);
	}
});


$(document).on('click', '.delete_user_icon, .delete_task_icon, .assign_task_icon, .completed_task_icon', function() {
	var findSelected = countSelectedUsers();
	if(findSelected.selected <= 0){
		alert('Select atleast one Task');
		return false;
	}
});


/*
* Delete Users functionality
*/
$(document).on('click', '#delete_user', function(){

	var usersId = [];
	$('.check_user_single').each(function() {
		if($(this).is(':checked')){ 
			usersId.push($(this).prop('value'));
		}
	});
	
	if(usersId == ""){
		alert('Select alteast one user first');
		return false;
	}
	
	$.ajax({
		url			: defaultUrl + 'manage/delete',
		type		: 'POST',
		dataType	: 'json',
		data		: {
			ids	: usersId
		},
		success : function(response){
				var msg = response.msg;
				var deletedIds = response.data;

				$.each(deletedIds, function(i, j){
					$('#tr_'+j).remove();
				});

				$('#delete_user_cancel').trigger('click');
				
				//remove delete model box
				removeModal();

				// Select count
				var countReturn = countSelectedUsers();
				$('.selected-user-count').html(countReturn['selected']);
				searchAllAjax();
				console.log(msg);
		}
	});
});

