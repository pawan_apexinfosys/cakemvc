<?php 
public function delete(){
	$this->layout = 'ajax';
	$this->autoRender = false;	
	$usersTable = TableRegistry::get('Users');

	if($this->request->is(['post', 'ajax'])){
		$deletedIds = [];
		$ids = $this->request->data['ids'];
		foreach($ids as $id){
			$entity = $usersTable->get($id, ['contain'=>'Profiles']);

			$result = $usersTable->delete($entity);
			if($result){
				$deletedIds[] = $id;
			}

			// Save Activity log short content in activity table
			$activity = [
				'user_id'	 => $id,
				'profile_id' => $entity->profile->id,
				'content'	 => ucfirst($entity->role).' '.$entity->profile->firstname.' '.$entity->profile->lastname.' deleted',
				'type'		 => $entity->role.'_deleted',
				'updated_by' => $this->Auth->user('Profile.id')
			];
		
			TableRegistry::get('Activities')->addActivity($activity);

		}
		$returnJson = ['msg'=>'Deleted', 'data'=>$deletedIds];
		
		echo json_encode($returnJson);
		die;
	}
	die;
}
?>