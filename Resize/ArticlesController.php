<?php
class ArticlesController extends AppController {
	
	public function beforeFilter() {		
		parent::beforeFilter();
		$this->Auth->allowedActions = array('upload_image', 'upload_file');
		$user_id = $this->Auth->user('id');
		$user_name = $this->Auth->user('email');
		$this->set("user_id", $user_id);
		$this->set("user_name", $user_name);	
		$action_name = $this->action;
		$contorller_name = strtolower($this->name);

		if(!empty($user_id)){
			$role_id = $this->Auth->user('role_id');
			if($role_id != '1'){
				$this->User = ClassRegistry::init('User');
				$permission_array = $this->User->find('first', array('conditions'=>array('User.id'=>$user_id), 'fields'=>array('admin_add', 'admin_edit',  'admin_delete', 'admin_add_back_issue', 'admin_edit_back_issue', 'admin_delete_back_issue')));
				$todaysDate = date("Y-m-d H:i:s");
				date('Y-m-d 24:00:00', strtotime('-1 day', strtotime($todaysDate)));
				foreach($permission_array as $permission){
 				}
				$page_val = $action_name;
				$admin_page_val = $contorller_name.'_'.$action_name;
				"pageval->".$page_val;
				if (array_key_exists($page_val, $permission)){
					if($permission[$page_val] == '1'){
						//User has privilege
						if($admin_page_val == 'users_admin_add' || $admin_page_val == 'users_admin_edit' || $admin_page_val == 'categories_admin_edit'){
							$this->Session->setFlash(__('User does not have privilege'), 'default', array('class'=>'alert alert-danger'));
							$this->redirect(array('controller'=>$contorller_name,'action'=>'index'));
						}
					}
					else{
						//User does not have privilege
						$this->Session->setFlash(__('User does not have privilege'), 'default', array('class'=>'alert alert-danger'));
						$this->redirect(array('controller'=>$contorller_name,'action'=>'index'));
					}
				  }
				else{
					//$this->Session->setFlash(__('User does not have privilege'));
					//$this->redirect(array('controller'=>'users','action'=>'index'));
				  }

				}
			}
		date_default_timezone_set('Asia/Kolkata');
	}
	
	public $paginate = array(
        'limit' => 20,
        'order' => array(
            'Article.created' => 'desc'
        )
    );
	
	public function admin_download($id = null) {
		$this->Files = ClassRegistry::init('Files');
		$files = $this->Files->find('first', array('conditions'=>array('Files.id'=>$id)));
		
		if(empty($files)){
			$this->Session->setFlash(__("File not found"), 'default', array('class'=>'alert alert-danger'));
			$this->redirect(array('controller'=>'articles', 'action'=>'index'));
		}

		$ext = pathinfo($files['Files']['name'], PATHINFO_EXTENSION);
		$name = pathinfo($files['Files']['name'], PATHINFO_FILENAME);
		
		$underscore_name = $this->clean_for_title($name);

		$filename = $underscore_name.'.'.$ext;

		$path = WWW_ROOT.str_replace($filename,'',$files['Files']['path']);
		
		$this->viewClass = 'Media';

		$params = array(
			'id'        => $filename,
			'name'      => $underscore_name,
			'extension' => $ext,
			'path'      => $path,
			'download'  => true
		);

		$this->set($params);
	}

	public function upload_image() {
		$this->autoRender = false;
		$this->Article = ClassRegistry::init('Article');
		$this->Images = ClassRegistry::init('Images');  
		$uuid = String::uuid();
		
		$article_uuid = $this->request->data['article_title'];

		$last_folder_name = $article_uuid;

		$root = WWW_ROOT;				
		$dest_folder = '/img/Articles/';		
		
		$gallery_image_folder = $root.$dest_folder.$last_folder_name;
		$thumb_image_folder = $root.$dest_folder.$last_folder_name.'/thumb';
		$sm_thumb_image_folder = $root.$dest_folder.$last_folder_name.'/sm-thumb';

		$this->mkdir_recursive($gallery_image_folder);
		$this->mkdir_recursive($thumb_image_folder);
		$this->mkdir_recursive($sm_thumb_image_folder);
		
		$ext = pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);
		$name = pathinfo($_FILES['Filedata']['name'], PATHINFO_FILENAME);

		$file_name = $this->clean_for_title($name);

		$filename		= $gallery_image_folder.'/'.$file_name.'.'.$ext;
		$thumbfile		= $thumb_image_folder.'/'.$file_name.'.'.$ext;
		$sm_thumbfile	= $sm_thumb_image_folder.'/'.$file_name.'.'.$ext;

		$size = getimagesize($_FILES['Filedata']['tmp_name']); 
		$width = $size[0]; 
		$height = $size[1]; 
		$aspect = $height / $width; 
		if ($aspect >= 1) 
			$mode = "vertical"; 
		else 
			$mode = "horizontal"; 

		if($mode == "horizontal"){
			if($ext == "tif" || $ext == "tiff" || $ext == "TIF" || $ext == "TIFF"){
				if (move_uploaded_file($_FILES['Filedata']['tmp_name'], $filename)) {
						copy($filename, $thumbfile);
						copy($filename, $sm_thumbfile);
				}
			}
			else{
				$this->Resize->resizeImage($_FILES['Filedata']['tmp_name'], $thumbfile, $width='341', $height='241');
				$this->Resize->resizeImage($_FILES['Filedata']['tmp_name'], $filename);
				$this->Resize->resizeImage($_FILES['Filedata']['tmp_name'], $sm_thumbfile, $width='134', $height='130');
			}
		}
		else{
			if($ext == "tif" || $ext == "tiff" || $ext == "TIF" || $ext == "TIFF"){
				if (move_uploaded_file($_FILES['Filedata']['tmp_name'], $filename)) {
						copy($filename, $thumbfile);
						copy($filename, $sm_thumbfile);
				}
			}
			else{
				$this->Resize->resizeImage($_FILES['Filedata']['tmp_name'], $thumbfile, $width='241', $height='341');
				$this->Resize->resizeImage($_FILES['Filedata']['tmp_name'], $filename);
				$this->Resize->resizeImage($_FILES['Filedata']['tmp_name'], $sm_thumbfile, $width='134', $height='180');
			}
		}
		
		

		$this->TempImage = ClassRegistry :: init('TempImage');
		$data = array(
			'id'		=> '',
			'uuid'		=> $uuid,
			'name'		=> $_FILES['Filedata']['name'],
			'path'		=> $dest_folder.$last_folder_name.'/'.$file_name.'.'.$ext,
			'thumb_path'=> $dest_folder.$last_folder_name.'/thumb/'.$file_name.'.'.$ext,
			'size'		=> $this->formatSizeUnits($_FILES['Filedata']['size']),
			'label'		=> 'Null');

		$this->TempImage->save($data);
		echo 'IMG~';
		echo $this->webroot.'/files/timthumb.php?src='.$this->webroot.'/'.$dest_folder.$last_folder_name.'/sm-thumb/'.$file_name.'.'.$ext.'&h=180&w=134&q=95~';
		echo $dest_folder.$last_folder_name.'/'.$file_name.'.'.$ext.'~';
		echo $dest_folder.$last_folder_name.'/thumb/'.$file_name.'.'.$ext.'~';
		echo $_FILES['Filedata']['name'].'~';
		echo $this->formatSizeUnits($_FILES['Filedata']['size']).'~';
		echo $last_folder_name.'~';
	}

	public function upload_file() {
		$this->autoRender = false;
		$this->TempFile = ClassRegistry::init('TempFile');
		$uuid = String::uuid();
		$article_uuid = $this->request->data['article_title'];
		$last_folder_name = $article_uuid;

		$root = WWW_ROOT;				
		$dest_folder = '/files/Articles/';			
		
		$gallery_image_folder = $root.$dest_folder.$last_folder_name;
		$this->mkdir_recursive($gallery_image_folder);

		$ext = pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);
		$name = pathinfo($_FILES['Filedata']['name'], PATHINFO_FILENAME);
		
		$file_name = $this->clean_for_title($name);

		$filename = $gallery_image_folder.'/'.$file_name.'.'.$ext;	

		if(move_uploaded_file($_FILES['Filedata']['tmp_name'], $filename)){
			$data = array(
				'id'		=> '',
				'uuid'		=> $uuid,
				'name'		=> $_FILES['Filedata']['name'], 
				'path'		=> $dest_folder.$last_folder_name.'/'.$file_name.'.'.$ext,
				'size'		=> $this->formatSizeUnits($_FILES['Filedata']['size']),
				'folder'	=> $last_folder_name);

			$this->TempFile->save($data);
		}
		echo 'FILE~';
		echo $dest_folder.$last_folder_name.'/'.$file_name.'.'.$ext.'~';
		echo $_FILES['Filedata']['name'].'~';
		echo $this->formatSizeUnits($_FILES['Filedata']['size']).'~';
		echo $last_folder_name.'~';
	}
	
	public function admin_demo(){
		$this->layout = 'admin';
	}

	public function upload_image_group() {
		$this->layout = 'ajax';
		$this->autoRender = false;
		$uuid = String::uuid();
		$new_count = $this->request->data['count_image'];
		$article_uuid = $this->request->data['article_uuid'];

		if(empty($article_uuid)){
			$article_uuid = $uuid;
		}

		$this->set(compact('new_count', 'article_uuid'));
		echo $this->render('/Elements/upload_image_group');
		die();
	}

	public function upload_file_group() {
		$this->layout = 'ajax';
		$this->autoRender = false;
		$new_count = $this->request->data['count_file'];
		$article_uuid = $this->request->data['article_uuid'];

		$this->set(compact('new_count', 'article_uuid'));
		echo $this->render('/Elements/upload_file_group');
		die();
	}

	public function admin_create_session() {
		$this->autoRender = false;
		$this->Session->delete('recipe_title');
		$recipe_title = $this->request->data['recipe_title'];
		$this->Session->write('recipe_title',$recipe_title);
	}
}