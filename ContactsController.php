<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Network\Email\Email;
use Cake\Utility\Text;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use UploadHandler\UploadHandler;

class ContactsController extends AppController
{
	
	public $user = '';
	public $paginate = [
        'limit' => 10,
		'sortWhitelist' => [
			'Users.id', 'Users.email', 'Profiles.firstname', 'Profiles.phone', 'Users.created', 'Procedures.name', 'Profiles.sales_rep_id', 'Schedules.date', 'profile_state.name', 'Profiles.state_id'
		],
        'order' => [
            'Users.id' => 'desc'
        ]
    ];
	
	public function initialize(){
		parent::initialize();
		$this->layout = 'contacts';
		$this->loadComponent('Paginator');
	}
	

	/*search funtion for all contact drop down*/
	public function allsearch($orderType = null){
		// Getting Pre-defined values from database
		$this->layout = 'contacts';
		// Procedures List
		$procedureTable = TableRegistry::get('Procedures');
		$procedures = $procedureTable->find('list');
		$proceduresList = $procedures->toArray();
		// Lead Sources List
		$LeadSourcesTable = TableRegistry::get('LeadSources');
		$leadSources = $LeadSourcesTable->find('list');
		$leadSourcesList = $leadSources->toArray();
		
		// State List
		$statesTable = TableRegistry::get('States');
		$statesSources = $statesTable->find('list');
		$stateList = $statesSources->toArray();
		$stageList = [
			'1'=>'Stage 1: Nurture Client',
			'2'=>'Stage 2: Client Interested',
			'3'=>'Stage 3: Client Login',
			'4'=>'Stage 4: Client Form Completed',
			'5'=>'Stage 5: R4aC Reviewed',
			'6'=>'Stage 6: Dr. Notified',
			'7'=>'Stage 7: Dr Completed',
			'8'=>'Stage 8: Client Approved',
			'9'=>'Stage 9: Deposit Requested',
			'10'=>'Stage 10: Deposit Paid',
			'11'=>'Stage 11: Client Travel Added',
			'12'=>'Stage 12: Hotel Confirmed',
			'13'=>'Stage 13: Client Pre-Op Confirmed',
			'14'=>'Stage 14: Post-Op Email Sent'
		];

		$usersTable = TableRegistry::get('Users');
		$profilesTable = TableRegistry::get('Profiles');
		$surgeriesTable = TableRegistry::get('Surgeries');
		
		$salesrepList = $this->getSalesRepList(); 
		$doctorsList = $this->getDoctorList();

		$page = isset($this->request->query['page']) ? $this->request->query['page'] : '';
		$session = $this->request->session();
		if($this->request->is(['post', 'ajax'])){
			$this->layout = 'ajax';
			switch($searchBy){
				case "livelead":
					$allUsers = $usersTable->find('all', [
						'conditions'=>[
							'Users.role'=>'lead',
							'Users.status'=>'Live'
						], 
						'order'=>'Users.id DESC'])
						->contain([
							'Calls',
							'Schedules',
							'Profiles.Procedures',
							'Profiles.SalesReps',
							'Profiles.Doctors', 'Profiles.Drivers',
							'Profiles.States', 
							'Profiles.Stages', 
							'Profiles.LeadSources', 
							'UserSurgeries.Surgeries', 
							'UserUploads.Uploads'
						]);
						break;
				case "patient":
						$allUsers = $usersTable->find('all', [
						'conditions'=>[
							'Users.role'=>'patient',
						], 
						'order'=>'Users.id DESC'])
						->contain([
							'Calls',
							'Schedules',
							'Profiles.Procedures',
							'Profiles.SalesReps',
							'Profiles.Doctors', 'Profiles.Drivers',
							'Profiles.States', 
							'Profiles.Stages', 
							'Profiles.LeadSources', 
							'UserSurgeries.Surgeries', 
							'UserUploads.Uploads'
						]);
						break;
				case "pastpatient" :
						$allUsers = $usersTable->find('all', [
						'conditions'=>[
							'Users.role'=>'patient',
							'Users.status'=>'Past'
						], 
						'order'=>'Users.id DESC'])
						->contain([
							'Calls',
							'Schedules',
							'Profiles.Procedures',
							'Profiles.SalesReps',
							'Profiles.Doctors', 'Profiles.Drivers',
							'Profiles.States', 
							'Profiles.Stages', 
							'Profiles.LeadSources', 
							'UserSurgeries.Surgeries', 
							'UserUploads.Uploads'
						]);
						break;
				case "deadlead" :
					$allUsers = $usersTable->find('all', [
						'conditions'=>[
							'Users.role'=>'lead',
							'Users.status'=>'Dead'
						], 
						'order'=>'Users.id DESC'])
						->contain([
							'Calls',
							'Schedules',
							'Profiles.Procedures',
							'Profiles.SalesReps',
							'Profiles.Doctors', 'Profiles.Drivers',
							'Profiles.States', 
							'Profiles.Stages', 
							'Profiles.LeadSources', 
							'UserSurgeries.Surgeries', 
							'UserUploads.Uploads'
						]);
					 break;
				case "allcontact":
					$allUsers = $usersTable->find('all', [
						'conditions'=>[
							'OR'=>[
									['Users.role'=>'lead'],
									['Users.role'=>'patient'],
								]
							], 
						'order'=>'Users.id DESC'])
						->contain([
							'Calls',
							'Schedules',
							'Profiles.Procedures',
							'Profiles.SalesReps',
							'Profiles.Doctors', 'Profiles.Drivers',
							'Profiles.States', 
							'Profiles.Stages', 
							'Profiles.LeadSources', 
							'UserSurgeries.Surgeries', 
							'UserUploads.Uploads'
						]);
						break;
				case "allqueue" :
					$allUsers = $usersTable->find('all', [
						'conditions'=> [
								'OR'=>[
									['Users.role'=>'lead'],
									['Users.role'=>'patient'],
								],
								'Profiles.call_queue' => '1'
						], 
						'order'=>'Users.id DESC'
							])->contain([
								'Profiles', 
								'Schedules',
								'UserUploads.Uploads',
								'Profiles.Procedures',
								'Profiles.States'
							]);
					break;
			}
			$allUsers = $this->paginate($allUsers);
			
			// Get Owner and Doctor and add with user array
			$this->getSalesRepNameArray($allUsers);

			/*
			foreach($allUsers as $key=>$user){
				if($key == 0){
					$sales_rep_id = isset($user->profile->sales_rep_id) ? $user->profile->sales_rep_id : '';
					if(!empty($sales_rep_id)){
						$salesArray = $this->getSalesRep($sales_rep_id);
						if(!empty($salesArray)){
							$user->profile->sales_rep->name = $salesArray;
						}
					}
					
					$doctor_id = isset($user->profile->doctor_id) ? $user->profile->doctor_id : '';
					if(!empty($doctor_id)){
						$doctorArray = $this->getDoctor($doctor_id);
						if(!empty($doctorArray)){
							$user->profile->doctor->name = $doctorArray;
						}
					}
				}
			}*/
			$user = $this->user;
			$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers', 'user'));
			echo $this->render('/Element/contacts/index');
			die;
		}else{

			switch($orderType){
				case "livelead":
					$allUsers = $usersTable->find('all', [
						'conditions'=>[
							'Users.role'=>'lead',
							'Users.status'=>'Live'
						], 
						'order'=>'Users.id DESC'])
						->contain([
							'Calls',
							'Schedules',
							'Profiles.Procedures',
							'Profiles.SalesReps',
							'Profiles.Doctors', 'Profiles.Drivers',
							'Profiles.States', 
							'Profiles.Stages', 
							'Profiles.LeadSources', 
							'UserSurgeries.Surgeries', 
							'UserUploads.Uploads'
						]);
						break;
				case "patient":
						$allUsers = $usersTable->find('all', [
						'conditions'=>[
							'Users.role'=>'patient',
						], 
						'order'=>'Users.id DESC'])
						->contain([
							'Calls',
							'Schedules',
							'Profiles.Procedures',
							'Profiles.SalesReps',
							'Profiles.Doctors', 'Profiles.Drivers',
							'Profiles.States', 
							'Profiles.Stages', 
							'Profiles.LeadSources', 
							'UserSurgeries.Surgeries', 
							'UserUploads.Uploads'
						]);
						break;
				case "pastpatient" :
						$allUsers = $usersTable->find('all', [
						'conditions'=>[
							'Users.role'=>'patient',
							'Users.status'=>'Past'
						], 
						'order'=>'Users.id DESC'])
						->contain([
							'Calls',
							'Schedules',
							'Profiles.Procedures',
							'Profiles.SalesReps',
							'Profiles.Doctors', 'Profiles.Drivers',
							'Profiles.States', 
							'Profiles.Stages', 
							'Profiles.LeadSources', 
							'UserSurgeries.Surgeries', 
							'UserUploads.Uploads'
						]);
						break;
				case "deadlead" :
					$allUsers = $usersTable->find('all', [
						'conditions'=>[
							'Users.role'=>'lead',
							'Users.status'=>'Dead'
						], 
						'order'=>'Users.id DESC'])
						->contain([
							'Calls',
							'Schedules',
							'Profiles.Procedures',
							'Profiles.SalesReps',
							'Profiles.Doctors', 'Profiles.Drivers',
							'Profiles.States', 
							'Profiles.Stages', 
							'Profiles.LeadSources', 
							'UserSurgeries.Surgeries', 
							'UserUploads.Uploads'
						]);
					 break;
				case "allcontact":
					$allUsers = $usersTable->find('all', [
						'conditions'=>[
							'OR'=>[
									['Users.role'=>'lead'],
									['Users.role'=>'patient'],
								]
							], 
						'order'=>'Users.id DESC'])
						->contain([
							'Calls',
							'Schedules',
							'Profiles.Procedures',
							'Profiles.SalesReps',
							'Profiles.Doctors', 'Profiles.Drivers',
							'Profiles.States', 
							'Profiles.Stages', 
							'Profiles.LeadSources', 
							'UserSurgeries.Surgeries', 
							'UserUploads.Uploads'
						]);

						break;
				case "allqueue" :

						$allUsers = $usersTable->find('all', [
						'conditions'=> [
								'OR'=>[
									['Users.role'=>'lead'],
									['Users.role'=>'patient'],
								],
								'Profiles.call_queue' => '1'
						], 
						'order'=>'Users.id DESC'
						])->contain([
								'Profiles', 
								'UserUploads.Uploads',
								'Profiles.Procedures',
								'Profiles.States',
								'Schedules',
						]);

					break;
				default:
					$allUsers = $usersTable->find('all', [
							'conditions'=>[
								'OR'=>[
										['Users.role'=>'lead'],
										['Users.role'=>'patient'],
									]

							], 
							'order'=>'Users.id DESC',
							'group'=>'Users.id'])
							->contain([
									'Calls',
									'Notes.Profiles',
									'Notes.Updates',
									'Tasks.Profiles',
									'Tasks.Updates',
									'Schedules',
									'Payments',
									'Profiles.Procedures',
									'Profiles.SalesReps',
									'Profiles.Doctors', 'Profiles.Drivers',
									'Profiles.States', 
									'Profiles.Stages', 
									'Profiles.LeadSources', 
									'UserSurgeries.Surgeries', 
									'UserUploads.Uploads'
								]);
						break;
			}

			$allUsers = $this->paginate($allUsers);

			// Get Owner and Doctor and add with user array
			$this->getSalesRepNameArray($allUsers);
			/*
			foreach($allUsers as $key=>$user){
				if($key == 0){
					$sales_rep_id = isset($user->profile->sales_rep_id) ? $user->profile->sales_rep_id : '';
					if(!empty($sales_rep_id)){
						$salesArray = $this->getSalesRep($sales_rep_id);
						if(!empty($salesArray)){
							$user->profile->sales_rep->name = $salesArray;
						}
					}
					
					$doctor_id = isset($user->profile->doctor_id) ? $user->profile->doctor_id : '';
					if(!empty($doctor_id)){
						$doctorArray = $this->getDoctor($doctor_id);
						if(!empty($doctorArray)){
							$user->profile->doctor->name = $doctorArray;
						}
					}
				}
			}
			*/

			$usersCount = $usersTable->find('all', [
				'conditions'=>[
					'OR'=>[
							['Users.role'=>'lead'],
							['Users.role'=>'patient'],
						]
				]]);
			$ordering['all'] = $usersCount->count();
			
			// Live Leads
			$usersCount = $usersTable->find('all', [
					'conditions'=>[
						'Users.role'=>'lead',
						'Users.status' => 'Live'
					]]);
			$ordering['leads_live'] = $usersCount->count();

			// Dead Leads
			$usersCount = $usersTable->find('all', [
					'conditions'=>[
						'Users.role'=>'lead',
						'Users.status' => 'Dead'
					]]);
			$ordering['leads_dead'] = $usersCount->count();
			
			// Patients
			$usersCount = $usersTable->find('all', [
					'conditions'=>[
						'Users.role'=>'patient',
					]]);
			$ordering['patients_active'] = $usersCount->count();
			
			// Past Patients
			$usersCount = $usersTable->find('all', [
					'conditions'=>[
						'Users.role'=>'patient',
						'Users.status' => 'Past'
					]]);
			$ordering['patients_past'] = $usersCount->count();

			//call queue
			$usersCount = $usersTable->find('all', [
					'conditions'=> [
								'OR'=>[
									['Users.role'=>'lead'],
									['Users.role'=>'patient'],
								],
								'Profiles.call_queue' => '1'
						], 
					'order'=>'Users.id DESC'
						])->contain([
							'Profiles', 
							'UserUploads.Uploads',
							'Profiles.Procedures',
							'Profiles.States'
						]);
			$ordering['call_queue'] = $usersCount->count();

			$statusList = [
				'Active'=>'Active',
				'Live'=>'Live',
				'Dead'=>'Dead',
				'Past'=>'Past',
			]; 
			$user = $this->user;
			$profile_id =  isset($user->profile->id) ? $user->profile->id : '';
		
			$options['fields'] = array('Profiles.id', 'Profiles.firstname', 'Profiles.lastname', 'Activities.content', 'Activities.created');
			$options['order'] = array('Activities.created' =>'DESC');
			$options['conditions'][] = ['Activities.profile_id' => $profile_id];
			$options['conditions'][] = ['OR' => [
													['Activities.type' => 'patient_booking'],
													['Activities.type' => 'patient_login_create'],
													['Activities.type' => 'patient_reviewed'],
													['Activities.type' => 'dr_feedback'],
													['Activities.type' => 'patient_scheduled']
												]
											];
			$this->Activities = TableRegistry::get('Activities');
			$activities = $this->Activities->find('all', 
													$options
												)
												->hydrate(false)
												->contain([
													'Profiles',
												]); 

			$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers', 'ordering', 'statusList', 'user', 'activities','userrole', 'orderType'));
		}
		
	}

	/* Left All search drop down odering functionalty*/
	public function odering(){
		$this->layout = 'ajax';
		$this->autoRender = false;
		$usersTable = TableRegistry::get('Users');
		$usersCount = $usersTable->find('all', [
				'conditions'=>[
					'OR'=>[
							['Users.role'=>'lead'],
							['Users.role'=>'patient'],
						]
				]]);
		$ordering['all'] = $usersCount->count();
		
		// Live Leads
		$usersCount = $usersTable->find('all', [
				'conditions'=>[
					'Users.role'=>'lead',
					'Users.status' => 'Live'
				]]);
		$ordering['leads_live'] = $usersCount->count();

		// Dead Leads
		$usersCount = $usersTable->find('all', [
				'conditions'=>[
					'Users.role'=>'lead',
					'Users.status' => 'Dead'
				]]);
		$ordering['leads_dead'] = $usersCount->count();
		
		// Patients
		$usersCount = $usersTable->find('all', [
				'conditions'=>[
					'Users.role'=>'patient',
				]]);
		$ordering['patients_active'] = $usersCount->count();
		
		// Past Patients
		$usersCount = $usersTable->find('all', [
				'conditions'=>[
					'Users.role'=>'patient',
					'Users.status' => 'Past'
				]]);
		$ordering['patients_past'] = $usersCount->count();


		//call queue
		$usersCount = $usersTable->find('all', [
				'conditions'=> [
								'OR'=>[
									['Users.role'=>'lead'],
									['Users.role'=>'patient'],
								],
								'Profiles.call_queue' => '1'
						], 
				'order'=>'Users.id DESC'
					])->contain([
						'Profiles', 
						'UserUploads.Uploads',
						'Profiles.Procedures',
						'Profiles.States'
					]);
		$ordering['call_queue'] = $usersCount->count();

		
		$this->set(compact('ordering'));
		echo $this->render('/Element/contacts/odering');
		die;
		
	}


	public function index(){
		$this->layout = 'contacts';
        $user = $this->Auth->user();
		$userrole = $user['role'];
	
		// Getting Pre-defined values from database
		// Procedures List
		$procedureTable = TableRegistry::get('Procedures');
		$procedures = $procedureTable->find('list');
		$proceduresList = $procedures->toArray();

		// Lead Sources List
		$LeadSourcesTable = TableRegistry::get('LeadSources');
		$leadSources = $LeadSourcesTable->find('list');
		$leadSourcesList = $leadSources->toArray();

		// State List
		$statesTable = TableRegistry::get('States');
		$statesSources = $statesTable->find('list');
		$stateList = $statesSources->toArray();
		
		// Stage List
		$stageList = [
			'1'=>'Stage 1: Nurture Client',
			'2'=>'Stage 2: Client Interested',
			'3'=>'Stage 3: Client Login',
			'4'=>'Stage 4: Client Form Completed',
			'5'=>'Stage 5: R4aC Reviewed',
			'6'=>'Stage 6: Dr. Notified',
			'7'=>'Stage 7: Dr Completed',
			'8'=>'Stage 8: Client Approved',
			'9'=>'Stage 9: Deposit Requested',
			'10'=>'Stage 10: Deposit Paid',
			'11'=>'Stage 11: Client Travel Added',
			'12'=>'Stage 12: Hotel Confirmed',
			'13'=>'Stage 13: Client Pre-Op Confirmed',
			'14'=>'Stage 14: Post-Op Email Sent'
		];

		$salesrepList = $this->getSalesRepList(); 
		$doctorsList = $this->getDoctorList();
		$driverList = $this->getDriverList();

		$usersTable = TableRegistry::get('Users');
		$profilesTable = TableRegistry::get('Profiles');
		$surgeriesTable = TableRegistry::get('Surgeries');

		if($this->request->is(['post', 'ajax'])){
			$this->layout = 'ajax';
			$searchBy = isset($this->request->data['search_filter_by']) ? $this->request->data['search_filter_by'] : '';
			$searchKey = isset($this->request->data['search_filter_key']) ? $this->request->data['search_filter_key'] : '';
			// 1st Condition : when both empty
			if(empty($searchBy) && empty($searchKey)){
				
				$allUsers = $usersTable->find('all', [
					'conditions'=>[
						'OR'=>[
								['Users.role'=>'lead'],
								['Users.role'=>'patient'],
							]
					]])
					->contain([
							'Calls',
							'Notes',
							'Schedules',
							'Profiles.Procedures',
							'Profiles.SalesReps',
							'Profiles.Doctors', 
							'Profiles.Drivers',
							'Profiles.States', 
							'Profiles.Stages', 
							'Profiles.LeadSources', 
							'UserSurgeries.Surgeries', 
							'UserUploads.Uploads'
						]);

				$allUsers = $this->paginate($allUsers);

				// Get Owner and Doctor and add with user array
				$this->getSalesRepNameArray($allUsers);

				$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
				echo $this->render('/Element/contacts/index');
			}else if(!empty($searchBy) && !empty($searchKey)){
				switch($searchBy){
				case "firstname" :
				case "lastname":
				case "phone" :
					$allUsers = $usersTable->find('all', [
						'conditions'=> [
							'OR' =>[
									['Users.role'=>'lead'],
									['Users.role'=>'patient']
								]
						], 
						'order'=>'Users.id DESC'
							])->contain([
								'Profiles' => [
									'joinType' => 'inner',
									'queryBuilder' =>
											function ($q) use ($searchBy, $searchKey) {
												return $q->where([
												'OR' =>[
														'Profiles.'. $searchBy .' LIKE' => '%'.$searchKey.'%'
														]
														]);
													}], 
								'UserUploads.Uploads',
								'Profiles.Procedures',
								'Profiles.States',
								'Profiles.SalesReps',
								'Schedules',
								'Profiles.Doctors',
								'Profiles.Drivers',
							]);
						$allUsers = $this->paginate($allUsers);

						// Get Owner and Doctor and add with user array
						$this->getSalesRepNameArray($allUsers);

						$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
						echo $this->render('/Element/contacts/index');
				
			        break;
				case "email":
						$allUsers = $usersTable->find('all', [
												'conditions'=>[
													'OR'=>[
														['Users.role'=>'lead'],
														['Users.role'=>'patient'],
													],
													'Users.' . $searchBy . ' LIKE'=> '%'.$searchKey.'%',
												], 
												'order'=>'Users.id DESC'
												])->contain([
													'Profiles',
													'Profiles.Procedures',
													'Profiles.States',
													'Profiles.SalesReps',
													'Schedules',
													'Profiles.Doctors',
													'Profiles.Drivers'
												]);
						$allUsers = $this->paginate($allUsers);
						
						// Get Owner and Doctor and add with user array
						$this->getSalesRepNameArray($allUsers);

						$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
						echo $this->render('/Element/contacts/index');
				
						break;
				case "procedure":
								
					$allUsers = $usersTable->find('all', [
						'conditions'=> [
						'OR' =>[
								['Users.role'=>'lead'],
								['Users.role'=>'patient']
							]
						], 
						'order'=>'Users.id DESC'
							])->contain([
								'Profiles',
								'Profiles.Procedures' => [
									'joinType' => 'left',
									'queryBuilder' =>
											function ($q) use ($searchBy, $searchKey) {
												return $q->where([
												'OR' =>[
														'Procedures.name LIKE' => '%'.$searchKey.'%'
														]
														]);
													}], 
								'Profiles.States',
								'Schedules',
								'Profiles.SalesReps',
								'Profiles.Doctors',
								'Profiles.Drivers'
							]);
						$allUsers = $this->paginate($allUsers);
						
						// Get Owner and Doctor and add with user array
						$this->getSalesRepNameArray($allUsers);

						$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
						echo $this->render('/Element/contacts/index');
				case "state" :

						$allUsers = $usersTable->find('all', [
						'conditions'=> [
						'OR' =>[
								['Users.role'=>'lead'],
								['Users.role'=>'patient']
							]
						], 
						'order'=>'Users.id DESC'
							])->contain([
								'Profiles',
								'Profiles.Procedures', 
								'Profiles.SalesReps',
								'Schedules',
								'Profiles.Doctors',
								'Profiles.Drivers',
								'Profiles.States' => [
									'joinType' => 'inner',
									'queryBuilder' =>
											function ($q) use ($searchBy, $searchKey) {
												return $q->where([
												'OR' =>[
														'States.name LIKE' => '%'.$searchKey.'%'
														]
														]);
													}]
							]);
						$allUsers = $this->paginate($allUsers);

						// Get Owner and Doctor and add with user array
						$this->getSalesRepNameArray($allUsers);

						$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
						echo $this->render('/Element/contacts/index');

					break;
				case "doctor" :
						$allUsers = $usersTable->find('all', [
										'conditions'=>[
												'OR'=> [
													['Users.role'=>'lead'],
													['Users.role'=>'patient']
												],
											],
											'order' =>'Users.id DESC'
										])->contain([
											'Profiles', 
											'Profiles.Doctors' => [
												'joinType' => 'inner',
												'queryBuilder' =>
													function ($q) use ($searchBy, $searchKey) {
														return $q->where([
														'OR' =>[
															'Doctors.username LIKE' => '%'.$searchKey.'%'
															]
														]);
													}],
											'Profiles.Procedures',
											'Profiles.States',
											'Profiles.SalesReps',
											'Schedules',
											'Profiles.Doctors',
											'Profiles.Drivers'
											]);
						$allUsers = $this->paginate($allUsers);

						// Get Owner and Doctor and add with user array
						$this->getSalesRepNameArray($allUsers);

						$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
						echo $this->render('/Element/contacts/index');
					break;
				case "created":
					$allUsers = $usersTable->find('all', [
										'conditions'=>[
												'OR'=> [
													['Users.role'=>'lead'],
													['Users.role'=>'patient']
												],
												['Users.created LIKE' => "%".$searchKey."%"]
											],
											'order' => 'Users.'.$searchBy.' DESC'
										])->contain([
											'Profiles', 
											'Profiles.Procedures',
											'Profiles.States',
											'Profiles.SalesReps',
											'Schedules',
											'Profiles.Doctors',
											'Profiles.Drivers'
											]);

					$allUsers = $this->paginate($allUsers);

					// Get Owner and Doctor and add with user array
					$this->getSalesRepNameArray($allUsers);

					$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
					echo $this->render('/Element/contacts/index');
					break;
				case "SurgeryDate":
					$allUsers = $usersTable->find('all', [
						'conditions'=> [
							'OR' =>[
								['Users.role'=>'lead'],
								['Users.role'=>'patient']
							]
						], 
						])->contain([
								'Profiles',
								'Profiles.Procedures', 
								'Profiles.States',
								'Profiles.SalesReps',
								'Profiles.Doctors',
								'Profiles.Drivers',
								'Schedules' => [
										'joinType' => 'inner',
										'queryBuilder' =>
											function ($q) use ($searchBy, $searchKey) {
												return $q->where(['Schedules.date LIKE' => '%'.$searchKey.'%']);
										}],

						]);

					$allUsers = $this->paginate($allUsers);

					// Get Owner and Doctor and add with user array
					$this->getSalesRepNameArray($allUsers);

					$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
					echo $this->render('/Element/contacts/index');
					break;
				default:
					$allUsers = [];
					break;
				}
			}else if(empty($searchBy) && !empty($searchKey)){ 
				
					$allUsers = $usersTable->find('all', [
										'conditions'=> [
											'OR' => [
												['Users.role' => 'lead'], 
												['Users.role' => 'patient']
											],
										], 
										'order'=>'Users.id DESC'
									])
									->contain([
										'Profiles', 
										'Profiles.SalesReps',
										'Schedules',
										'Profiles.Doctors',
													'Profiles.Drivers',
										'Profiles.Procedures' =>[
											'joinType' => 'INNER',
											'queryBuilder' =>
												function ($q) use ($searchKey) {
													return $q->where([
														'OR' => [
															'Profiles.firstname LIKE' => '%'.$searchKey.'%',
															'Profiles.lastname LIKE' => '%'.$searchKey.'%',
															'Profiles.phone LIKE' => '%'.$searchKey.'%',
															'Profiles.state LIKE' => '%'.$searchKey.'%',
															'Procedures.name LIKE' => '%'.$searchKey.'%',
															'Users.email LIKE' => '%'.$searchKey.'%',
															'Users.username LIKE' => '%'.$searchKey.'%'	
														]
													]);
											}],
										'Profiles.States'
									]);
				$allUsers = $this->paginate($allUsers);

				// Get Owner and Doctor and add with user array
				$this->getSalesRepNameArray($allUsers);

				$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
				echo $this->render('/Element/contacts/index');
				die;
			}else if(!empty($searchBy) && empty($searchKey)){
				switch($searchBy){
					case "firstname" :
					case "lastname":
					case "phone" :
							$allUsers = $usersTable->find('all', [
											'conditions'=>[
													'OR'=> [
														['Users.role'=>'lead'],
														['Users.role'=>'patient']
														]], 
											  'order'=>'Profiles.'.$searchBy.' ASC'
										])->contain([
												'Profiles', 
												'Profiles.Procedures',
												'Profiles.States',
												'Profiles.SalesReps',
												'Schedules',
												'Profiles.Doctors',
												'Profiles.Drivers'	
												]);

						$allUsers = $this->paginate($allUsers);

						// Get Owner and Doctor and add with user array
						$this->getSalesRepNameArray($allUsers);

						$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
						echo $this->render('/Element/contacts/index');
					
						break;

					case "email":
					case "type" :
							if($searchBy == 'type'){
								$searchBy = 'role';
							}
							$allUsers = $usersTable->find('all', [
											'conditions'=> [
												'OR' => [
													['Users.role'=>'lead'],
													['Users.role'=>'patient']
												]
											], 
											'order'=>'Users.'.$searchBy.' ASC'
										])->contain([
												'Profiles',	
												'Profiles.Procedures',
												'Profiles.States',
												'Profiles.SalesReps',
												'Schedules',
												'Profiles.Doctors',
													'Profiles.Drivers'
											]);
						
						$allUsers = $this->paginate($allUsers);

						// Get Owner and Doctor and add with user array
						$this->getSalesRepNameArray($allUsers);

						$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
						echo $this->render('/Element/contacts/index');
					
						break;
					
					case "procedure":
							$allUsers = $usersTable->find('all', [
											'conditions'=>[
													'OR'=> [
														['Users.role'=>'lead'],
														['Users.role'=>'patient']
													]
												],
											])->contain([
												'Profiles', 
												'Profiles.Procedures'=> [
													'joinType' => 'inner',
													'queryBuilder' =>
														function ($q) use ($searchBy, $searchKey) {
															return $q->order(['Procedures.name' => 'DESC']);
													}],
												'Profiles.States',
												'Profiles.SalesReps',
												'Schedules','Profiles.Doctors',
													'Profiles.Drivers'
											]);
						$allUsers = $this->paginate($allUsers);

						// Get Owner and Doctor and add with user array
						$this->getSalesRepNameArray($allUsers);

						$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
						echo $this->render('/Element/contacts/index');
						break;
					case "doctor" :
							$allUsers = $usersTable->find('all', [
											'conditions'=>[
													'OR'=> [
														['Users.role'=>'lead'],
														['Users.role'=>'patient']
													],
												],
												'order' => 'Profiles.doctor_id DESC'
											])->contain([
												'Profiles', 
												'Profiles.Procedures',
												'Profiles.States',
												'Profiles.SalesReps',
												'Schedules','Profiles.Doctors',
													'Profiles.Drivers'
												]);

							$allUsers = $this->paginate($allUsers);

							// Get Owner and Doctor and add with user array
							$this->getSalesRepNameArray($allUsers);

							$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
							echo $this->render('/Element/contacts/index');
						break;
					case "created":
							$allUsers = $usersTable->find('all', [
											'conditions'=>[
													'OR'=> [
														['Users.role'=>'lead'],
														['Users.role'=>'patient']
													],
												],
												'order' => 'Users.created DESC'
											])->contain([
												'Profiles', 
												'Schedules',
												'Profiles.Procedures',
												'Profiles.States',
												'Profiles.SalesReps',
												'Schedules',
												'Profiles.Doctors',
												'Profiles.Drivers'
											]);

							$allUsers = $this->paginate($allUsers);

							// Get Owner and Doctor and add with user array
							$this->getSalesRepNameArray($allUsers);

							$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
							echo $this->render('/Element/contacts/index');
						break;
					case "state" :

						$allUsers = $usersTable->find('all', [
						'conditions'=> [
						'OR' =>[
								['Users.role'=>'lead'],
								['Users.role'=>'patient']
							]
						], 
						])->contain([
							'Profiles',
							'Profiles.Procedures', 
							'Profiles.SalesReps',
							'Schedules',
							'Profiles.Doctors',
							'Profiles.Drivers',
							'Profiles.States' => [
								'joinType' => 'inner',
								'queryBuilder' =>
										function ($q) use ($searchBy, $searchKey) {
											return $q->order(['States.name' => 'ASC']);
												}]
						]);
						$allUsers = $this->paginate($allUsers);

						// Get Owner and Doctor and add with user array
						$this->getSalesRepNameArray($allUsers);

						$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
						echo $this->render('/Element/contacts/index');
					break;
				case "SurgeryDate":
					$allUsers = $usersTable->find('all', [
						'conditions'=> [
						'OR' =>[
								['Users.role'=>'lead'],
								['Users.role'=>'patient']
							]
						], 
						])->contain([
								'Profiles',
								'Profiles.Procedures',
								'Profiles.SalesReps',
								'Profiles.States',
								'Profiles.Doctors',
								'Profiles.Drivers',
								'Schedules' => [
										'joinType' => 'inner',
										'queryBuilder' =>
											function ($q) use ($searchBy, $searchKey) {
												return $q->order([
													'date' => 'DESC'
											]);
										}],

						]);
					
					//pr($allUsers->toArray());
					$allUsers = $this->paginate($allUsers);

					// Get Owner and Doctor and add with user array
					$this->getSalesRepNameArray($allUsers);

					$this->set(Compact('proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers'));
					echo $this->render('/Element/contacts/index');
					break;
				default:
						$allUsers = [];
					break;

					
				}
			}
			die;
		}

		$allUsers = $usersTable->find('all', [
			'conditions'=>[
				'OR'=>[
						['Users.role'=>'lead'],
						['Users.role'=>'patient'],
					]
			], 
			'group'=>'Users.id'])
			->contain([
					'Calls',
					'Profiles',
					'Notes.Profiles',
					'Notes.Updates',
					'Tasks.Profiles',
					'Tasks.Updates',
					'Schedules',
					'Payments',
					'PatientHealths',
					'PatientDietHistories',
					'PatientAllergicInfos',
					'PatientTravelInfos',
					'PatientPreOpInfos',
					'PatientReviewSystems',
					'Profiles.Procedures',
					'Profiles.SalesReps',
					'Profiles.Doctors',
					'Profiles.Drivers',
					'Profiles.States', 
					'Profiles.Stages', 
					'Profiles.LeadSources', 
					'UserSurgeries.Surgeries', 
					'UserUploads.Uploads'
				]);

		$allUsers = $this->paginate($allUsers);
		
		// Get Owner and Doctor and add with user array
		$this->getSalesRepNameArray($allUsers);

		$usersCount = $usersTable->find('all', [
				'conditions'=>[
					'OR'=>[
							['Users.role'=>'lead'],
							['Users.role'=>'patient'],
						]
				]]);
		$ordering['all'] = $usersCount->count();
		
		// Live Leads
		$usersCount = $usersTable->find('all', [
				'conditions'=>[
					'Users.role'=>'lead',
					'Users.status' => 'Live'
				]]);
		$ordering['leads_live'] = $usersCount->count();

		// Dead Leads
		$usersCount = $usersTable->find('all', [
				'conditions'=>[
					'Users.role'=>'lead',
					'Users.status' => 'Dead'
				]]);
		$ordering['leads_dead'] = $usersCount->count();
		
		// Patients
		$usersCount = $usersTable->find('all', [
				'conditions'=>[
					'Users.role'=>'patient',
				]]);
		$ordering['patients_active'] = $usersCount->count();
		
		// Past Patients
		$usersCount = $usersTable->find('all', [
				'conditions'=>[
					'Users.role'=>'patient',
					'Users.status' => 'Past'
				]]);
		$ordering['patients_past'] = $usersCount->count();

		//call queue
		$usersCount = $usersTable->find('all', [
				'conditions'=> [
							'Users.role'=>'lead',
							'Profiles.call_queue' => '1'
				], 
				'order'=>'Users.id DESC'
					])->contain([
						'Profiles', 
						'UserUploads.Uploads',
						'Profiles.Procedures',
						'Profiles.States'
					]);
		$ordering['call_queue'] = $usersCount->count();

		$statusList = [
			'Active'=>'Active',
			'Live'=>'Live',
			'Dead'=>'Dead',
			'Past'=>'Past',
		]; 
		
		$profile_id =  isset($user->profile->id) ? $user->profile->id :'';
	
		$options['fields'] = array('Profiles.id', 'Profiles.firstname', 'Profiles.lastname', 'Activities.content', 'Activities.created');
		$options['order'] = array('Activities.created' =>'DESC');
		$options['conditions'][] = ['Activities.profile_id' => $profile_id];
		$options['conditions'][] = ['OR' => [
												['Activities.type' => 'patient_booking'],
												['Activities.type' => 'patient_login_create'],
												['Activities.type' => 'patient_reviewed'],
												['Activities.type' => 'dr_feedback'],
												['Activities.type' => 'patient_scheduled']
											]
										];
		$this->Activities = TableRegistry::get('Activities');
		$activities = $this->Activities->find('all', 
												$options
											)
											->hydrate(false)
											->contain([
												'Profiles',
											]); 
		$user = $this->user;
		$this->set(Compact('proceduresList', 'salesrepList', 'driverList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'allUsers', 'ordering', 'statusList', 'user', 'activities', 'userrole'));
    }
	
	/*
	* Lead Add 
	*/
	public function add(){
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		// Users Table
		$usersTable = TableRegistry::get('Users');

		// Remove Validation from Users
		$usersTable->validator()->remove('username');
		$usersTable->validator()->remove('password');
		$usersTable->validator()->remove('confirm_password');
		$user = $usersTable->newEntity();

		$profileTable = TableRegistry::get('Profiles');

		// Remove Validation From Profile
		$profileTable->validator()->remove('state');
		$profile = $profileTable->newEntity();

		$UserSurgeriesTable = TableRegistry::get('UserSurgeries');

		$UploadsTable = TableRegistry::get('Uploads');

		$UserUploadsTable = TableRegistry::get('UserUploads');

		if($this->request->is(['post', 'ajax'])){
			
			

			$user = $usersTable->patchEntity($user, $this->request->data['User']);
			$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
			
			// Error check before user save
			if (!$user->errors()) {
				
				// Error check before profile save
				if(!$profile->errors()){
					
					//Adding user_id and role in User table
					$user->parent_id = $this->Auth->user('id');
					$user->role = 'lead';
					$user->status = 'Live';
					
					// Save User
					if ($savedUser = $usersTable->save($user)) {
						
							// Save User Profile ADD user_id and upload_id
							$this->request->data['Profile']['user_id'] = $savedUser->id;
							$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
							if ($savedProfile = $profileTable->save($profile)) {
								
								// User Uploads save
								// Upload file and get ID
								$filesToUpload = isset($this->request->data['Profile']['file']) ? $this->request->data['Profile']['file'] : '';
								if(!empty($filesToUpload)){
									foreach($filesToUpload as $files){
										$uploads = $UploadsTable->newEntity();
										$uploads = $UploadsTable->patchEntity($uploads, $files);
										$upload_id = NULL;
										if($UploadsTable->save($uploads)){
											$upload_id = $uploads->id;

											$uuArray = [
												'user_id'=>$savedUser->id,
												'profile_id'=>$savedProfile->id,
												'upload_id'=>$uploads->id,
											];

											$userUploads = $UserUploadsTable->newEntity();
											$userUploads = $UserUploadsTable->patchEntity($userUploads, $uuArray);
											if($userUploadssaved = $UserUploadsTable->save($userUploads)){
												
											}else{
												echo json_encode($userUploads->errors());
											}
										}	
									}
								}
								
								if($savedProfile->state_id == 1){
									$leadButton = '<button type="button" class="btn btn-warning btn-xs">Scared</button>';
								}else if($savedProfile->state_id == 2){
									$leadButton = '<button type="button" class="btn btn-success btn-xs">Good</button>';
								}else{
									$leadButton = '<button type="button" class="btn btn-danger btn-xs">Hot</button>';
								}
								
								//Get Procedure Name;
								$procedureTable = TableRegistry::get('Procedures');
								$procedure = $procedureTable->get($savedProfile->procedure_id);
								if(empty($procedure->name)){
									$procedure->name = '';
								}
								

								// Save Activity log short content in activity table
								$activity = [
									'user_id'	 => $savedUser->id,
									'profile_id' => $savedProfile->id,
									'content'	 => ucfirst($savedUser->role).' '.$savedProfile->firstname.' '.$savedProfile->lastname.' added',
									'type'		 => $savedUser->role.'_added',
									'updated_by' => $this->Auth->user('Profile.id')
								];
								TableRegistry::get('Activities')->addActivity($activity);
								
								// Return result when all successfull
								$result = [
										'result'=> 'success',
										'msg'	=> 'Lead added successfully', 
										'data'	=> '<tr class="get_right_sidebar_user_leads" id="tr_'.$savedUser->id.'">
														<td><input type="checkbox" name="Users.user_id[]" class="check_user_single" value="'.$savedUser->id.'"/></td>
														<td><strong style="cursor:pointer;">'.$savedProfile->firstname.' '.$savedProfile->lastname.'</strong></td>
														<td><a href="mailto:' . $savedUser->email. '" target="_top"><img src="img/email_icon.png" class="email_icon" alt=""></a></td>
														<td>'.$savedProfile->phone.'</td>
														<td>'.$procedure->name.'</td>
														<td>'.$procedure->name.'</td>
														<td>'.date('m-d-Y', strtotime($savedUser->created)).'</td>
														<td>&mdash;</td>
														<td>'.$leadButton.'</td>
														<td><a title="Login as Client" href="/users/login_as_user/'.base64_encode($savedUser->id).'"><span class="glyphicon glyphicon-log-in"></span></a></td>
													</tr>'
										];

								echo json_encode($result);

							}else{
								echo json_encode(['profile'=>$profile->errors()]);
								$usersTable->delete($user);
							}
					}
				}else{
					echo json_encode(['profile'=>$profile->errors()]);
				}
			}else{
				echo json_encode(['profile' =>$profile->errors(), 'user'=>$user->errors()]);
			}
			die;
		}
	}
	
	/*
	* Patient Task Add 
	*/
	public function add_task(){
		$this->autoRender = false;
		$this->layout = 'ajax';
		if($this->request->is(['post', 'ajax'])){
			
			$userTable = TableRegistry::get('Users');
			$this->Tasks = TableRegistry::get('Tasks');
				
				//$this->Tasks->validator()->remove('description');
			
			$task = $this->Tasks->newEntity();
			
			$this->request->data['Task']['status'] = 'new';
			$this->request->data['Task']['due_date'] = date('Y-m-d', strtotime($this->request->data['Task']['due_date']));
			if($this->request->data['Task']['due_date'] == "1970-01-01"){
				$this->request->data['Task']['due_date'] = "";
			}

			$user_id = isset($this->request->data['Task']['user_id']) ? $this->request->data['Task']['user_id'] : '';
			if(!empty($user_id)){
				$userProfile = $userTable->get($user_id, ['contain'=>'Profiles']);
			}
			$updaterProfile = $userTable->get($this->Auth->user('id'), ['contain'=>'Profiles']);

			if($this->request->is(['post', 'ajax'])){
				$task = $this->Tasks->patchEntity($task, $this->request->data['Task']);	
				$task->section = 'patient';

				if (!$task->errors()){
					if ($this->Tasks->save($task)){

						
						$callsTable = TableRegistry::get('Calls');
						
						// If user ID find Call ID
						if(!empty($user_id)){
							$user = $userTable->get($user_id, [
									'contain' => 'Calls'
								]);

							// If call ID update id ELSE enter new
							if(isset($user->call->id)){
								$call_id = $user->call->id;
								$call = $callsTable->get($call_id);
							}else{
								$call = $callsTable->newEntity();
							}
						}
						
						$call = $callsTable->patchEntity($call, $this->request->data['Call']);
						$call->user_id = $user_id;

						if($savedCalls = $callsTable->save($call)){
							$profileTable = TableRegistry::get('Profiles');
							$profileTable->validator()->remove('firstname');
							$profileTable->validator()->remove('lastname');
							$profileTable->validator()->remove('state');
							$profileTable->validator()->remove('state_id');
							$profileTable->validator()->remove('stage_id');
							$profileTable->validator()->remove('deposit_amount');
							
							$profileTable->validator()->remove('doctor_id');
							$profileTable->validator()->remove('procedure_id');	

							$profile = $profileTable->get(isset($this->request->data['Task']['profile_id']) ? $this->request->data['Task']['profile_id'] : null);
							$profile->state_id = 6;
							$profileTable->save($profile);
						}

						
						$activity = array(
							'user_id'	=> isset($this->request->data['Task']['user_id']) ? $this->request->data['Task']['user_id'] : null,
							'profile_id' => isset($this->request->data['Task']['profile_id']) ? $this->request->data['Task']['profile_id'] : null,
							'content'	=> $this->request->data['Task']['description'],
							'type'		=> 'patient_task',
							'updated_by' => $this->Auth->user('Profile.id')
						);
					

						$dataMsg = '<li>';
							$dataMsg .= '<i class="glyphicon glyphicon-edit"></i>';
							$dataMsg .= '<div class="timeline-item">';
								$dataMsg .= '<div class="speech-box">';
									$dataMsg .= '<p>Task was created  <br>';
									$dataMsg .= '<span>';
									$dataMsg .= 'by ';
									$dataMsg .= ''. isset($updaterProfile->profile->firstname) ? $updaterProfile->profile->firstname : '' .'';
									$dataMsg .= ''. isset($updaterProfile->profile->lastname) ? $updaterProfile->profile->lastname : '' .'';
									$dataMsg .= ' at ';
									$dataMsg .= ''. date('h:i a',strtotime($task->created)).' ';
									$dataMsg .= '</span>';
									$dataMsg .= '</p>';
									$dataMsg .= '<p class="activity-txt">';
									$dataMsg .= isset($task->description) ? $task->description : '';
									$dataMsg .= '</p>';
								$dataMsg .= '</div>';
							$dataMsg .= '</div>';
						$dataMsg .= '</li>';


						TableRegistry::get('Activities')->addActivity($activity);
						$response['result'] = 'success';
						$response['msg'] = 'Task added successfully.';
						$response['data'] = $dataMsg;
						$response['user_id'] = $user_id;
						echo json_encode($response);
						exit();
					}
				}else{
					echo json_encode(['task'=>$task->errors()]);
				}
			}
		}
	}


	/*
	* Right Sidebar Manage Lead Profiles
	*/
	public function right_sidebar(){
		
		$this->layout = 'ajax';
		$this->autoRender = false;
		// Getting Pre-defined values from database

		// Procedures List
		$procedureTable = TableRegistry::get('Procedures');
		$procedures = $procedureTable->find('list');
		$proceduresList = $procedures->toArray();

		// Lead Sources List
		$LeadSourcesTable = TableRegistry::get('LeadSources');
		$leadSources = $LeadSourcesTable->find('list');
		$leadSourcesList = $leadSources->toArray();

		// State List
		$statesTable = TableRegistry::get('States');
		$statesSources = $statesTable->find('list');
		$stateList = $statesSources->toArray();
		
		// Stage List
		$stageList = [
			'1'=>'Stage 1: Nurture Client',
			'2'=>'Stage 2: Client Interested',
			'3'=>'Stage 3: Client Login',
			'4'=>'Stage 4: Client Form Completed',
			'5'=>'Stage 5: R4aC Reviewed',
			'6'=>'Stage 6: Dr. Notified',
			'7'=>'Stage 7: Dr Completed',
			'8'=>'Stage 8: Client Approved',
			'9'=>'Stage 9: Deposit Requested',
			'10'=>'Stage 10: Deposit Paid',
			'11'=>'Stage 11: Client Travel Added',
			'12'=>'Stage 12: Hotel Confirmed',
			'13'=>'Stage 13: Client Pre-Op Confirmed',
			'14'=>'Stage 14: Post-Op Email Sent'
		];

		$salesrepList = $this->getSalesRepList(); 
		$doctorsList = $this->getDoctorList();
		$driverList = $this->getDriverList();
		
		$usersTable = TableRegistry::get('Users');
		$profilesTable = TableRegistry::get('Profiles');
		$surgeriesTable = TableRegistry::get('Surgeries');

		if($this->request->is(['post', 'ajax'])){
			$id = $this->request->data['id'];
			$id = trim($id, 'tr_');

			$allUsers = $usersTable->find('all', [
					'conditions'=>[
						'Users.id'=>$id
					]])
					->contain([
							'Calls',
							'Notes.Profiles',
							'Notes.Updates',
							'Tasks.Profiles',
							'Tasks.Updates',
							'Schedules',
							'Payments',
							'PatientHealths',
							'PatientDietHistories',
							'PatientAllergicInfos',
							'PatientTravelInfos',
							'PatientPreOpInfos',
							'PatientReviewSystems',
							'Profiles.Procedures',
							'Profiles.SalesReps',
							'Profiles.Doctors',
							'Profiles.Drivers',
							'Profiles.States', 
							'Profiles.Stages', 
							'Profiles.LeadSources', 
							'UserSurgeries.Surgeries', 
							'UserUploads.Uploads'
						]);
			$user = $allUsers->first();

			$sales_rep_id = isset($user->profile->sales_rep_id) ? $user->profile->sales_rep_id : '';
			if(!empty($sales_rep_id)){
				$salesArray = $this->getSalesRep($sales_rep_id);
					if(!empty($salesArray)){
						$user->profile->sales_rep->name = $salesArray;
					}
			}
			
			$doctor_id = isset($user->profile->doctor_id) ? $user->profile->doctor_id : '';
			if(!empty($doctor_id)){
				$doctorArray = $this->getDoctor($doctor_id);
						if(!empty($doctorArray)){
							$user->profile->doctor->name = $doctorArray;
						}
			}

			$driver_id = isset($user->profile->driver_id) ? $user->profile->driver_id : '';
			if(!empty($driver_id)){
				$driverArray = $this->getDriver($driver_id);
						if(!empty($driverArray)){
							$user->profile->driver->name = $driverArray;
						}
			}


			$surgeriesList = $surgeriesTable->find('list');
			$surgeriesList = $surgeriesList->toArray();
			$this->set(compact('user', 'surgeriesList', 'proceduresList', 'salesrepList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'driverList'));

			echo $this->render('/Element/contacts/right_sidebar_lead');
			die;
		}
		die;
	}
	
	/*
	* Save Data from right sidebar
	*/
	public function right_sidebar_add($type = null){
		$this->autoRender = false;
		$this->layout = 'ajax';


		if($this->request->is(['post', 'ajax'])){
			$type = $this->request->data['type'];

			switch($type){
				
				case 'booking' :
					
						$usersTable = TableRegistry::get('Users');
						$profileTable = TableRegistry::get('Profiles');
						
						// Remove Validation from Profile table
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');
						
						$profileTable->validator()
											->notEmpty('doctor_id', 'Doctor is required field');
						
						$profile = $profileTable->get($this->request->data['Profile']['id']);
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
						$user_id = isset($this->request->data['User']['id']) ? $this->request->data['User']['id'] : '';

						$callsTable = TableRegistry::get('Calls');
						
						// If user ID find Call ID
						if(!empty($user_id)){
							$user = $usersTable->get($user_id, [
									'contain' => 'Calls'
								]);

							// If call ID update id ELSE enter new
							if(isset($user->call->id)){
								$call_id = $user->call->id;
								$call = $callsTable->get($call_id);
							}else{
								$call = $callsTable->newEntity();
							}
						}
						
						$call = $callsTable->patchEntity($call, $this->request->data['Call']);
						$call->user_id = $user_id;

						// Error check before profile save
						if(!$profile->errors() && !$call->errors()){
							// Save User Profile ADD user_id and upload_id
							$profile->stage_id = '2';
							$profile->state_id = '7';

							if($call->call_result == "Client is not interested"){
								$profile->state_id = '7';
							}

							if ($savedProfile = $profileTable->save($profile)) {
								// Return result when all successfull
								
								if($savedCalls = $callsTable->save($call)){
									
									if($call->call_result == "Client is interested"){
										$m_msg = "Client Booking Done successfully";
										$data = '<div class="col-md-12 login-txt">'.$profile->firstname.' '.$profile->lastname.' stage is now set to Client.</br>Create the Client login details below.</div>';
										
									}else{
										$m_msg = "Saved";
										$data = '<div class="col-md-12 login-txt-danger">'.$profile->firstname.' '.$profile->lastname.' stage is not set to Client.</br>Complete "Step1 : Client Booking" first</div>';
									}

									$result = [
										'result'=> 'success',
										'msg'	=> $m_msg, 
										'data'	=> $data,
										'b_data'=> '<span class="' . $this->getState($call->call_result) .'">' . $call->call_result . '</span>',
										'user_id' => $user_id,
										'status'=>$call->call_result
									];

									// Save Activity log short content in activity table
									$activity = [
										'user_id'	 => $user_id,
										'profile_id' => $profile->id,
										'content'	 => '<b>Client</b> '.$profile->firstname.' '.$profile->lastname.' booking done.',
										'type'		 => 'patient_booking',
										'updated_by' => $this->Auth->user('Profile.id')
									];
									TableRegistry::get('Activities')->addActivity($activity);

									echo json_encode($result);
								}else{
									echo json_encode(['call'=>$call->errors()]);
								}
							}else{
								echo json_encode(['profile'=>$profile->errors()]);
							}
						}else{
							echo json_encode(['profile'=>$profile->errors(), 'call'=>$call->errors()]);
						}
					break;
				case 'login' :
						$usersTable = TableRegistry::get('Users');
							$profileTable = TableRegistry::get('Profiles');
						
						// Remove Validation from Profile table
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('doctor_id');
						$profileTable->validator()->remove('procedure_id');
						$profileTable->validator()->remove('deposit_amount');
						
						$profile = $profileTable->get($this->request->data['Profile']['id']);
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);

						
						
						// Remove validation for email
						$usersTable->validator()->remove('email');

						$user = $usersTable->get($this->request->data['User']['id'], ['contain'=>['Profiles', 'Calls']]);
						
						$saleRep = array();
						if($user->profile->sales_rep_id){
							$saleRep =  $usersTable->get($user->profile->sales_rep_id);							
						}

						$interestedLead = isset($user->call->call_result) ? $user->call->call_result : 'Set one is not completed yet';
						if($interestedLead == "Client is not interested"){
							$result = [
								'result'=> 'error',
								'custom'=> 'Lead is not interested, you cannot create login',
								'data'	=> ''
								];
							echo json_encode($result);
							die();
						}else if($interestedLead == "Set one is not completed yet"){
							$result = [
								'result'=> 'error',
								'custom'=> 'Set one is not completed yet.', 
								'data'	=> '', 
								'user_id'=> $this->request->data['User']['id']
								];
							echo json_encode($result);
							die();
						}

						// Check if entered username is same as saved username
						// Update user state
						$profile->stage_id = '3';
						$profile->state_id = '8';
						if ($savedProfile = $profileTable->save($profile)) {
						// Do nothing
						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}

						$msg = 'Login created successfully';
						$password = 'NOT-UPDATED';
						$usernameNew = isset($this->request->data['User']['username']) ? $this->request->data['User']['username'] : '';
						if(!empty($user->username)){
							$msg = 'Client login updated successfully';	
							$result = [
								'result'=> 'success',
								'msg'	=> '', 
								'data'	=> ''
								];
							echo json_encode($result);
							die();

						}else{
							// Get normal password store in password variable
							$password = isset($this->request->data['User']['password']) ? $this->request->data['User']['password'] : '';
						}
						
							

						$user = $usersTable->patchEntity($user, $this->request->data['User']);
						$user->role = 'patient';
						$user->status = 'Active';
						// Error check before user save
						if (!$user->errors()) {
							// Update User
							
							if ($savedUser = $usersTable->save($user)) {
								// Return result when all successfull
								$result = [
										'result'=> 'success',
										'msg'	=> '', 
										'data'	=> ''
										];
								// Save Activity log short content in activity table
								$activity = [
									'user_id'	 => $this->request->data['User']['id'],
									'profile_id' => $user->profile->id,
									'content'	 => $user->profile->firstname.' '.$user->profile->lastname.' login was created.',
									'type'		 => 'patient_login_created',
									'updated_by' => $this->Auth->user('Profile.id')
								];
								TableRegistry::get('Activities')->addActivity($activity);
								

								// Email
								$url = Router::url('/', true);
								$emailSetting = Configure::read('Emails.login');
								$params = [
									'user'=>[
										'to' => $user->email,
										'cc' => '',
										'bcc' => '',
										'subject'=>	$emailSetting['user']['subject'],
										'mode' => 'html',
										'template' => 'custom_default',
										'label' => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' login was created.',
										'content' => [
												'title' => $emailSetting['user']['title'],
												'firstname' => $user->profile->firstname,
												'lastname' => $user->profile->lastname,
												'username' => $user->username, 
												'password' => $password,
												'url' => $url,
												'text' => $emailSetting['user']['text']
											],
										'type' => 'PATIENT_LOGIN_CONFIRMATION',
										'system_category' => 'patient_login_created',
										'module_type'	=> 'Contact Module'
									],
									'admin'=>[
										'to' => $emailSetting['admin']['to'],
										'cc' => $emailSetting['admin']['cc'],
										'bcc' => $emailSetting['admin']['bcc'],
										'subject'=>$emailSetting['admin']['subject'],
										'mode' => 'html',
										'template' => 'custom_default',
										'label' => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' login was created.',
										'content' => [
												'title' => $emailSetting['admin']['title'],
												'firstname'=>$user->profile->firstname,
												'lastname'=>$user->profile->lastname,
												'username'=>$user->username, 
												'password'=>$password,
												'url'=>$url,
												'text' => $emailSetting['admin']['text']
											],
										'type' => 'PATIENT_LOGIN_CONFIRMATION_TO_R4AC',
										'system_category' => 'patient_login_created',
										'module_type'	=> 'Contact Module'	
									],
									'saleRep'=>[
										'to' => isset($saleRep->email) ? $saleRep->email : '',
										'cc' => '',
										'bcc' => '',
										'subject'=>	$emailSetting['sales_reps']['subject'],
										'mode' => 'html',
										'template' => 'custom_default',
										'label' => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' login was created.',
										'content' => [
												'title' => $emailSetting['sales_reps']['title'],
												'firstname' => $user->profile->firstname,
												'lastname' => $user->profile->lastname,
												'username' => $user->username, 
												'password' => $password,
												'url' => $url,
												'text' => $emailSetting['sales_reps']['text']
											],
										'type' => 'PATIENT_LOGIN_CONFIRMATION_TO_SALESREPS',
										'system_category' => 'patient_login_created',
										'module_type'	=> 'Contact Module'
									]
								];
								$email = $this->EmailTrigger->sendEmail('PATIENT_LOGIN_CONFIRMATION', $params['user']);
								$email = $this->EmailTrigger->sendEmail('PATIENT_LOGIN_CONFIRMATION_TO_R4AC', $params['admin']);
								
								if($saleRep){
									$params['saleRep']['to'] = $saleRep->email;
									$email = $this->EmailTrigger->sendEmail('PATIENT_LOGIN_CONFIRMATION_TO_SALESREPS', $params['saleRep']);
								}

								echo json_encode($result);
							}
						}else{
							echo json_encode(['user'=>$user->errors()]);
						}
						
					break;
				case 'patient_form_completion':
						$usersTable = TableRegistry::get('Users');

						$profileTable = TableRegistry::get('Profiles');
						$schedulesTable = TableRegistry::get('Schedules');
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');
						
						$profileTable->validator()->remove('doctor_id');
						$profileTable->validator()->remove('procedure_id');

						$profile = $profileTable->get($this->request->data['Profile']['id']);
						
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
						
						$user_id = isset($this->request->data['User']['id']) ? $this->request->data['User']['id'] : '';
						
						// If user ID find Call ID
						if(!empty($user_id)){
							// Get user detail
							$user = $usersTable->get($user_id, [
									'contain' => ['Schedules', 'Profiles']
								]);
							

							// Find Doctor detail
							$doctor = $usersTable->find('all', [
								'conditions' => [
									'Users.id' => isset($user->profile->doctor_id) ? $user->profile->doctor_id : ''
								],
								'contain' => ['Profiles']]);

							$doctor = $doctor->first();

						}

						$profile->stage_id = 4; 
						$profile->state_id = 9;

						if ($savedProfile = $profileTable->save($profile)) {
							//Do nothing
						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}

						$result = [
							'result'=> 'success',
							'msg'	=> '', 
							'data'	=> '',
							'b_data'=> '',
							'user_id' => $user_id,
							];
						echo json_encode($result);
					break;
				case 'crm_review' :
						$profileTable = TableRegistry::get('Profiles');
						
						// Remove Validation from Profile table
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');
						
						$profileTable->validator()->remove('procedure_id');
						$profileTable->validator()
											->notEmpty('is_form_reviewed', 'Action is required field');

						$profile = $profileTable->get($this->request->data['Profile']['id']);
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
						
						$usersTable = TableRegistry::get('Users');
						$user = $usersTable->get($this->request->data['User']['id'], ['contain'=>['Profiles', 'UserUploads.Uploads', 'Calls']]);
						
						$doctor = $usersTable->get(isset($user->profile->doctor_id) ? $user->profile->doctor_id : '', ['contain'=>'Profiles']);
						
						if(empty($this->request->data['Profile']['is_form_reviewed'])){
							$this->request->data['Profile']['is_form_reviewed'] = 0;
						}

						$profile->stage_id = 5;
						$profile->state_id = 10;

						// Error check before profile save
						if(!$profile->errors()){
							// Save User Profile ADD user_id and upload_id

							if ($savedProfile = $profileTable->save($profile)) {
								// Return result when all successfull

								$result = [
										'result'=> 'success',
										'msg'	=> 'Review Profile Done successfully', 
										'data'	=> '',
										'user_id' => $this->request->data['User']['id']
										];
								$activity = [
									'user_id'	 => $this->request->data['User']['id'],
									'profile_id' => $this->request->data['Profile']['id'],
									'content'	 => '<b>Client</b> '.$profile->firstname.' '.$profile->lastname.' profile reviewed.',
									'type'		 => 'patient_reviewed',
									'updated_by' => $this->Auth->user('Profile.id')
								];
								TableRegistry::get('Activities')->addActivity($activity);
								
								// Email
								$url = Router::url('/', true);
								$attachment = [];
								$uploads = $user->user_uploads;
								if(!empty($uploads)){
									foreach($uploads as $upload){
										if(!empty($upload['upload']['name'])){
											$attachment[$upload['upload']['name']] = [
												'file' => WWW_ROOT . 'files' . DS . $upload['upload']['name'],
												'mimetype' => $upload['upload']['type'],
												'contentId' => $upload['upload']['id']
											];
										}
									}
								}


								echo json_encode($result);
							}else{
								echo json_encode(['profile'=>$profile->errors()]);
							}
						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}
					break;
				case 'dr_notified' :
						$profileTable = TableRegistry::get('Profiles');
						
						// Remove Validation from Profile table
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');

						$profileTable->validator()->remove('procedure_id');
						//$profileTable->validator()->notEmpty('is_form_reviewed', 'Action is required field');

						$profile = $profileTable->get($this->request->data['Profile']['id']);
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
						
						$usersTable = TableRegistry::get('Users');
						$user = $usersTable->get($this->request->data['User']['id'], ['contain'=>['Profiles', 'Profiles.Doctors', 'Profiles.Drivers', 'UserUploads.Uploads', 'Calls']]);

						$doctor_id = isset($user->profile->doctor_id) ? $user->profile->doctor_id : '';
						if(!empty($doctor_id)){
							$doctorArray = $this->getDoctor($doctor_id);
							if(!empty($doctorArray)){
								$user->profile->doctor->name = $doctorArray;
							}
						}
						if(empty($this->request->data['Profile']['is_dr_notified'])){
							$this->request->data['Profile']['is_dr_notified'] = 0;
						}

						$profile->stage_id = 6;
						$profile->state_id = 11;

						// Error check before profile save
						if(!$profile->errors()){
							// Save User Profile ADD user_id and upload_id

							if ($savedProfile = $profileTable->save($profile)) {
								// Return result when all successfull

								$result = [
										'result'=> 'success',
										'msg'	=> 'Dr. notified successfully', 
										'data'	=> '',
										'user_id' => $this->request->data['User']['id']
										];
								$activity = [
									'user_id'	 => $this->request->data['User']['id'],
									'profile_id' => $this->request->data['Profile']['id'],
									'content'	 => '<b>Client</b> '.$profile->firstname.' '.$profile->lastname.' profile set to doctor for comments.',
									'type'		 => 'dr_notified',
									'updated_by' => $this->Auth->user('Profile.id')
								];
								TableRegistry::get('Activities')->addActivity($activity);
								
								// Email
								$url = Router::url('/', true);
								$attachment = [];
								$uploads = $user->user_uploads;
								if(!empty($uploads)){
									foreach($uploads as $upload){
										if(!empty($upload['upload']['name'])){
											$attachment[$upload['upload']['name']] = [
												'file' => WWW_ROOT . 'files' . DS . $upload['upload']['name'],
												'mimetype' => $upload['upload']['type'],
												'contentId' => $upload['upload']['id']
											];
										}
									}
								}
								
								$emailSetting = Configure::read('Emails.patient_review');
								$params = [
									'doctor'=>[
										'to' => $user->profile->doctor->email,
										'cc' => '',
										'bcc' => '',
										'subject'=> $emailSetting['doctor']['subject'],
										'mode' => 'html',
										'template' => 'custom_default',
										'label' => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' profile to doctor for review.',
										'content' => [
												'title' => $emailSetting['doctor']['title'],
												'title' => $emailSetting['doctor']['title'],
												'dr_firstname' =>$user->profile->doctor->name->firstname,
												'dr_lastname' => $user->profile->doctor->name->lastname,
												'firstname' => $user->profile->firstname,
												'lastname' => $user->profile->lastname,
												'username' => $user->username, 
												'attachment' => $attachment,
												'url' => $url,
												'text' => $emailSetting['doctor']['text'],
											],
										'type' => 'PATIENT_DETAIL_TO_DOCTOR_FOR_REVIEW',
										'system_category' => 'patient_profile_sent_to_doctor',
										'module_type'	=> 'Contact Module'
									]
								];

								$email = $this->EmailTrigger->sendEmail('PATIENT_DETAIL_TO_DOCTOR_FOR_REVIEW', $params['doctor']);

								echo json_encode($result);
							}else{
								echo json_encode(['profile'=>$profile->errors()]);
							}
						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}
					break;
				case 'dr_comment' :
						$profileTable = TableRegistry::get('Profiles');
						
						// Remove Validation from Profile table
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');
						
						$profileTable->validator()->remove('procedure_id');
						
						// Add validations
						$profileTable->validator()
											->notEmpty('doctor_feedback', 'Action is required field');
						$profile = $profileTable->get($this->request->data['Profile']['id']);
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
						
						$usersTable = TableRegistry::get('Users');
						$user = $usersTable->get($this->request->data['User']['id'], ['contain'=>['Profiles', 'UserUploads.Uploads']]);
						
						$doctor = $usersTable->get(isset($user->profile->doctor_id) ? $user->profile->doctor_id : '', ['contain'=>'Profiles']);
						
						$profile->doctor_feedback = $this->request->data['Profile']['doctor_feedback'];

						$profile->is_doctor_approve = ($profile->doctor_feedback == "Approve") ? 1 : 0;
						
						// Error check before profile save
						$profile->stage_id = 7;
						$profile->state_id = 12;

						if(!$profile->errors()){
							// Save User Profile ADD user_id and upload_id
							if ($savedProfile = $profileTable->save($profile)) {
								// Return result when all successfull

								$result = [
										'result'=> 'success',
										'msg'	=> 'Doctor feedback Done successfully', 
										'data'	=> '',
										'user_id' => $this->request->data['User']['id']
										];

								$activity = [
									'user_id'	 => $this->request->data['User']['id'],
									'profile_id' => $this->request->data['Profile']['id'],
									'content'	 => '<b>Dr '.$doctor->profile->firstname.' '.$doctor->profile->lastname.'</b> '.$profile->doctor_feedback.' Client '.$profile->firstname.' '.$profile->lastname.'.',
									'type'		 => 'dr_feedback',
									'updated_by' => $this->Auth->user('Profile.id')
								];
								TableRegistry::get('Activities')->addActivity($activity);
								
								// Email
								$url = Router::url('/', true);
								$attachment = [];

								$emailSetting = Configure::read('Emails.patient_status.approve');
								// Email Functionality
								$params = [
									'approve'=>[
										'to' => isset($user->email) ? $user->email: 'noemail@gotchamobi.com',
										'cc' => '',
										'bcc' => '',
										'subject'=> $emailSetting['subject'],
										'mode' => 'html',
										'template' => 'custom_default',
										'label' => '<b>Dr '.$doctor->profile->firstname.' '. $doctor->profile->lastname .'</b> '. $profile->doctor_feedback .' Client '. $profile->firstname .' '. $profile->lastname .'.',
										'content' => [
											'title' => $emailSetting['title'],
											'dr_firstname'=>$doctor->profile->firstname,
											'dr_lastname'=>$doctor->profile->lastname,
											'firstname'=>$user->profile->firstname,
											'lastname'=>$user->profile->lastname,
											'username'=>$user->username,
											'url'=> $url,
											'status'=> $profile->doctor_feedback,
											'text' => $emailSetting['text']
										],
										'type' => 'CONGRATULATION_TO_PATIENT_FOR_APPROVAL',
										'system_category' => 'dr_feedback',
										'module_type'	=> 'Contact Module'	
									]
								];		
								
								if($profile->doctor_feedback == "Approve"){
									$email = $this->EmailTrigger->sendEmail('CONGRATULATION_TO_PATIENT_FOR_APPROVAL', $params['approve']);
								}

								echo json_encode($result);
							}else{
								echo json_encode(['profile'=>$profile->errors()]);
							}
						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}

					break;
				case 'patient_approve_schedule' :
						$usersTable = TableRegistry::get('Users');

						$profileTable = TableRegistry::get('Profiles');
						$schedulesTable = TableRegistry::get('Schedules');
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');
						
						$profileTable->validator()->remove('doctor_id');
						$profileTable->validator()->remove('procedure_id');

						$profile = $profileTable->get($this->request->data['Profile']['id']);
						
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);

						$datePost = isset($this->request->data['Schedule']['date']) ? $this->request->data['Schedule']['date'] : '';
						if(!empty($datePost)){
							// Convert Date into mysql Date
							$datePost = explode(' ',$datePost);
							$datePost = end($datePost);

							$datePost = date("Y-m-d", strtotime(trim(str_replace('-', '/', $datePost))));

							$this->request->data['Schedule']['date'] = $datePost;
						}
						
						$timePost = isset($this->request->data['Schedule']['time']) ? $this->request->data['Schedule']['time'] : '';
						if(!empty($timePost)){
							// Convert Time into mysql Time
							$timePost = date("H:i", strtotime($timePost));
							$this->request->data['Schedule']['time'] = $timePost;
						}
						$user_id = isset($this->request->data['User']['id']) ? $this->request->data['User']['id'] : '';
						// If user ID find Call ID
						if(!empty($user_id)){
							// Get user detail
							$user = $usersTable->get($user_id, [
									'contain' => ['Schedules', 'Profiles']
								]);
							

							// Find Doctor detail
							$doctor = $usersTable->find('all', [
								'conditions' => [
									'Users.id' => isset($user->profile->doctor_id) ? $user->profile->doctor_id : ''
								],
								'contain' => ['Profiles']]);

							$doctor = $doctor->first();

							// If call ID update id ELSE enter new
							if(isset($user->schedule->id)){
								$schedule_id = $user->schedule->id;
								$schedule = $schedulesTable->get($schedule_id);
							}else{
								$schedule = $schedulesTable->newEntity();
							}
						}
						
						$schedule = $schedulesTable->patchEntity($schedule, $this->request->data['Schedule']);
						$schedule->user_id = $user_id;
						$schedule->profile_id = $profile->id;
						$schedule->doctor_id = isset($user->profile->doctor_id) ? $user->profile->doctor_id : '';
						
						$profile->stage_id = 8; // step8
						$profile->state_id = 13;

						if ($savedProfile = $profileTable->save($profile)) {
							//Do nothing
						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}

						
						// Error check before profile save
						if(!$schedule->errors()){ 
							// Save Schedule
							if ($savedSchedule = $schedulesTable->save($schedule)) {
								// Return result when all successfull
								$result = [
									'result'=> 'success',
									'msg'	=> 'Client scheduling done successfully', 
									'data'	=> '',
									'b_data'=> isset($savedSchedule->date) ? date('m-d-Y', strtotime($savedSchedule->date)) : '',
									'user_id' => $user_id,
									];
								
								$activity = [
									'user_id'	 => $this->request->data['User']['id'],
									'profile_id' => $this->request->data['Profile']['id'],
									'content'	 => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' surgery schedule was set.',
									'type'		 => 'patient_scheduled',
									'updated_by' => $this->Auth->user('Profile.id')
								];
								TableRegistry::get('Activities')->addActivity($activity);
								
								// Email 
								$url = Router::url('/', true);
								$scheduleSetting = Configure::read('Emails.patient_schedule');
								$params = [
									'user'=>[
										'to' => $user->email,
										'cc' => '',
										'bcc' => '',
										'subject'=> $scheduleSetting['user']['subject'],
										'title'=> $scheduleSetting['user']['title'],
										'mode' => 'html',
										'template' => 'custom_default',
										'label' => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' scheduled.',
										'content' => [
												'title' => $scheduleSetting['user']['title'],
												'dr_firstname'=> isset($doctor->profile->firstname) ? $doctor->profile->firstname : '',
												'dr_lastname'=>	isset($doctor->profile->lastname) ? $doctor->profile->lastname : '',
												'firstname' => $user->profile->firstname,
												'lastname' => $user->profile->lastname,
												'date' => date('m-d-Y', strtotime($schedule->date)),
												'time' => date("h:i a", strtotime($schedule->time)),
												'url' => $url,
												'text' => $scheduleSetting['user']['text']
											],
										'type' => 'CONGRATULATION_TO_PATIENT_AFTER_APPROVAL',
										'system_category' => 'patient_scheduled',
										'module_type'	=> 'Contact Module'
									]
								];		
								$email = $this->EmailTrigger->sendEmail('CONGRATULATION_TO_PATIENT_AFTER_APPROVAL', $params['user']);
								

								echo json_encode($result);
							}else{
								echo json_encode(['schedule'=>$schedule->errors()]);
							}
						}else{
							echo json_encode(['schedule'=>$schedule->errors()]);
						}
					break;
				case 'deposite_request':
						$usersTable = TableRegistry::get('Users');

						$profileTable = TableRegistry::get('Profiles');
						$schedulesTable = TableRegistry::get('Schedules');
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						
						$profileTable->validator()->remove('doctor_id');
						$profileTable->validator()->remove('procedure_id');
						
						// Add validations
						$profileTable->validator()
											->notEmpty('deposit_amount', 'Amount is required field');
						
						$profile = $profileTable->get($this->request->data['Profile']['id']);
						
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
						
						$user_id = isset($this->request->data['User']['id']) ? $this->request->data['User']['id'] : '';
						
						// If user ID find Call ID
						if(!empty($user_id)){
							// Get user detail
							$user = $usersTable->get($user_id, [
									'contain' => ['Schedules', 'Profiles']
								]);
							

							// Find Doctor detail
							$doctor = $usersTable->find('all', [
								'conditions' => [
									'Users.id' => isset($user->profile->doctor_id) ? $user->profile->doctor_id : ''
								],
								'contain' => ['Profiles']]);

							$doctor = $doctor->first();

						}

						if(empty($this->request->data['Profile']['is_deposite_request'])){
							$this->request->data['Profile']['is_deposite_request'] = 0;
						}
						$profile->stage_id = 9; // step8
						$profile->state_id = 14;
						
						if ($savedProfile = $profileTable->save($profile)) {
							// Return result when all successfull
							$result = [
								'result'=> 'success',
								'msg'	=> 'Deposit request send successfully', 
								'data'	=> '',
								'b_data'=> '',
								'user_id' => $user_id,
								];
							
							$activity = [
								'user_id'	 => $this->request->data['User']['id'],
								'profile_id' => $this->request->data['Profile']['id'],
								'content'	 => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' is requested for deposit.',
								'type'		 => 'patient_deposit_request',
								'updated_by' => $this->Auth->user('Profile.id')
							];
							TableRegistry::get('Activities')->addActivity($activity);							
							
							// Email 
							$url = Router::url('/', true);
							$paymentSetting = Configure::read('Emails.payment');
							$params = [
								'payment_request'=>[
									'to' => $user->email,
									'cc' => '',
									'bcc' => '',
									'subject'=> $paymentSetting['request']['subject'],
									'mode' => 'html',
									'template' => 'custom_default',
									'label' => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' scheduled.',
									'content' => [
											'title'			=> $paymentSetting['admin']['title'],
											'dr_firstname'	=> $doctor->profile->firstname,
											'dr_lastname'	=> $doctor->profile->lastname,
											'firstname'		=> $user->profile->firstname,
											'lastname'		=> $user->profile->lastname,
											'date'			=> '',
											'time'			=> '',
											'url'			=> $url,
											'user_id'		=> base64_encode($user->id),
											'uuid'			=> Text::uuid(),
											'deposit_amount'=> $profile->deposit_amount,
											'payment_url'	=> $paymentSetting['request']['payment_url'],
											'text'			=> $paymentSetting['request']['text'] 
										],
									'type' => 'PAYMENT_REQUEST_TO_PATIENT_AFTER_SCHEDULE',
									'system_category' => 'payment_request',
									'module_type'	=> 'Contact Module'
								]
							];		
							
							$email = $this->EmailTrigger->sendEmail('PAYMENT_REQUEST_TO_PATIENT_AFTER_SCHEDULE', $params['payment_request']);
							echo json_encode($result);

						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}						
					break;				
				case 'deposite_paid':
						$usersTable = TableRegistry::get('Users');

						$profileTable = TableRegistry::get('Profiles');
						$schedulesTable = TableRegistry::get('Schedules');
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');
						
						$profileTable->validator()->remove('doctor_id');
						$profileTable->validator()->remove('procedure_id');

						$profile = $profileTable->get($this->request->data['Profile']['id']);
						
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
						
						$user_id = isset($this->request->data['User']['id']) ? $this->request->data['User']['id'] : '';
						
						// If user ID find Call ID
						if(!empty($user_id)){
							// Get user detail
							$user = $usersTable->get($user_id, [
									'contain' => ['Schedules', 'Profiles']
								]);
						}
						
						$driver_id = isset($user->profile->driver_id) ? $user->profile->driver_id : '';
						$driver = array();
						if(!empty($driver_id)){
							// Find Driver detail
							$driver = $usersTable->find('all', [
								'conditions' => [
									'Users.id' => $driver_id
								],
								'contain' => ['Profiles']]);
							$driver = $driver->first();
						}
						$driverEmail = isset($driver->email) ? $driver->email : '';

						if(empty($this->request->data['Profile']['is_deposite_paid'])){
							$this->request->data['Profile']['is_deposite_paid'] = 0;
						}
						$profile->stage_id = 10; // step8
						$profile->state_id = 15;
						$first_name = isset($profile->firstname) ? $profile->firstname : '';
						$last_name = isset($profile->lastname) ? $profile->lastname : '';

						if ($savedProfile = $profileTable->save($profile)) {
							//Do nothing
							$url = Router::url('/', true);
							$emailTravelinfo = Configure::read('Emails.travel_info');
							$params = [
								'travel_info'=>[
									'to' => isset($user->email) ? $user->email : 'noemail@gotchamobi.com',
									'cc' => '',
									'bcc' => '',
									'subject'=> $emailTravelinfo['user']['title'],
									'mode' => 'html',
									'template' => 'custom_default',
									'label' => '<b>Client</b> '.$first_name.' '.$last_name.' travel info request reminder.',
									'content' => [
											'title'			=> $emailTravelinfo['user']['title'],
											'firstname'		=> $first_name,
											'lastname'		=> $last_name,
											'text'			=> $emailTravelinfo['user']['text']
										],
									'type' => 'REMINDER_TO_PATIENT_ABOUT_TRAVEL_INFO',
									'system_category' => 'travel_info_request',
									'module_type'	=> 'Payment Module'
								],
								'driver'=>[
									'to' => $driverEmail,
									'cc' => '',
									'bcc' => '',
									'subject'=> $emailTravelinfo['driver']['title'],
									'mode' => 'html',
									'template' => 'custom_default',
									'label' => '<b>Client</b> '.$first_name.' '.$last_name.' travel info reminder to driver.',
									'content' => [
											'title'				=> $emailTravelinfo['driver']['title'],
											'driver_firstname'	=> isset($driver->profile->firstname) ? $driver->profile->firstname : '',
											'driver_lastname'	=> isset($driver->profile->lastname) ? $driver->profile->lastname : '',
											'firstname'			=> $first_name,
											'lastname'			=> $last_name,
											'text'				=> $emailTravelinfo['driver']['text']
										],
									'type' => 'REMINDER_TO_DRIVER_ABOUT_TRAVEL_INFO',
									'system_category' => 'travel_info_to_driver',
									'module_type'	=> 'Travel Module'
								]
							];	

							$email = $this->EmailTrigger->sendEmail('REMINDER_TO_PATIENT_ABOUT_TRAVEL_INFO', $params['travel_info']);
							if(!empty($driverEmail)){
								$email = $this->EmailTrigger->sendEmail('REMINDER_TO_DRIVER_ABOUT_TRAVEL_INFO', $params['driver']);
							}


						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}

						// Return result when all successfull
						$result = [
							'result'=> 'success',
							'msg'	=> 'Deposit Paid confirmed successfully', 
							'data'	=> '',
							'b_data'=> '',
							'user_id' => $user_id,
							];
						
						$activity = [
							'user_id'	 => $this->request->data['User']['id'],
							'profile_id' => $this->request->data['Profile']['id'],
							'content'	 => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' is requested for deposit.',
							'type'		 => 'patient_deposit_request',
							'updated_by' => $this->Auth->user('Profile.id')
						];
						TableRegistry::get('Activities')->addActivity($activity);
						echo json_encode($result);
							
					break;
				case 'patient_travel_info':
						$usersTable = TableRegistry::get('Users');

						$profileTable = TableRegistry::get('Profiles');
						$schedulesTable = TableRegistry::get('Schedules');
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');

						$profileTable->validator()->remove('doctor_id');
						$profileTable->validator()->remove('procedure_id');

						$profile = $profileTable->get($this->request->data['Profile']['id']);
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
						$user_id = isset($this->request->data['User']['id']) ? $this->request->data['User']['id'] : '';
						
						$patientTravelTable = TableRegistry::get('PatientTravelInfos');
						$travelInfo = $patientTravelTable->find('all', ['conditions'=>['user_id'=>$user_id]]);
						$travelData = $travelInfo->first();
						
						$arrivalDate = isset($travelData['arrival_date']) ? $travelData['arrival_date'] : '';
						$departureDate = isset($travelData['departure_date']) ? $travelData['departure_date'] : '';
						
						$tasksTable = TableRegistry::get('Tasks');
						

						if(!empty($arrivalDate)){
							if($arrivalDate){
								$this->request->data['Task']['user_id'] = $travelData['user_id'];
								$this->request->data['Task']['profile_id'] = $travelData['profile_id'];
								$this->request->data['Task']['updated_by'] = $this->Auth->user('id');
								$this->request->data['Task']['description'] = $profile->firstname.' '.$profile->lastname.' Arrival Date: '. date('m-d-Y', strtotime($arrivalDate));
								$this->request->data['Task']['due_date'] = $travelData['arrival_date'];
								$this->request->data['Task']['due_time'] = $travelData['arrival_time'];
								$this->request->data['Task']['reminder_duration'] = '86400000';
								$this->request->data['Task']['notification_type'] = 'email';
								$this->request->data['Task']['section'] = 'flight';

								$task = $tasksTable->newEntity();
								$task = $tasksTable->patchEntity($task, $this->request->data['Task']);	
								if(!$task->errors()){
									$tasksTable->save($task);
								}
							}
						}
						if(!empty($departureDate)){
							if($departureDate){
								$this->request->data['Task']['user_id'] = $travelData['user_id'];
								$this->request->data['Task']['profile_id'] = $travelData['profile_id'];
								$this->request->data['Task']['updated_by'] = $this->Auth->user('id');
								$this->request->data['Task']['description'] = $profile->firstname.' '.$profile->lastname.' Departure Date: '. date('m-d-Y', strtotime($departureDate));
								$this->request->data['Task']['due_date'] = $travelData['departure_date'];
								$this->request->data['Task']['due_time'] = $travelData['departure_time'];
								$this->request->data['Task']['reminder_duration'] = '86400000';
								$this->request->data['Task']['notification_type'] = 'email';
								$this->request->data['Task']['section'] = 'flight';
								
								$task = $tasksTable->newEntity();
								$task = $tasksTable->patchEntity($task, $this->request->data['Task']);	
								if(!$task->errors()){
									$tasksTable->save($task);
								}
							}
						}
						
						
						
						
						// If user ID find Call ID
						if(!empty($user_id)){
							// Get user detail
							$user = $usersTable->get($user_id, [
									'contain' => ['Schedules', 'Profiles']
								]);
							

							// Find Doctor detail
							$doctor = $usersTable->find('all', [
								'conditions' => [
									'Users.id' => isset($user->profile->doctor_id) ? $user->profile->doctor_id : ''
								],
								'contain' => ['Profiles']]);

							$doctor = $doctor->first();

						}
						

						$profile->stage_id = 11; // step8
						$profile->state_id = 16;
		
						if ($savedProfile = $profileTable->save($profile)) {
							//Do nothing
						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}

						$result = [
							'result'=> 'success',
							'msg'	=> '', 
							'data'	=> '',
							'b_data'=> '',
							'user_id' => $user_id,
							];
						echo json_encode($result);
					break;
				case 'hotel':
						$usersTable = TableRegistry::get('Users');

						$profileTable = TableRegistry::get('Profiles');
						$schedulesTable = TableRegistry::get('Schedules');
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');
						
						$profileTable->validator()->remove('doctor_id');
						$profileTable->validator()->remove('procedure_id');

						$profile = $profileTable->get($this->request->data['Profile']['id']);
						
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
						
						$user_id = isset($this->request->data['User']['id']) ? $this->request->data['User']['id'] : '';

						$is_hotel_confirmed = isset($this->request->data['Profile']['is_hotel_confirmed']) ? $this->request->data['Profile']['is_hotel_confirmed'] : '';
						
						$result = ($is_hotel_confirmed == 1) ? "success" : "not_confirmed";					

						// If user ID find Call ID
						if(!empty($user_id)){
							// Get user detail
							$user = $usersTable->get($user_id, [
									'contain' => ['Schedules', 'Profiles']
								]);
							

							// Find Doctor detail
							$doctor = $usersTable->find('all', [
								'conditions' => [
									'Users.id' => isset($user->profile->doctor_id) ? $user->profile->doctor_id : ''
								],
								'contain' => ['Profiles']]);

							$doctor = $doctor->first();

						}

						//If hotel is confirmed, send email
						if($is_hotel_confirmed == 1){
								$url = Router::url('/', true);
								$emailSetting = Configure::read('Emails.hotel_confirm');
								$emailSettingPreopt = Configure::read('Emails.pre_opt');
								$params = [
									'user'=>[
										'to' => $user->email,
										'cc' => '',
										'bcc' => '',
										'subject'=>	$emailSetting['user']['subject'],
										'mode' => 'html',
										'template' => 'custom_default',
										'label' => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' hotel was confirmed.',
										'content' => [
												'title' => $emailSetting['user']['title'],
												'firstname' => $user->profile->firstname,
												'lastname' => $user->profile->lastname,
												'text' => $emailSetting['user']['text']
											],
										'type' => 'PATIENT_HOTEL_CONFIRMATION',
										'system_category' => 'patient_hotel_confirmed',
										'module_type'	=> 'Contact Module'
									],
									'pre_opt'=>[
										'to' => $user->email,
										'cc' => '',
										'bcc' => '',
										'subject'=>	$emailSettingPreopt['inform_user']['subject'],
										'mode' => 'html',
										'template' => 'custom_default',
										'label' => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' is remind to read pre opt info.',
										'content' => [
												'title' => $emailSettingPreopt['inform_user']['title'],
												'firstname' => $user->profile->firstname,
												'lastname' => $user->profile->lastname,
												'text' => $emailSettingPreopt['inform_user']['text']
											],
										'type' => 'PATIENT_PRE_OPT_INFO_READ_REMINDER',
										'system_category' => 'patient_pre_opt_info_reminder',
										'module_type'	=> 'Contact Module'
									]	
								];
								$email = $this->EmailTrigger->sendEmail('PATIENT_HOTEL_CONFIRMATION', $params['user']);
								$email2 = $this->EmailTrigger->sendEmail('PATIENT_PRE_OPT_INFO_READ_REMINDER', $params['pre_opt']);
						}

						$profile->stage_id = 12; // step8
						$profile->state_id = 17;

						if ($savedProfile = $profileTable->save($profile)) {
							//Do nothing
						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}
						
						$result = [
							'result'=> $result,
							'msg'	=> '', 
							'data'	=> '',
							'b_data'=> '',
							'user_id' => $user_id,
							];
						echo json_encode($result);
					break;
				case 'pre_opt_confirmed':
						$usersTable = TableRegistry::get('Users');

						$profileTable = TableRegistry::get('Profiles');
						$schedulesTable = TableRegistry::get('Schedules');
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');
						
						$profileTable->validator()->remove('doctor_id');
						$profileTable->validator()->remove('procedure_id');

						$profile = $profileTable->get($this->request->data['Profile']['id']);
						
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
						
						$user_id = isset($this->request->data['User']['id']) ? $this->request->data['User']['id'] : '';
						
						// If user ID find Call ID
						if(!empty($user_id)){
							// Get user detail
							$user = $usersTable->get($user_id, [
									'contain' => ['Schedules', 'Profiles']
								]);
							

							// Find Doctor detail
							$doctor = $usersTable->find('all', [
								'conditions' => [
									'Users.id' => isset($user->profile->doctor_id) ? $user->profile->doctor_id : ''
								],
								'contain' => ['Profiles']]);

							$doctor = $doctor->first();

						}

						$profile->stage_id = 13; // step8
						$profile->state_id = 18;

						if ($savedProfile = $profileTable->save($profile)) {
							//Do nothing
						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}

						$result = [
							'result'=> 'success',
							'msg'	=> '', 
							'data'	=> '',
							'b_data'=> '',
							'user_id' => $user_id,
							];
						echo json_encode($result);
					break;
				case 'surgery_in_process':
						$usersTable = TableRegistry::get('Users');

						$profileTable = TableRegistry::get('Profiles');
						$schedulesTable = TableRegistry::get('Schedules');
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');
						
						$profileTable->validator()->remove('doctor_id');
						$profileTable->validator()->remove('procedure_id');

						$profile = $profileTable->get($this->request->data['Profile']['id']);
						
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
						
						$user_id = isset($this->request->data['User']['id']) ? $this->request->data['User']['id'] : '';
						
						// If user ID find Call ID
						if(!empty($user_id)){
							// Get user detail
							$user = $usersTable->get($user_id, [
									'contain' => ['Schedules', 'Profiles']
								]);
							

							// Find Doctor detail
							$doctor = $usersTable->find('all', [
								'conditions' => [
									'Users.id' => isset($user->profile->doctor_id) ? $user->profile->doctor_id : ''
								],
								'contain' => ['Profiles']]);

							$doctor = $doctor->first();

						}

						$profile->stage_id = 14; // step8
						$profile->state_id = 19;

						if ($savedProfile = $profileTable->save($profile)) {
								$user = $usersTable->get($this->request->data['User']['id'], ['contain'=>['Profiles']]);

								$profile = $profileTable->get($this->request->data['Profile']['id']);
								
								$profile->is_surgery_completed = $this->request->data['Profile']['is_surgery_completed'];
								$profileSave = $profileTable->save($profile);

						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}

						$result = [
							'result'=> 'success',
							'msg'	=> 'Surgery completed send post op email', 
							'data'	=> '',
							'b_data'=> '',
							'user_id' => $user_id,
							];
						echo json_encode($result);
					break;
				case 'post_opt_email':
						$usersTable = TableRegistry::get('Users');

						$profileTable = TableRegistry::get('Profiles');
						$schedulesTable = TableRegistry::get('Schedules');
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');
						
						$profileTable->validator()->remove('doctor_id');
						$profileTable->validator()->remove('procedure_id');

						$profile = $profileTable->get($this->request->data['Profile']['id']);
						
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
						
						$user_id = isset($this->request->data['User']['id']) ? $this->request->data['User']['id'] : '';
						
						// If user ID find Call ID
						if(!empty($user_id)){
							// Get user detail
							$user = $usersTable->get($user_id, [
									'contain' => ['Schedules', 'Profiles']
								]);
							

							// Find Doctor detail
							$doctor = $usersTable->find('all', [
								'conditions' => [
									'Users.id' => isset($user->profile->doctor_id) ? $user->profile->doctor_id : ''
								],
								'contain' => ['Profiles']]);

							$doctor = $doctor->first();

						}

						$profile->stage_id = 15;
						$profile->state_id = 20;

						if ($savedProfile = $profileTable->save($profile)) {
								$user = $usersTable->get($this->request->data['User']['id'], ['contain'=>['Profiles']]);

								$profile = $profileTable->get($this->request->data['Profile']['id']);

								// Email
								$url = Router::url('/', true);

								$emailSetting = Configure::read('Emails.post_opt');

								$params = [
									'satisfaction'=>[
										'to' => $user->email,
										'cc' => '',
										'bcc' => '',
										'subject'=>	$emailSetting['satisfaction']['subject'],
										'mode' => 'html',
										'template' => 'custom_default',
										'replyTo' => 'noReply@ready4achange.com',
										'label' => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' satisfaction email sent.',
										'content' => [
												'title' => $emailSetting['satisfaction']['title'],
												'firstname' => $user->profile->firstname,
												'lastname' => $user->profile->lastname,
												'url' => $url,
												'text' => $emailSetting['satisfaction']['text']
											],
										'type' => 'PATIENT_POST_OPT_INFO',
										'system_category' => 'patient_satisfaction',
										'module_type'	=> 'Client portal'
									],
									'support'=>[
										'to' => $user->email,
										'cc' => '',
										'bcc' => '',
										'subject'=>	$emailSetting['support']['subject'],
										'mode' => 'html',
										'template' => 'custom_default',
										'replyTo' => 'noReply@ready4achange.com',
										'label' => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' support email sent.',
										'content' => [
												'title' => $emailSetting['support']['title'],
												'firstname' => $user->profile->firstname,
												'lastname' => $user->profile->lastname,
												'url' => $url,
												'text' => $emailSetting['support']['text']
											],
										'type' => 'PATIENT_POST_OPT_INFO',
										'system_category' => 'patient_support',
										'module_type'	=> 'Client portal'
									],
									'services_opportunities'=>[
										'to' => $user->email,
										'cc' => '',
										'bcc' => '',
										'subject'=>	$emailSetting['services_opportunities']['subject'],
										'mode' => 'html',
										'template' => 'custom_default',
										'replyTo' => 'noReply@ready4achange.com',
										'label' => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' services_opportunities email sent.',
										'content' => [
												'title' => $emailSetting['services_opportunities']['title'],
												'firstname' => $user->profile->firstname,
												'lastname' => $user->profile->lastname,
												'url' => $url,
												'text' => $emailSetting['services_opportunities']['text']
											],
										'type' => 'PATIENT_POST_OPT_INFO',
										'system_category' => 'patient_services_opportunities',
										'module_type'	=> 'Client portal'
									],
									'refferal_request'=>[
										'to' => $user->email,
										'cc' => '',
										'bcc' => '',
										'subject'=>	$emailSetting['refferal_request']['subject'],
										'mode' => 'html',
										'template' => 'custom_default',
										'replyTo' => 'noReply@ready4achange.com',
										'label' => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' all-set-trip-support email sent.',
										'content' => [
												'title'		=> $emailSetting['refferal_request']['title'],
												'firstname' => $user->profile->firstname,
												'lastname'	=> $user->profile->lastname,
												'url'		=> $url,
												'text'		=> $emailSetting['refferal_request']['text']
											],
										'type' => 'PATIENT_POST_OPT_INFO',
										'system_category' => 'patient_refferal_request',
										'module_type'	=> 'Client portal'
									]
								];


								$email1 = $this->EmailTrigger->sendEmail('PATIENT_POST_OPT_INFO', $params['satisfaction']);
								$email2 = $this->EmailTrigger->sendEmail('PATIENT_POST_OPT_INFO', $params['support']);
								$email3 = $this->EmailTrigger->sendEmail('PATIENT_POST_OPT_INFO', $params['services_opportunities']);
								$email4 = $this->EmailTrigger->sendEmail('PATIENT_POST_OPT_INFO', $params['refferal_request']);	

								$profile->is_post_opt = '1';
								$profileSave = $profileTable->save($profile);

						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}

						$result = [
							'result'=> 'success',
							'msg'	=> '', 
							'data'	=> '',
							'b_data'=> '',
							'user_id' => $user_id,
							];
						echo json_encode($result);
					break;
				case 'review' :
						$profileTable = TableRegistry::get('Profiles');
						
						// Remove Validation from Profile table
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');
						
						$profileTable->validator()->remove('procedure_id');
						
						$profile = $profileTable->get($this->request->data['Profile']['id']);
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
						
						$usersTable = TableRegistry::get('Users');
						$user = $usersTable->get($this->request->data['User']['id'], ['contain'=>['Profiles', 'UserUploads.Uploads', 'Calls']]);
						
						$doctor = $usersTable->get(isset($user->profile->doctor_id) ? $user->profile->doctor_id : '', ['contain'=>'Profiles']);
						
						$profile->review = 1;
						$profile->state_id=8;
						// Error check before profile save
						if(!$profile->errors()){
							// Save User Profile ADD user_id and upload_id

							if ($savedProfile = $profileTable->save($profile)) {
								// Return result when all successfull

								$result = [
										'result'=> 'success',
										'msg'	=> 'Review Profile Done successfully', 
										'data'	=> ''
										];
								$activity = [
									'user_id'	 => $this->request->data['User']['id'],
									'profile_id' => $this->request->data['Profile']['id'],
									'content'	 => '<b>Client</b> '.$profile->firstname.' '.$profile->lastname.' profile reviewed.',
									'type'		 => 'patient_reviewed',
									'updated_by' => $this->Auth->user('Profile.id')
								];
								TableRegistry::get('Activities')->addActivity($activity);
								
								// Email
								$url = Router::url('/', true);
								$attachment = [];
								$uploads = $user->user_uploads;
								if(!empty($uploads)){
									foreach($uploads as $upload){
										if(!empty($upload['upload']['name'])){
											$attachment[$upload['upload']['name']] = [
												'file' => WWW_ROOT . 'files' . DS . $upload['upload']['name'],
												'mimetype' => $upload['upload']['type'],
												'contentId' => $upload['upload']['id']
											];
										}
									}
								}
								
								$emailSetting = Configure::read('Emails.patient_review');
								$params = [
									'user'=>[
										'to' => $user->email,
										'cc' => '',
										'bcc' => '',
										'subject'=> $emailSetting['user']['subject'],
										'mode' => 'html',
										'template' => 'custom_default',
										'label' => '<b>Client</b> '.$user->profile->firstname.' '.$user->profile->lastname.' profile reviewed.',
										'content' => [
												'title' => $emailSetting['user']['title'],
												'title' => $emailSetting['user']['title'],
												'dr_firstname' => $doctor->profile->firstname,
												'dr_lastname' => $doctor->profile->lastname,
												'firstname' => $user->profile->firstname,
												'lastname' => $user->profile->lastname,
												'username' => $user->username, 
												'attachment' => $attachment,
												'url' => $url,
												'text' => $emailSetting['user']['text'],
											],
										'type' => 'PATIENT_DETAIL_TO_DOCTOR_FOR_REVIEW',
										'system_category' => 'patient_review_by_doctor',
										'module_type'	=> 'Contact Module'
									],
									'admin'=>[
										'to' => $emailSetting['admin']['to'],
										'cc' => $emailSetting['admin']['cc'],
										'bcc' => $emailSetting['admin']['bcc'],
										'subject'=>$emailSetting['admin']['subject'],
										'mode' => 'html',
										'template' => 'custom_default',
										'label' => '<b>Client</b> '.$profile->firstname.' '.$profile->lastname.' profile reviewed.',
										'content' => [
												'title' => $emailSetting['admin']['title'],
												'dr_firstname' => $doctor->profile->firstname,
												'dr_lastname' => $doctor->profile->lastname,
												'firstname' => $user->profile->firstname,
												'lastname' => $user->profile->lastname,
												'username' => $user->username, 
												'attachment' => $attachment,
												'url' => $url,
												'text' => $emailSetting['admin']['text']
											],
										'type' => 'DOCTOR_REVIEW_ABOUT_PATIENT_TO_R4AC',
										'system_category' => 'patient_review_notification',
										'module_type'	=> 'Contact Module'	
									]
								];
								$email = $this->EmailTrigger->sendEmail('PATIENT_DETAIL_TO_DOCTOR_FOR_REVIEW', $params['user']);

								echo json_encode($result);
							}else{
								echo json_encode(['profile'=>$profile->errors()]);
							}
						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}
					break;
				case 'procedure' :
						$profileTable = TableRegistry::get('Profiles');
						
						// Remove Validation from Profile table
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('state_id');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');
						
						
						$profile = $profileTable->get($this->request->data['Profile']['id']);
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
					
						
						// Error check before profile save
						if(!$profile->errors()){
							// Save User Profile ADD user_id and upload_id
							if ($savedProfile = $profileTable->save($profile)) {
								// Return result when all successfull
								
								$profile = $profileTable->get($this->request->data['Profile']['id'], ['contain'=>'procedures']);
								$result = [
										'result'=> 'success',
										'msg'	=> 'Profile updated successfully', 
										'data'	=> $profile->Procedures['name'],
										'user_id'=> $this->request->data['User']['id'],
										'type'	=> 'procedure'
										];
								echo json_encode($result);
							}else{
								echo json_encode(['profile'=>$profile->errors()]);
							}
						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}
					break;
				case 'state' :
						$profileTable = TableRegistry::get('Profiles');
						
						// Remove Validation from Profile table
						$profileTable->validator()->remove('firstname');
						$profileTable->validator()->remove('lastname');
						$profileTable->validator()->remove('state');
						$profileTable->validator()->remove('stage_id');
						$profileTable->validator()->remove('deposit_amount');
						
						$profileTable->validator()->remove('procedure_id');
						
						$profile = $profileTable->get($this->request->data['Profile']['id']);
						$profile = $profileTable->patchEntity($profile, $this->request->data['Profile']);
					
						
						// Error check before profile save
						if(!$profile->errors()){
							// Save User Profile ADD user_id and upload_id
							if ($savedProfile = $profileTable->save($profile)) {
								// Return result when all successfull
								
								$profile = $profileTable->get($this->request->data['Profile']['id'], ['contain'=>'states']);
								$result = [
										'result'	=> 'success',
										'msg'		=> 'State updated successfully', 
										'data'		=> '<span class="' . $this->getState($profile->States['name']) .'">' . $profile->States['name'] . '</span>',
										'sub_data'	=> '<sup><span class="glyphicon glyphicon-edit" aria-hidden="true" style="opacity:0.5"></span></sup>',
										'user_id'	=> $this->request->data['User']['id'],
										'type'		=> 'state'
										];
								echo json_encode($result);
							}else{
								echo json_encode(['profile'=>$profile->errors()]);
							}
						}else{
							echo json_encode(['profile'=>$profile->errors()]);
						}

					break;
				case 'file' :

						$UploadsTable = TableRegistry::get('Uploads');
						$UserUploadsTable = TableRegistry::get('UserUploads');
						$filesToUpload = isset($this->request->data['Profile']['file']) ? $this->request->data['Profile']['file'] : '';
						$msg = '';
						$section = isset($this->request->data['section']) ? $this->request->data['section'] : '';
						if(!empty($filesToUpload)){
							if(!empty($filesToUpload)){
								foreach($filesToUpload as $files){
									$uploads = $UploadsTable->newEntity();
									$uploads = $UploadsTable->patchEntity($uploads, $files);
									$upload_id = NULL;
									if($uploads = $UploadsTable->save($uploads)){
										$upload_id = $uploads->id;

										$msg .='<tr class="remove_file_'.$uploads->id.'">';
											$msg .='<td>';
											$msg .='<a href="'.$uploads->url.'" target="_blank">'.$uploads->name.'</a></td><td>'.$this->size($uploads->size);
											$msg .='</td>';
											$msg .='<td>';
											$msg .='<button data-user-attr="'.$uploads->user_id.'" data-attr="'.$uploads->id.'" data-target="#deleteFileModal" data-toggle="modal" onclick="javascript:void(0)" class="btn btn-danger delete delete_file_modal remove_file_86"><i class="glyphicon glyphicon-trash"></i></button>';
											$msg .='</td>';
										$msg .='</tr>';

										$uuArray = [
											'user_id'=>$this->request->data['User']['id'],
											'profile_id'=>$this->request->data['Profile']['id'],
											'upload_id'=>$uploads->id,
										];

										$userUploads = $UserUploadsTable->newEntity();
										$userUploads = $UserUploadsTable->patchEntity($userUploads, $uuArray);
										
										if($userUpload = $UserUploadsTable->save($userUploads)){
											$fileUpload = true;
										}else{
											$fileUpload = false;
											echo json_encode($userUploads->errors());
										}
									}	
								}
								if($fileUpload){
									
									$result = [
											'result'=> 'success',
											'msg'	=> 'Files Uploaded successfully', 
											'data'	=> '',
											'user_id'=> $this->request->data['User']['id'],
											'type' => 'file',
											'patient_data' => $section
											];
									echo json_encode($result);
								}else{
									echo json_encode(['result'=>'custom', 'msg'=>'There is some internal error while uploading, try again later']);
								}
							}
						}else{
							echo json_encode(['result'=>'custom', 'msg'=>'Select atleast one file first']);
						}
					break;
				case "userpassword":
			
						$usersTable = TableRegistry::get('Users');
						$user = $usersTable->get($this->request->data['User']['id']);
						$usersTable->validator()->remove('email');
						$usersTable->validator()->remove('username');
						$user = $usersTable->patchEntity($user, $this->request->data['User']);
							if ($savedUser = $usersTable->save($user)) {
								$result = ['result'=> 'success',
											'msg'	=> 'Password updated Successfully', 
											'data'	=> '',
											'type' => 'pass'
											];
								echo json_encode($result);
							}else{
								$result = ['result'=> 'Error',
											'msg'	=> 'Unable to update password', 
											'data'	=> ''
											];
								echo json_encode(['user'=>$user->errors()]);
							}
							die();
						break;
				case "doctorUserPassword":
					$usersTable = TableRegistry::get('Users');
					$user = $usersTable->get($this->request->data['User']['id']);
					$usersTable->validator()->remove('email');
					$usersTable->validator()->remove('username');
					$user = $usersTable->patchEntity($user, $this->request->data['User']);
						if ($savedUser = $usersTable->save($user)) {
						$result = ['result'=> 'success',
										'msg'	=> 'Files Uploaded successfully', 
										'data'	=> '',
										'type' => 'file'
										];
						echo json_encode($result);
						}else{
							$result = ['result'=> 'Error',
										'msg'	=> 'Unable to update password', 
										'data'	=> ''
										];
							echo json_encode($user->error());

						}
						die();
					break;
			}
		}
		die;
    }
	
	/*
	* Email function called from payment gateway using CURL
	*/
	public function payment_emails(){
		$this->autoRender = false;
		if($this->request->is('post')){
			$user_id		= $this->request->data['user_id'];
			$uuid			= isset($this->request->data['uuid']) ? $this->request->data['uuid'] : '';
			$status			= $this->request->data['status'];
			$dr_firstname	= $this->request->data['dr_firstname'];
			$dr_lastname	= $this->request->data['dr_lastname'];
			$surgery_date	= $this->request->data['surgery_date'];
			$first_name		= isset($this->request->data['first_name']) ? $this->request->data['first_name'] : '';
			$last_name		= isset($this->request->data['last_name']) ? $this->request->data['last_name'] : '';
			$address		= $this->request->data['address'];
			$city			= $this->request->data['city'];
			$country		= $this->request->data['country'];
			$state			= $this->request->data['state'];
			$zip			= $this->request->data['zip'];
			$email			= $this->request->data['email'];
			$amount			= $this->request->data['amount'];
			$type			= $this->request->data['type'];
			$created		= $this->request->data['created'];
			$created		= explode(' ',$created);
			switch($type){
				case 'payment':	
						$url = Router::url('/', true);
						$emailSetting = Configure::read('Emails.payment');
						$emailTravelinfo = Configure::read('Emails.travel_info');
						$params = [
							'made'=>[
								'to' => $email,
								'cc' => '',
								'bcc' => '',
								'subject'=> $emailSetting['made']['subject'],
								'mode' => 'html',
								'template' => 'custom_default',
								'label' => '<b>Client</b> '.$first_name.' '.$last_name.' made payment.',
								'content' => [
										'title'			=> $emailSetting['made']['title'],
										'dr_firstname'	=> $dr_firstname,
										'dr_lastname'	=> $dr_lastname,
										'firstname'		=> $first_name,
										'lastname'		=> $last_name,
										'surgery_date'	=> $surgery_date,
										'status'		=> ($status ==1) ? 'Paid' : 'Cancelled',
										'address'		=> $address,
										'city'			=> $city,
										'country'		=> $country,
										'state'			=> $state,
										'zip'			=> $zip,
										'email'			=> $email,
										'amount'		=> $amount,
										'date'			=> $created[0],
										'time'			=> $created[1],
										'url'			=> $url,
										'uuid'			=> $uuid,
										'text'			=> $emailSetting['made']['text']
									],
								'type' => 'CONFIRMATION_OF_PAYMENT_MADE_BY_PATIENT',
								'system_category' => 'payment_made',
								'module_type'	=> 'Payment Module'
							],
							'received'=>[
								'to' => $emailSetting['received']['to'],
								'cc' => '',
								'bcc' => '',
								'subject'=> $emailSetting['received']['subject'],
								'mode' => 'html',
								'template' => 'custom_default',
								'label' => '<b>Client</b> '.$first_name.' '.$last_name.' made payment received.',
								'content' => [
										'title'			=> $emailSetting['received']['title'],
										'dr_firstname'	=> $dr_firstname,
										'dr_lastname'	=> $dr_lastname,
										'firstname'		=> $first_name,
										'lastname'		=> $last_name,
										'surgery_date'	=> $surgery_date,
										'status'		=> ($status ==1) ? 'Paid' : 'Cancelled',
										'address'		=> $address,
										'city'			=> $city,
										'country'		=> $country,
										'state'			=> $state,
										'zip'			=> $zip,
										'email'			=> $email,
										'amount'		=> $amount,
										'date'			=> $created[0],
										'time'			=> $created[1],
										'url'			=> $url,
										'uuid'			=> $uuid,
										'text'			=> $emailSetting['received']['text']
									],
								'type' => 'CONFIRMATION_OF_PAYMENT_RECEIVE_BY_R4AC',
								'system_category' => 'payment_received',
								'module_type'	=> 'Payment Module'
							],
							'travel_info'=>[
								'to' => $email,
								'cc' => '',
								'bcc' => '',
								'subject'=> $emailTravelinfo['user']['title'],
								'mode' => 'html',
								'template' => 'custom_default',
								'label' => '<b>Client</b> '.$first_name.' '.$last_name.' travel info request reminder.',
								'content' => [
										'title'			=> $emailTravelinfo['user']['title'],
										'dr_firstname'	=> $dr_firstname,
										'dr_lastname'	=> $dr_lastname,
										'firstname'		=> $first_name,
										'lastname'		=> $last_name,
										'surgery_date'	=> $surgery_date,
										'status'		=> ($status ==1) ? 'Paid' : 'Cancelled',
										'address'		=> $address,
										'city'			=> $city,
										'country'		=> $country,
										'state'			=> $state,
										'zip'			=> $zip,
										'email'			=> $email,
										'amount'		=> $amount,
										'date'			=> $created[0],
										'time'			=> $created[1],
										'url'			=> $url,
										'uuid'			=> $uuid,
										'text'			=> $emailTravelinfo['user']['text']
									],
								'type' => 'REMINDER_TO_PATIENT_ABOUT_TRAVEL_INFO',
								'system_category' => 'travel_info_request',
								'module_type'	=> 'Payment Module'
							]
						];		
						$email_1 = $this->EmailTrigger->sendEmail('CONFIRMATION_OF_PAYMENT_MADE_BY_PATIENT', $params['made']);
						$email_2 = $this->EmailTrigger->sendEmail('CONFIRMATION_OF_PAYMENT_RECEIVE_BY_R4AC', $params['received']);
						$email_3 = $this->EmailTrigger->sendEmail('REMINDER_TO_PATIENT_ABOUT_TRAVEL_INFO', $params['travel_info']);
					break;
				default:
					break;
				
			}
			die;
		}
	}

	public function delete(){
		$this->layout = 'ajax';
		$this->autoRender = false;	
		$usersTable = TableRegistry::get('Users');

		if($this->request->is(['post', 'ajax'])){
			$deletedIds = [];
			$ids = $this->request->data['ids'];
			foreach($ids as $id){
				$entity = $usersTable->get($id);
				$result = $usersTable->delete($entity);
				if($result){
					$deletedIds[] = $id;
				}
			}
			$returnJson = ['msg'=>'Deleted', 'data'=>$deletedIds];
			echo json_encode($returnJson);
			die;
		}
		die;
	}
	/*
	* Assign owner functionalty  click on assign owner icon 
	*/
	public function assign_owner(){
		$this->layout = 'ajax';
		$this->autoRender = false;	
		$profilesTable = TableRegistry::get('Profiles');

		if($this->request->is(['post', 'ajax'])){
			$deletedIds = [];
			$ids = $this->request->data['ids'];
			$owner_id = $this->request->data['sales_rep_id'];
			
			foreach($ids as $id){
				$entity = $profilesTable->findByUserId($id)->first();
				$entity->sales_rep_id = $owner_id;
				$msg = 'Unable to add Owner, please try again later';
				$result = $profilesTable->save($entity);
				if($result){
					$deletedIds[] = $id;
					$msg = 'Owner added to selected contact(s)';
				}
				// Save Activity log short content in activity table
				$activity = [
					'user_id'	 => $id,
					'profile_id' => $entity->id,
					'content'	 => 'Sales rep. assigned to '.$entity->firstname,
					'type'		 => 'assign_salesrep',
					'updated_by' => $this->Auth->user('Profile.id')
				];
				TableRegistry::get('Activities')->addActivity($activity);

			}
			$returnJson = ['msg'=>$msg, 'data'=>$deletedIds];
			echo json_encode($returnJson);
			die;
		}
		die;
	}

	/*
	* Call qeues functionalty  click on call qeues icon 
	*/
	public function assign_call_queue(){
		$this->layout = 'ajax';
		$this->autoRender = false;	
		$callsTable = TableRegistry::get('Calls');
		$usersTable = TableRegistry::get('Users');
		$profileTable = TableRegistry::get('Profiles');

		if($this->request->is(['post', 'ajax'])){
			$deletedIds = [];
			$ids = $this->request->data['ids'];
			foreach($ids as $id){

				$user = $usersTable->get($id, ['contain'=>'Profiles']);
				
				if(!empty($user)){
					$msg = 'Unable to add call queue';
					//$entity->queue = 1;
					//$result = $callsTable->save($entity);
					
					$updateProfile = $profileTable->findByUserId($id)->first();
					$updateProfile->call_queue = 1;
					$resultProfile = $profileTable->save($updateProfile);

					if($resultProfile){

						// Save Activity log short content in activity table
						$activity = [
							'user_id'	 => $id,
							'profile_id' => $user->profile->id,
							'content'	 => $user->profile->firstname.' '.$user->profile->lastname.' add to call queue.',
							'type'		 => 'call_queue',
							'updated_by' => $this->Auth->user('Profile.id')
						];
						TableRegistry::get('Activities')->addActivity($activity);

						$deletedIds[] = $id;
						$msg = 'Contact(s) add to call queue';
					}
				}
			}
			$returnJson = ['msg'=>$msg, 'data'=>$deletedIds];
			echo json_encode($returnJson);
			die;
		}
		die;
	}

	public function update_user_status(){
		$this->layout = 'ajax';
		$this->autoRender = false;	
		$usersTable = TableRegistry::get('Users');

		if($this->request->is(['post', 'ajax'])){
			$deletedIds = [];
			$ids = $this->request->data['ids'];
			$status = $this->request->data['status'];
			foreach($ids as $id){
				$entity = $usersTable->get($id, ['contain'=>'Profiles']);
				$entity->status = $status;
				$entity->role = 'lead';
				$result = $usersTable->save($entity);
				if($result){
					// Save Activity log short content in activity table
					$activity = [
						'user_id'	 => $id,
						'profile_id' => $entity->profile->id,
						'content'	 => $entity->profile->firstname.' '.$entity->profile->lastname.' state updated to '.$status.'.',
						'type'		 => 'state_updated',
						'updated_by' => $this->Auth->user('Profile.id')
					];
					TableRegistry::get('Activities')->addActivity($activity);

					$deletedIds[] = $id;
				}
			}
			$returnJson = ['msg'=>'Status updated', 'data'=>$deletedIds];
			echo json_encode($returnJson);
			die;
		}
		die;
	}

	public function uploadhandler(){
		$this->autoRender = false;
		$this->layout = 'ajax';
		if($this->request->is(['post', 'ajax'])){
			require_once(ROOT .DS. 'vendor' . DS . 'UploadHandler' . DS . 'UploadHandler.php');
			$upload_handler = new UploadHandler();
			die;
		}
	}
	
	

	public function deleteFile(){
		$this->autoRender = false;
		$this->layout = 'ajax';
		$this->UserUploads = TableRegistry::get('UserUploads');
		$this->Uploads = TableRegistry::get('Uploads');
		
		if($this->request->is(['post', 'ajax'])){
			$id = $this->request->data['id'];
			$userFile = $this->UserUploads->get($id);
			$result = $this->UserUploads->delete($userFile);
			if($result){
				$uploadFile = $this->Uploads->get($userFile->upload_id);
				$result2 = $this->Uploads->delete($uploadFile);
				if($result2){
					$m_img = $uploadFile->name;

					$m_img_path = WWW_ROOT.'/files/'.$uploadFile->name;

					if (file_exists($m_img_path))
					{
						 unlink($m_img_path);
						 echo 'ok';
					}
				}
			}
		}
		die;
	}

	public function getState($state){
		switch($state){
			case 'Scared' :
			case '0' :
					return 'label label-warning';
				break;
			case 'Good' :
			case '1' :
					return 'label label-success';
				break;
			case 'Hot' :	
					return 'label label-danger';
				break;
			case 'New' :	
					return 'label label-default';
				break;
			default : 
					return 'label label-primary';
				break;
		}
	}

	public function size($size){
		// Calculate Size
		$fixedSize = $size / 1024;
		$SizeHtml = round($fixedSize, 2).' KB';
		if($fixedSize > 1024){
			$fixedSize = $fixedSize / 1024;
			$SizeHtml = round($fixedSize, 2) .' MB';
		}
		return $SizeHtml;
	}

	public function cronJobs(){
		$this->autoRender = false;
		$tasksTable = TableRegistry::get('Tasks');
		$allTasks = $tasksTable->find('all', [
				'conditions'=>[
					'Tasks.status'=> 'new'
				],
				'order'=>'Tasks.id DESC'
			])->contain(['Users', 'Profiles.SalesReps', 'RelatedContact']);


		echo '<table border="1" cellpadding="20" cellspacing="0">';
				echo '<tr>';
					echo '<th>ID</th>';
					echo '<th>Task Description</th>';
					echo '<th>Date Time</th>';
					echo '<th>Before</th>';
					echo '<th>Trigger Date</th>';
					echo '<th>Now</th>';
					echo '<th>Status</th>';
					echo '<th>Mail Status</th>';
					
				echo '</tr>';

		foreach($allTasks as $task){

			$changeDate = $this->formatMillisecondsToSecond($task->reminder_duration);

			$date = date('Y-m-d', strtotime($task->due_date));
			$time = date('H:i:s', strtotime($task->due_time));
			
			$dateTime =  strtotime($date. ' ' .$time);
			
			$diffSecond =  strtotime($date.' '.$time.' - '.$changeDate['seconds']. 'seconds');

			$triggerDate = date("Y-m-d H:i:s", $diffSecond);
			
			$now = date("Y-m-d H:i:s"); 
			
			$nowSecond = strtotime($now);
			
			$leftTime = $this->formatMillisecondsToSecond(($diffSecond - $nowSecond) * 1000);

			$ss = $leftTime['day']. ' days';
			$triggerEmail = $leftTime['day'];
			if($leftTime['day'] <= 0){
				$ss = $leftTime['hours']. ' hours';
				$triggerEmail = $leftTime['hours'];
			}
				
			if($leftTime['hours'] <= 0){
				$ss = $leftTime['minutes']. ' minutes';
				$triggerEmail = $leftTime['minutes'];
			}

			if($leftTime['minutes'] <= 0){
				$ss = $leftTime['seconds']. ' seconds';
				$triggerEmail = $leftTime['seconds'];
			}

			$status = $ss;
			$mailStatus = ($task->status == "new") ? 'Pending' : 'Sent';
			if($nowSecond > $diffSecond){
				$status = 'Completed';
			}
			

			$dd = $changeDate['day']. ' days';
			if($changeDate['day'] <= 0){
				$dd = $changeDate['hours']. ' hours';
			}
				
			if($changeDate['hours'] <= 0){
				$dd = $changeDate['minutes']. ' minutes';
			}

			if($changeDate['minutes'] <= 0){
				$dd = $changeDate['seconds']. ' seconds';
			}

			$status = $ss;
			$mailStatus = ($task->status == "new") ? 'Pending' : 'Sent';
			if($nowSecond > $diffSecond){
				$status = 'Completed';
			}

			echo '<tr>';
				echo '<td>'.$task->id.'</td>';
				echo '<td>'.$task->description.'</td>';
				echo '<td>'.$date.' '.$time.'</td>';
				echo '<td>'.$dd.'</td>';
				echo '<td>'.$triggerDate.'</td>';
				echo '<td>'.$now.'</td>';
				echo '<td>'.$status.'</td>';
				echo '<td>'.$mailStatus.'</td>';
			echo '</tr>';
			
			
			if($mailStatus == "Pending"){
				if($triggerEmail <= 0 && $task->status == "new"){

					$dataTask = $tasksTable->get($task->id);
					
					$dataTask->status = 'completed';

					if($saveTask = $tasksTable->save($dataTask)){
						
						if($task->section == "patient"){
							$firstname = isset($task->profile->firstname) ? $task->profile->firstname : '';
							$lastname = isset($task->profile->lastname) ? $task->profile->lastname : '';
							$role = isset($task->user->role) ? $task->user->role : '';
							$email = isset($task->remindercontact->email) ? $task->remindercontact->email : '';
							$description = isset($task->description) ? $task->description : '';
						}

						if($task->section == "task"){
							$firstname = isset($task->relatedcontact->firstname) ? $task->relatedcontact->firstname : '';
							$lastname = isset($task->relatedcontact->lastname) ? $task->relatedcontact->lastname : '';
							$role = isset($task->user->role) ? $task->user->role : '';
							$email = isset($task->user->email) ? $task->user->email : '';
							$description = isset($task->description) ? $task->description : '';
						}
						
						if($task->section == "flight"){
							//pr($task);
							$firstname = isset($task->profile->firstname) ? $task->profile->firstname : '';
							$lastname = isset($task->profile->lastname) ? $task->profile->lastname : '';
							$role = isset($task->user->role) ? $task->user->role : '';
							$email = isset($task->profile->sales_rep->email) ? $task->profile->sales_rep->email : $task->user->email;
							$description = isset($task->description) ? $task->description : '';
						}
					
						if(!empty($email)){
							$params = [
								'user'=>[
									'to' => $email,
									'cc' => '',
									'bcc' => '',
									'subject'=>	'Reminder',
									'label' => 'Task email sent using cron',
									'mode' => 'html',
									'template' => 'custom_default',
									'content' => [
											'firstname' => $firstname,
											'lastname' => $lastname,
											'username' => '', 
											'password' => '',
											'url' => '',
											'text' => $description
										],
									'type' => 'PATIENT_TASK_REMINDER',
									'system_category' => 'reminder_cronjobs',
									'module_type'	=> 'Task Module'
								],
								'admin'=>[
									'to' => '',
									'cc' => '',
									'bcc' => '',
									'subject'=>'',
									'label' => '',
									'content' => [
											'firstname'=>'',
											'lastname'=>'',
											'username'=>'', 
											'password'=>'',
											'url'=>'',
											'text' => ''
										],
									'type' => '',
									'system_category' => '',
									'module_type'	=> ''	
								]
							];
							
							$emailSent = $this->EmailTrigger->sendEmail('PATIENT_TASK_REMINDER', $params['user']);
							/*
							if(!$emailSent){
								$dataTask->status = 'new';
								$tasksTable->save($dataTask);
							}
							*/
							$activity = [
								'user_id'	 => $task->id,
								'profile_id' => $task->profile_id,
								'content'	 => 'Reminder sent to '.ucfirst($role).' '.$firstname.' '.$lastname,
								'type'		 => 'reminder',
								'updated_by' => ''
							];
							TableRegistry::get('Activities')->addActivity($activity);
							
						}
					}
				}
			}
			
		}
		echo '</table>';
	}

	public function formatMillisecondsToSecond($milliseconds) {
		$seconds = floor($milliseconds / 1000);
		$minutes = floor($seconds / 60);
		$hours = floor($minutes / 60);
		$day = floor($hours / 24);
		$returnArray = [];

		$returnArray['seconds'] = $seconds;
		$returnArray['minutes'] = $minutes;
		$returnArray['hours'] = $hours;
		$returnArray['day'] = $day;

		return $returnArray;
	}
	public function checkboxState() {
		$this->layout = 'ajax';
		$this->autoRender = false;
			$PatientAllergicInfosTable = TableRegistry::get('PatientAllergicInfos');
			$ProfilesTable = TableRegistry::get('Profiles');
			$PatientReviewSystemsTable = TableRegistry::get('PatientReviewSystems');
			$PatientDietHistoriesTable = TableRegistry::get('PatientDietHistories');
			$PatientHealthsTable = TableRegistry::get('PatientHealths');
		if($this->request->is(['post', 'ajax'])){
			   $state=$this->request->data['state'];
				$id=$this->request->data['id'];
		     	$tabelType=$this->request->data['table'];
		switch($tabelType){
			case 'pofileState' :
				$ProfilesData = $ProfilesTable->get($id);
				$ProfilesData->doctor_comment_state = $state;
				$ProfilesTable->save($ProfilesData);
				break;
			
			case 'patientReviewState' :
			     $PatientReviewData = $PatientReviewSystemsTable->get($id);
			 	 $PatientReviewData->doctor_comment_state = $state;
				 $PatientReviewSystemsTable->save($PatientReviewData);
				 break;
			
			case 'patientAllegricState' :
				 $PatientAllergicInfos = $PatientAllergicInfosTable->get($id);
			 	 $PatientAllergicInfos->doctor_comment_state = $state;
				 $PatientAllergicInfosTable->save($PatientAllergicInfos);
				 break;
			
			case 'patientHealtState' :
				 $PatientHealthsData = $PatientHealthsTable->get($id);
				 $PatientHealthsData->doctor_comment_state = $state;
				 $PatientHealthsTable->save($PatientHealthsData);
				
				 break;
			
			case 'patientDietState' :
				 $PatientDietHistoriesData = $PatientDietHistoriesTable->get($id);
				 $PatientDietHistoriesData->doctor_comment_state = $state;
				 $PatientDietHistoriesTable->save($PatientDietHistoriesData);
				 break;
			
			default : 
				
				break;
			}

		}
	}

	/*
	* Function to get Doctor
	* return Doctor name array
	*/
	
	public function getDoctor($doctor_id = null){
		$usersTable = TableRegistry::get('Users');
		$profilesTable = TableRegistry::get('Profiles');

		$doctorsTable['Profiles'] =[];

		if(!empty($doctor_id)){

			$doctorsTable = $usersTable->find('all', [
				'conditions'=> [
							'Users.id'=>$doctor_id
				],
				'fields'=>['Profiles.firstname', 'Profiles.lastname']
				])->contain(['Profiles']);

			if(!empty($doctorsTable)){
				$doctorsTable = $doctorsTable->first();
			}
		}
		return $doctorsTable['Profiles'];
			
	}

	/*
	* Function to get Driver
	* return Driver name array
	*/
	
	public function getDriver($driver_id = null){
		$usersTable = TableRegistry::get('Users');
		$profilesTable = TableRegistry::get('Profiles');

		$driverTable['Profiles'] =[];

		if(!empty($driver_id)){

			$driverTable = $usersTable->find('all', [
				'conditions'=> [
							'Users.id'=>$driver_id
				],
				'fields'=>['Profiles.firstname', 'Profiles.lastname']
				])->contain(['Profiles']);

			if(!empty($driverTable)){
				$driverTable = $driverTable->first();
			}
		}
		return $driverTable['Profiles'];
			
	}
	
	/*
	* Function to get Sales Reps
	* return Sales name array
	*/
	public function getSalesRep($sales_rep_id = null){
		$usersTable = TableRegistry::get('Users');
		$profilesTable = TableRegistry::get('Profiles');

		$salesTable['Profiles'] = [];
		if(!empty($sales_rep_id)){
				
			$salesTable = $usersTable->find('all', [
				'conditions'=> [
							'Users.id'=>$sales_rep_id
				],
				'fields'=>['Profiles.firstname', 'Profiles.lastname']
				])->contain(['Profiles']);

			if(!empty($salesTable)){
				$salesTable = $salesTable->first();
			}

			return $salesTable['Profiles'];
		}
	}
		
	// Doctors List 
	public function getDoctorList(){
		$usersTable = TableRegistry::get('Users');
		$doctorsList = [];
		$usersDoctor = $usersTable->find('all', [
							'fields' => ['Users.id', 'Profiles.firstname', 'Profiles.lastname'],
							'conditions'=>[
								'Users.role'=>'doctor', 
								'status'=>'Active']
							])->contain(['Profiles']);
		
		foreach($usersDoctor as $user){
			$doctorsList[$user->id] = $user->profile->firstname.' '.$user->profile->lastname;	
		}
		return $doctorsList;
	}

	// Sales Rep List
	public function getSalesRepList(){
		$usersTable = TableRegistry::get('Users');
		$salesrepList = [];
		$salesReps = $usersTable->find('all', [
							'fields' => ['Users.id', 'Profiles.firstname', 'Profiles.lastname'],
							'conditions'=>[
								'Users.role'=>'salesrep', 
								'status'=>'Active']
							])->contain(['Profiles']);
		
		foreach($salesReps as $user){
			$salesrepList[$user->id] = $user->profile->firstname.' '.$user->profile->lastname;	
		}

		return $salesrepList;
	}
	

	// Driver List 
	public function getDriverList(){
		$usersTable = TableRegistry::get('Users');
		$driverList = [];
		$usersDriver = $usersTable->find('all', [
							'fields' => ['Users.id', 'Profiles.firstname', 'Profiles.lastname'],
							'conditions'=>[
								'Users.role'=>'driver', 
								'status'=>'Active']
							])->contain(['Profiles']);
		
		foreach($usersDriver as $user){
			$driverList[$user->id] = $user->profile->firstname.' '.$user->profile->lastname;	
		}
		return $driverList;
	}

	public function getSalesRepNameArray($allUsers){
		// Get Owner and Doctor and add with user array
		foreach($allUsers as $key=>$users){
			if($key == 0){
				$this->user = $users;
			}
			$sales_rep_id = isset($users->profile->sales_rep_id) ? $users->profile->sales_rep_id : '';
			if(!empty($sales_rep_id)){
				$salesArray = $this->getSalesRep($sales_rep_id);
				if(!empty($salesArray)){
					$users->profile->sales_rep->name = $salesArray;
				}
			}
			
			$doctor_id = isset($users->profile->doctor_id) ? $users->profile->doctor_id : '';
			if(!empty($doctor_id)){
				$doctorArray = $this->getDoctor($doctor_id);
				if(!empty($doctorArray)){
					$users->profile->doctor->name = $doctorArray;
				}
			}

			$driver_id = isset($user->profile->driver_id) ? $user->profile->driver_id : '';
			if(!empty($driver_id)){
				$driverArray = $this->getDriver($driver_id);
						if(!empty($driverArray)){
							$user->profile->driver->name = $driverArray;
						}
			}
		}
	}
}
