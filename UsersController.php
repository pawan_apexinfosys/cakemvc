<?php 
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Network\Email\Email;
use Cake\Utility\Text;
use Cake\Routing\Router;
use Cake\View\Helper\SessionHelper;
use Cake\Network\Session\DatabaseSession;

class UsersController extends AppController
{
	public function beforeFilter(Event $event){
        parent::beforeFilter($event);
        $this->Auth->allow(['login', 'resetPassword', 'getUserListAjax']);
    }

	function cleanFileName($str){
		$ext = pathinfo($str, PATHINFO_EXTENSION);
		$str = pathinfo($str, PATHINFO_FILENAME);
		$str = preg_replace('/\s+/', '',$str);
		$str = str_replace('.',' ',$str);
		$str = preg_replace('/[^A-Za-z0-9\-]/', '', $str);

		return $str.'.'.$ext;    
	}
	
	function mkdir_recursive($pathname, $mode='777'){
		is_dir(dirname($pathname)) || $this->mkdir_recursive(dirname($pathname), $mode);
		return is_dir($pathname) || @mkdir($pathname);
	}

	public function profile($id=null) {
		 $this->layout = 'dashboard';
		 $usersTable = TableRegistry::get('Users');
		 $profilesTable = TableRegistry::get('Profiles');
		 $editData = $usersTable->get($id,['contain'=>'Profiles']);
		 $this->set(compact('editData'));
		 if($this->request->is(['post', 'put'])){
			$editData->email =  $this->request->data['User']['email'];
			$editData->title = $this->request->data['User']['title'];
			$usersTable->save($editData);
			
			$editP = $profilesTable->get($this->request->data['Profile']['id']);
			$editP->firstname = $this->request->data['Profile']['firstname'];
			$editP->lastname = $this->request->data['Profile']['lastname'];
			$editP->phone = $this->request->data['Profile']['phone'];
			$editP->address = $this->request->data['Profile']['address'];
			$editP->city = $this->request->data['Profile']['city'];
			$editP->state = $this->request->data['Profile']['state'];
			$editP->country = $this->request->data['Profile']['country'];
			$editP->description = $this->request->data['Profile']['description'];
			
			if(isset($this->request->data['Profile']['photo'])){
				if($this->request->data['Profile']['photo']['size'] != 0){
					$path =  WWW_ROOT.'img/profile/'.md5($this->Auth->user('id'));		
					$this->mkdir_recursive($path);
					$ImageUpload = $this->loadComponent('ImageUpload');
						$file_name = $this->cleanFileName($this->request->data['Profile']['photo']['name']);
						if($file =  $ImageUpload->copy(array(
							'file' => $this->request->data['Profile']['photo'], 
							'file_name' => $file_name,
							'path' => $path.'/'))){

							$ImageUpload->resize(array(
										'file' => $path.'/'.$file_name, 
									   'file_name' => $file_name,
										'prefix' => '36x36',
										'output' => $path.'/', 
										'width' => '36', 
										'height' => '36'
									));
							$ImageUpload->resize(array(
										'file' => $path.'/'.$file_name, 
									   'file_name' => $file_name,
										'prefix' => '41x41',
										'output' => $path.'/', 
										'width' => '41', 
										'height' => '41'
									));
							$ImageUpload->resize(array(
										'file' => $path.'/'.$file_name, 
									   'file_name' => $file_name,
										'prefix' => '125x161',
										'output' => $path.'/', 
										'width' => '125', 
										'height' => '161'
									));
					
					}
					$editP->photo = $file_name;
				}
			}
			$result=$profilesTable->save($editP);
			if($result){
				$session = $this->request->session();
				$auth = $session->read('Auth');
				$editData = $usersTable->get($id,['contain'=>'Profiles']);
				$session->write('Auth.User.email', $editData->email);
				$session->write('Auth.User.title', $editData->title);
				$session->write('Auth.User.Profile', $editData->profile);
				$this->Flash->success('Profile updated');
				return $this->redirect(['action' => 'profile', $id]);
			}
		 } 
    }

	
	public function index(){
		$this->layout = 'dashboard';
        $this->set('users', $this->Users->find('all'));
    }

    public function view($id){
        if (!$id) {
            throw new NotFoundException(__('Invalid user'));
        }

        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

	public function login() {
		$this->layout = 'login';

		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
			
			
			if ($user) {
				$this->Auth->setUser($user);

				if($user['status'] !== "Active"){
					$this->Flash->error(
						__('Your account is not acitve, please contact website Administrator'),
						'default',
						[],
						'auth'
					);
					return $this->redirect($this->Auth->logout());
				}

				/*Updating user table last login*/
				$usersTable = TableRegistry::get('Users');
				$users = $usersTable->get($user['id'], ['contain'=>'Profiles']);

				$users->id = $user['id'];
				$users->login_ip = $this->request->clientIp();
				$users->last_login = Time::now();

				$usersTable->save($users);

				$session = $this->request->session();
				$session->write('Auth.User.Profile', $users->profile);
				$role = $users->role;
				
				// Save Activity log short content in activity table
				$activity = [
					'user_id'	 => $user['id'],
					'profile_id' => $users->profile->id,
					'content'	 => $users->profile->firstname.' '.$users->profile->lastname.' logged in',
					'type'		 => 'login',
					'updated_by' => $this->Auth->user('Profile.id')
				];
				TableRegistry::get('Activities')->addActivity($activity);
					
				switch($role){
					case 'superadmin' :
					case 'admin' : 
					case 'salesrep' :	
					case 'superadmin' :
					case 'staff' :
							return $this->redirect($this->Auth->redirectUrl());
						break;
					case 'doctor' : 
							return $this->redirect(['controller'=>'contacts', 'action'=>'index', 'prefix' => 'doctor']);
						break;
					case 'patient' :
							return $this->redirect(['controller'=>'contacts', 'action'=>'index', 'prefix' => 'patient']);
						break;
					case 'driver' :
							return $this->redirect(['controller'=>'contacts', 'action'=>'index', 'prefix' => 'driver']);
						break;
				}
				
			} else {
				$this->Flash->error(
					__('Username or password is incorrect'),
					'default',
					[],
					'auth'
				);
			}
		}
	}

	public function add() {
		$this->layout = 'login';
		$user = $this->Users->newEntity();

		if ($this->request->is('post')) {
			$user = $this->Users->patchEntity($user, $this->request->data);
			if (!$user->errors()) {
				$user->parent_id = $this->Auth->user('id');
				if ($this->Users->save($user)) {
					$user = $this->Users->get($user, ['contain'=>'Profiles']);
					if(!empty($user)){
						// Save Activity log short content in activity table
						$activity = [
							'user_id'	 => $user->id,
							'profile_id' => $user->profile->id,
							'content'	 => $user->profile->firstname.' '.$user->profile->lastname.' Add',
							'type'		 => $user->role.'_added',
							'updated_by' => $this->Auth->user('Profile.id')
						];
						TableRegistry::get('Activities')->addActivity($activity);
					}
					$this->Flash->success(__('The user has been saved.'));
					
				}
			}else{
				$this->Flash->error(__('Unable to add the user.'));
			}
		}
		
		$this->set('user', $user);
	}

	public function logout(){
		// Save Activity log short content in activity table
		$activity = [
			'user_id'	 => $this->Auth->user('User.id'),
			'profile_id' => $this->Auth->user('Profile.id'),
			'content'	 => $this->Auth->user('Profile.firstname').' '.$this->Auth->user('Profile.lastname').' logged out',
			'type'		 => 'logout',
			'updated_by' => $this->Auth->user('Profile.id')
		];
		TableRegistry::get('Activities')->addActivity($activity);

		$this->Flash->success('You are now logged out.');
		return $this->redirect($this->Auth->logout());
	}
	
	public function forgotPassword(){
		$this->layout = 'login';

		$time = Time::now();
		
		$user = $this->Users->newEntity();

		$this->loadModel('PasswordRecoveries');
		$passwordRecovery = $this->PasswordRecoveries->newEntity();

		if ($this->request->is('post')) {

				$email = $this->request->data['email'];
				if($email){

				$users = $this->Users->find('all', ['conditions'=>['Users.email'=>$email]])->contain('Profiles');
				$user = $users->first();
				
				if(!empty($user)){
					unset($this->request->data['email']);
					
					$prevRecovery = $this->PasswordRecoveries->find('all', ['conditions'=>['PasswordRecoveries.user_id'=>$user->id]]);

					if(count($prevRecovery->all()) > 0){
						$this->PasswordRecoveries->deleteAll(['PasswordRecoveries.user_id'=>$user->id]);
					}

					$this->request->data['id'] = null;
					$this->request->data['user_id'] = $user->id;
					$this->request->data['token_key'] = Text::uuid();
					$this->request->data['expire'] = strtotime($time->modify('+5 days'));

					$passwordRecovery = $this->PasswordRecoveries->patchEntity($passwordRecovery, $this->request->data);
					if ($this->PasswordRecoveries->save($passwordRecovery)) {
						/*Send reset password email with URL*/
						$url = Router::url(['controller' => 'Users', 'action' => 'resetPassword', $this->request->data['token_key']], true);
						
						$emailSettings = Configure::read('Emails.forgot_password');
						// Email
						$params = [
							'to' => $email,
							'cc' => '',
							'bcc' => '',
							'mode' => 'html',
							'template' => 'custom_default',
							'from'=> $emailSettings['user']['from'],
							'subject' => $emailSettings['user']['subject'],
							'label' => '<b>'.ucfirst($user->role).'</b> '.$user->profile->firstname.' '.$user->profile->lastname.' reset password request.',
							'content' => [
									'title' => $emailSettings['user']['title'],
									'username' => $user->username,
									'firstname' => $user->profile->firstname,
									'lastname' => $user->profile->lastname,
									'url' => $url,
									'text' => $emailSettings['user']['text'],
								],
							'type' => 'FORGOT_PASSWORD',
							'system_category' => 'forgot_password',
							'module_type'	=> 'Users Module'
						];

						if($this->EmailTrigger->sendEmail('FORGOT_PASSWORD', $params)){
							$this->Flash->success(__('Password reset instruction is sent to '. $email. ', Check you email'));
							return $this->redirect(['action' => 'login']);
						}else{
							$this->Flash->success(__('Unable to send password reset instruction is sent to '. $email. ', please try again later'));
							return $this->redirect(['action' => 'resetPassword']);
						}
					}
					
				}else{
					$this->Flash->error(__($email. ' doesn\'t exist. Please check your email address.'));
					return $this->redirect(['action' => 'forgotPassword']);
				}
			}else{
					$this->Flash->error(__('Please enter valid email address.'));
					return $this->redirect(['action' => 'forgotPassword']);
			}
		}
    }

	public function resetPassword($token_key = null){
		$this->layout = 'login';
		$this->loadModel('PasswordRecoveries');
		$passwordRecovery = $this->PasswordRecoveries->newEntity();
		$user = $this->Users->newEntity();

		$prevData = $this->PasswordRecoveries->find('all', [
								'conditions'=>[
									'token_key'=>$token_key
								]]);
		$data = $prevData->first();
		/* If no data found*/
		if(empty($data)){
			$this->Flash->error(__('This URL is not valid.'));
			return $this->redirect(['action' => 'login']);
		}
		
		$data = $prevData->first();
		$tokenExpiredTime = $data->expire;
		$tokenTodaysTime = strtotime(Time::now());
		
		/*If token is expired*/
		if($tokenTodaysTime > $tokenExpiredTime){
			$this->Flash->error(__('The password reset token is expired'));
			return $this->redirect(['action' => 'login']);
		}

		if($this->request->is('post')){
			
			unset($this->request->data['password2']);

			$usersTable = TableRegistry::get('Users');
			$users = $usersTable->get($data->user_id, ['contain'=>'Profiles']); // article with id 12
			
			if(!empty($users)){
				$users->password = $this->request->data['password'];
				if ($usersTable->save($users)) {
					
					/*Delete Token from database*/
					$entity = $this->PasswordRecoveries->get($data->id);
					$result = $this->PasswordRecoveries->delete($entity);
					// Email
					$url = Router::url(['controller' => 'Users', 'action' => 'login', 'home'], true);
					$emailSettings = Configure::read('Emails.reset_password_done');
					$params = [
						'to' => $users->email,
						'cc' => '',
						'bcc' => '',
						'from'=> $emailSettings['user']['from'],
						'subject' => $emailSettings['user']['subject'],
						'mode' => 'html',
						'template' => 'custom_default',
						'label' => '<b>'.ucfirst($users->role).'</b> '.$users->profile->firstname.' '.$users->profile->lastname.' reset password request.',
						'content' => [
								'title' => $emailSettings['user']['title'],
								'username' => $users->username,
								'firstname' => $users->profile->firstname,
								'lastname' => $users->profile->lastname,
								'url' => $url,
								'text' => $emailSettings['user']['text'],
							],
						'type' => 'RESET_PASSWORD_DONE',
						'system_category' => 'reset_password',
						'module_type'	=> 'Users Module'
					];

					$this->EmailTrigger->sendEmail('RESET_PASSWORD_DONE', $params);
					
					$this->Flash->success(__('Your new password has been updated, you can login now.'));
					return $this->redirect(['action' => 'login']);
				}else{
					$this->Flash->error(__('Unable to update your new password, Please try again later.'));
					return $this->redirect(['action' => 'login']);
				}
			}else{
				$this->Flash->success(__('No user found.'));
				return $this->redirect(['action' => 'login']);
			}
		}
    }
	
	public function dashboard(){
		$this->layout = 'login';
	
    }
	
	public function tasks(){
		$this->layout = 'dashboard';
    }
	
	public function messages(){
		$this->layout = 'dashboard';
	}

	public function isAuthorized($user){
		$action = $this->request->params['action'];
		// The index actions are always allowed.
		if (in_array($action, ['index', 'add'])) {
			return true;
		}
		return true;
		// All other actions require an id.
		if (empty($this->request->params['pass'][0])) {
			return false;
		}
		// Check that the users belongs to the current user.
		$id = $this->request->params['pass'][0];

		$users = $this->Users->get($id);
		if ($users->user_id == $user['id']) {
			return true;
		}
		return parent::isAuthorized($user);
	}
		

	public function getUserListAjax($query = null){
		$this->autoRender = false;
		$this->layout = 'ajax';
		
		if($users = $this->Users->find('all', [
			'conditions'=>['OR' => ['Users.email LIKE' => "%$query%", 'Profiles.firstname LIKE' => "%$query%", 'Profiles.lastname LIKE' => "%$query%"]],
			'fields' => ['Profiles.id', 'Profiles.firstname', 'Profiles.lastname', 'Users.email']
			])
		->contain([
			'Profiles',
		]))  {
			$json = array();
			foreach($users as $user){
				$json[]['username'] = $user->profile->firstname . ' '.$user->profile->lastname. ' ('.$user->email.')';
			}
			echo json_encode($json);
			exit();
		}

	}

	public function searchCommonFilter(){
		$this->layout = 'ajax';
		$this->autoRender = false;
		$usersTable = TableRegistry::get('Users');
		if($this->request->is(['post', 'ajax'])){
		$userAuth = $this->request->session()->read('Auth');
			$userrole = $userAuth['User']['role'];
			switch($userrole){
				case "admin" :
						$userSearchRole = '';
				break;
				case "superadmin":
						$userSearchRole = ['Users.role' => 'superadmin'];
				break;
				default:
					$userSearchRole = '';
				break;
		}
		$searchKey = isset($this->request->data['search_filter_key']) ? $this->request->data['search_filter_key'] :'';
		
		$allUsers = $usersTable->find('all', [
							'conditions'=>[
									'Users.username LIKE' => '%'.$searchKey.'%',
									'Users.email LIKE' => '%'.$searchKey.'%',
								'OR'=>[
									['Users.role'=>'admin'],
									['Users.role'=>'salesrep'],
									['Users.role'=>'staff'],
									['Users.role'=>'patient'],
									['Users.role'=>'lead'],
									['Users.role'=>'doctor']
								],
						]])->hydrate(false)
						   ->contain(['Calls',
								'Notes',
								'Schedules',
								'Profiles.Procedures',
								'Profiles.SalesReps',
								'Profiles.Doctors',
								'Profiles.States', 
								'Profiles.Stages', 
								'Profiles.LeadSources', 
								'UserSurgeries.Surgeries', 
								'UserUploads.Uploads'
						]);
		$this->set(Compact('allUsers'));
		echo $this->render('/Element/user/notification');
			die;
		}
	}
	
	public function edit_doctor(){
		$this->layout = 'ajax';
		$this->autoRender = false;
		
		$surgeriesTable = TableRegistry::get('Surgeries');
		$surgeriesList = $surgeriesTable->find('list');
		$surgeriesList = $surgeriesList->toArray();

		if($this->request->is(['post', 'ajax'])){
			$id = isset($this->request->data['id']) ? $this->request->data['id'] : '';
			if(!empty($id)){
				$user = $this->Users->get($id, ['contain'=>['Profiles', 'UserSurgeries.Surgeries', 'UserUploads.Uploads']]);
				$this->set(Compact(['user', 'surgeriesList']));
				echo $this->render('/Element/user/edit_doctor');
			}else{
				$result = [
						'result'=> 'Error',
						'msg'	=> 'No record found', 
						'data'	=> ''
						];
				echo json_encode($result);	
			}
		die;
		}
	}

	public function edit_user(){
		$this->layout = 'ajax';
		$this->autoRender = false;
			$usersTable = TableRegistry::get('Users');
			$profileTable = TableRegistry::get('Profiles');
	
		$surgeriesTable = TableRegistry::get('Surgeries');
		$surgeriesList = $surgeriesTable->find('list');
		$surgeriesList = $surgeriesList->toArray();

		if($this->request->is(['post', 'ajax'])){
			$id = isset($this->request->data['id']) ? $this->request->data['id'] : '';
			if(!empty($id)){

				$procedureTable = TableRegistry::get('Procedures');
				$procedures = $procedureTable->find('list');
				$proceduresList = $procedures->toArray();

				// Lead Sources List
				$LeadSourcesTable = TableRegistry::get('LeadSources');
				$leadSources = $LeadSourcesTable->find('list');
				$leadSourcesList = $leadSources->toArray();

				// State List
				$statesTable = TableRegistry::get('States');
				$statesSources = $statesTable->find('list');
				$stateList = $statesSources->toArray();
				
				// Stage List
				$stageList = [
					'1'=>'Stage 1: Nurture Client',
					'2'=>'Stage 2: Client Interested',
					'3'=>'Stage 3: Client Login',
					'4'=>'Stage 4: Client Form Completed',
					'5'=>'Stage 5: R4aC Reviewed',
					'6'=>'Stage 6: Dr. Notified',
					'7'=>'Stage 7: Dr Completed',
					'8'=>'Stage 8: Client Approved',
					'9'=>'Stage 9: Deposit Requested',
					'10'=>'Stage 10: Deposit Paid',
					'11'=>'Stage 11: Client Travel Added',
					'12'=>'Stage 12: Hotel Confirmed',
					'13'=>'Stage 13: Client Pre-Op Confirmed',
					'14'=>'Stage 14: Post-Op Email Sent'
				];

				//sales rep list
				$salesrepList = $this->getSalesRepList(); 

				//owner list
				$doctorsList = $this->getDoctorList();


				$user = $this->Users->get($id, ['contain'=>['Profiles', 'UserSurgeries.Surgeries', 'UserUploads.Uploads']]);
				$this->set(Compact(['user', 'surgeriesList', 'proceduresList', 'leadSourcesList', 'stateList', 'stageList', 'doctorsList', 'salesrepList']));
				echo $this->render('/Element/user/edit_user');
			}else{
				$result = [
						'result'=> 'Error',
						'msg'	=> 'No record found', 
						'data'	=> ''
						];
				echo json_encode($result);	
			}
			die;
		}
	}


	/*
	* Function to get Doctor
	* return Doctor name array
	*/
	public function getDoctor($doctor_id = null){
		$usersTable = TableRegistry::get('Users');
		$profilesTable = TableRegistry::get('Profiles');

		$doctorsTable['Profiles'] =[];

		if(!empty($doctor_id)){

			$doctorsTable = $usersTable->find('all', [
				'conditions'=> [
							'Users.id'=>$doctor_id
				],
				'fields'=>['Profiles.firstname', 'Profiles.lastname']
				])->contain(['Profiles']);

			if(!empty($doctorsTable)){
				$doctorsTable = $doctorsTable->first();
			}
		}
		return $doctorsTable['Profiles'];
	}
	
	/*
	* Function to get Sales Reps
	* return Sales name array
	*/
	public function getSalesRep($sales_rep_id = null){
		$usersTable = TableRegistry::get('Users');
		$profilesTable = TableRegistry::get('Profiles');

		$salesTable['Profiles'] = [];
		if(!empty($sales_rep_id)){
				
			$salesTable = $usersTable->find('all', [
				'conditions'=> [
							'Users.id'=>$sales_rep_id
				],
				'fields'=>['Profiles.firstname', 'Profiles.lastname']
				])->contain(['Profiles']);

			if(!empty($salesTable)){
				$salesTable = $salesTable->first();
			}

			return $salesTable['Profiles'];
		}
	}
		
	// Doctors List 
	public function getDoctorList(){
		$usersTable = TableRegistry::get('Users');
		$doctorsList = [];
		$usersDoctor = $usersTable->find('all', [
							'fields' => ['Users.id', 'Profiles.firstname', 'Profiles.lastname'],
							'conditions'=>[
								'Users.role'=>'doctor', 
								'status'=>'Active']
							])->contain(['Profiles']);
		
		foreach($usersDoctor as $user){
			$doctorsList[$user->id] = $user->profile->firstname.' '.$user->profile->lastname;	
		}
		return $doctorsList;
	}

	// Sales Rep List
	public function getSalesRepList(){
		$usersTable = TableRegistry::get('Users');
		$salesrepList = [];
		$salesReps = $usersTable->find('all', [
							'fields' => ['Users.id', 'Profiles.firstname', 'Profiles.lastname'],
							'conditions'=>[
								'Users.role'=>'salesrep', 
								'status'=>'Active']
							])->contain(['Profiles']);
		
		foreach($salesReps as $user){
			$salesrepList[$user->id] = $user->profile->firstname.' '.$user->profile->lastname;	
		}

		return $salesrepList;
	}
	

	public function login_as_user($id = null, $direction = 'right', $controller = 'contacts'){
		$this->autoRender = false;
		$this->layout = 'ajax';
		$session = $this->request->session();

		$id = base64_decode($id);
		$user = $session->read('Auth');
		$admin_id = $user['User']['id'];
		
		/*Updating user table last login*/
		$usersTable = TableRegistry::get('Users');
		$user = $this->Users->findAuth($usersTable, array('id'=>$id));
		
		if ($user) {
			
			$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'contacts/';
			if($user['status'] !== "Active"){
				$this->Flash->error(
					__('This account is not acitve'),
					'default',
					[],
					'auth'
				);
				return $this->redirect($referer);
			}
			$sessionReferer = $session->read('Auth.User.runtime_referer');
			
			$this->Auth->logout();
			$this->Auth->setUser($user);
			
			$users = $usersTable->get($user['id'], ['contain'=>'Profiles']);

			$users->id = $user['id'];
			$users->login_ip = $this->request->clientIp();
			$users->last_login = Time::now();

			$usersTable->save($users);

			$session->write('Auth.User.Profile', $users->profile);

			if($direction == 'right'){
				$session->write('Auth.User.runtime', 1);
				$session->write('Auth.User.runtime_from', $admin_id);
				$session->write('Auth.User.runtime_referer', $controller);
			}

			if($direction == 'left'){
				$session->delete('Auth.User.runtime');
				$session->delete('Auth.User.runtime_from');
				$session->delete('Auth.User.runtime_referer');
			}

			$role = $users->role;
			
			// Save Activity log short content in activity table
			$activity = [
				'user_id'	 => $user['id'],
				'profile_id' => $users->profile->id,
				'content'	 => $users->profile->firstname.' '.$users->profile->lastname.' logged in',
				'type'		 => 'login_as_user',
				'updated_by' => $this->Auth->user('Profile.id')
			];
			TableRegistry::get('Activities')->addActivity($activity);
			
			
			switch($role){
				case 'superadmin' :
				case 'admin' : 
				case 'salesrep' :	
				case 'superadmin' :
				case 'staff' :
						return $this->redirect(['controller'=>$sessionReferer, 'action'=>'index', 'prefix' => false]);
					break;
				case 'doctor' : 
						return $this->redirect(['controller'=>'contacts', 'action'=>'index', 'prefix' => 'doctor']);
					break;
				case 'patient' :
				case 'lead' :
						return $this->redirect(['controller'=>'contacts', 'action'=>'index', 'prefix' => 'patient']);
					break;
				case 'driver' :
						return $this->redirect(['controller'=>'contacts', 'action'=>'index', 'prefix' => 'driver']);
					break;
			}
			
		} else {
			$this->Flash->error(
				__('Username or password is incorrect'),
				'default',
				[],
				'auth'
			);
		}
	}

}