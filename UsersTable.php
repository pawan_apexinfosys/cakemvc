<?php 

// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Query;

class UsersTable extends Table
{

	public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
		$this->hasOne('Profiles', [
			'dependent' => true,
			'joinType' => 'LEFT'
		]);

		$this->hasOne('Calls', [
			'dependent' => true,
			'joinType' => 'LEFT'
		]);
		
		$this->hasOne('Schedules', [
			'dependent' => true,
			'joinType' => 'LEFT'
		]);
	
		$this->hasOne('Payments', [
			'dependent' => true,
			'joinType' => 'LEFT'
		]);
		
		$this->hasOne('PatientHealthCareProviders', [
			'dependent' => true,
			'joinType' => 'LEFT'
		]);
		
		$this->hasOne('PatientHealths', [
			'dependent' => true,
			'joinType' => 'LEFT'
		]);

		$this->hasOne('PatientDietHistories', [
			'dependent' => true,
			'joinType' => 'LEFT'
		]);
		
		$this->hasOne('PatientEmergencyContacts', [
			'dependent' => true,
			'joinType' => 'LEFT'
		]);
		
		$this->hasOne('PatientAllergicInfos', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);

		$this->hasOne('PatientReviewSystems', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);

		$this->hasOne('PatientPayDeposits', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);
		
		$this->hasOne('PatientPostOfInfos', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);
		
		$this->hasOne('PatientPreOpInfos', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);
		
		$this->hasOne('PatientTravelInfos', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);

		$this->hasMany('Notes', [
			'dependent' => true,
			'joinType' => 'LEFT'
		]);

		$this->hasMany('Tasks', [
			'dependent' => true,
			'joinType' => 'LEFT'
		]);

		$this->hasMany('UserSurgeries', [
            'foreignKey' => 'user_id',
            'dependent' => true,
        ]);

		$this->hasMany('UserUploads', [
            'foreignKey' => 'user_id',
            'dependent' => true,
        ]);

		$this->hasMany('PatientCurrentMedications', [
            'foreignKey' => 'user_id',
            'dependent' => true,
        ]);

    }

	public function validationDefault(Validator $validator)
    {
          $validator
				->requirePresence('email')
				->notEmpty('email', 'Email is required field')
				->add('email', 'unique', [
					'rule' => 'uniqueEmail',
					'provider' => 'table',
					'message'=>'Email is already taken, try something new.'
				])
				->requirePresence('username')
				->notEmpty('username', 'Username is required field')
				->add('username', 'unique', [
					'rule' => 'uniqueUsername',
					'provider' => 'table',
					'message'=>'Username is already taken, try something new.'
				])
				->add('username', [
					'rule2'	=> [
						'rule' => 'usernameValidate',
						'provider' => 'table',
						'message' => 'Username can only consist of alphanumeric and @._'
					]
				])
				->requirePresence('password')
				->notEmpty('password', 'Password is required field')
				->add('password', [
					'rule1' => [
						'rule' => ['minLength', 8],
						'last' => true,
						'message' => 'Password must be 8 to 15 character long.'
					],
					'rule2' => [
						'rule' => ['maxLength', 15],
						'message' => 'Password must be 8 to 15 character long.'
					],
					'rule3' =>[
						'rule' => 'passwordValidate',
						'provider' => 'table',
						'message' => '<p>Password must contain at least 1 number & 1 letter<br/>May contain any of these characters: !@#$%</p>'
					]
				])
				->requirePresence('confirm_password')
				->notEmpty('confirm_password', 'Confirm Password is required field')
				->add('confirm_password', [
					'rule1' => [
						'rule' => ['minLength', 8],
						'last' => true,
						'message' => ' Confirm Password must be 8 to 15 character long.'
					],
					'rule2' => [
						'rule' => ['maxLength', 15],
						'message' => 'Confirm Password must be 8 to 15 character long.'
					]
				])
				->add('password', [
					'compare' => [
						'rule' => ['compareWith', 'confirm_password'],
						'message' => 'Both password do not match.'
					]
				]);
				
	  
		return $validator;
    }

	public function uniqueEmail($email){
		// If we need Email unique then remove return ture below this line

		return true;
		$users = $this->find('all', ['conditions'=>['Users.email'=>$email]]);
		$user = $users->first();
		
		if(empty($user)){
			return true;
		}
		return false;
	}

	public function uniqueUsername($username) {
		$users = $this->find('all', ['conditions'=>['Users.username'=>$username]]);
		$user = $users->first();
		
		if(empty($user)){
			return true;
		}
		return false;
	}
	
	public function usernameValidate($username){
		if(preg_match('/^[a-zA-Z0-9@._]+$/', $username)) {
			return true;
		}
		return false;
	}

	public function passwordValidate($password){
		if(preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,15}$/', $password)) {
			return true;
		}
		return false;
	}


	public function findAuth($query, array $options = [])
    {
		if (isset($options['id'])) {
			$user = $query->get($options['id'])->toArray();
			return $user;
        }
        return $query;
    }
	
}